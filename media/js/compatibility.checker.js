if(isOkForDrive()){
    console.log('ok');
}else{
    $('input').attr('disabled','true');    
            $('button').attr('disabled','true');
            $('section').css({opacity:'0'});
            $('.comContent').slideToggle(1500,function(){
                    $(this).children('div:not(.textModal)').slideToggle(1000);
            });
}
    


function isOkForDrive(){
    
    if(!window.EventSource && getBroswer()[0]=='Microsoft Internet' && getBroswer()[1]=='10.0'){
        console.log('EventSource');
//        alert('EventSource');
        return false;
    }
    if(!window.File){
        console.log('File');
        alert('File');
        return false;
    }
//    if(!window.DataTransfer){
//        console.log(window);
//        console.log('DataTransfer');
//        return false;
//    }
//    if(!window.FileReader){
//        console.log('FileReader');
//        alert('FileReader');
//        return false;
//    }
    if(!window.FileList){
        console.log('FileList');
        alert('FileList');
        return false;
    }
    if(!window.Modernizr.draganddrop){
        console.log('DragnDrop');
        alert('DragnDrop');
        return false;
    }
    if(!window.XMLHttpRequestUpload && getBroswer()[0]=='Microsoft Internet' && getBroswer()[1]=='10.0'){
        console.log('XMLHttpRequestUpload');
        alert('XMLHttpRequestUpload');
        return false;
    }
    if(!window.XMLHttpRequest){
        console.log('XMLHttpRequest');
        alert('XMLHttpRequest');
        return false;
    }
//    if(!window.DragEvent){
//        console.log('DragEvent');
//        return false;
//    }
    if(!window.FormData){
        alert('Formdata');
        return false;
    }
    return true;
}

function getBroswer(){
    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName  = navigator.appName;
    var fullVersion  = ''+parseFloat(navigator.appVersion); 
    var majorVersion = parseInt(navigator.appVersion,10);
    var nameOffset,verOffset,ix;

    // In Opera, the true version is after "Opera" or after "Version"
    if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
     browserName = "Opera";
     fullVersion = nAgt.substring(verOffset+6);
     if ((verOffset=nAgt.indexOf("Version"))!=-1) 
       fullVersion = nAgt.substring(verOffset+8);
    }
    // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
     browserName = "Microsoft Internet Explorer";
     fullVersion = nAgt.substring(verOffset+5);
    }
    // In Chrome, the true version is after "Chrome" 
    else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
     browserName = "Chrome";
     fullVersion = nAgt.substring(verOffset+7);
    }
    // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
     browserName = "Safari";
     fullVersion = nAgt.substring(verOffset+7);
     if ((verOffset=nAgt.indexOf("Version"))!=-1) 
       fullVersion = nAgt.substring(verOffset+8);
    }
    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
     browserName = "Firefox";
     fullVersion = nAgt.substring(verOffset+8);
    }
    // In most other browsers, "name/version" is at the end of userAgent 
    else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
              (verOffset=nAgt.lastIndexOf('/')) ) 
    {
     browserName = nAgt.substring(nameOffset,verOffset);
     fullVersion = nAgt.substring(verOffset+1);
     if (browserName.toLowerCase()==browserName.toUpperCase()) {
      browserName = navigator.appName;
     }
    }
    // trim the fullVersion string at semicolon/space if present
    if ((ix=fullVersion.indexOf(";"))!=-1)
       fullVersion=fullVersion.substring(0,ix);
    if ((ix=fullVersion.indexOf(" "))!=-1)
       fullVersion=fullVersion.substring(0,ix);

    majorVersion = parseInt(''+fullVersion,10);
    if (isNaN(majorVersion)) {
     fullVersion  = ''+parseFloat(navigator.appVersion); 
     majorVersion = parseInt(navigator.appVersion,10);
    }
    
    return [browserName,fullVersion];
}