var origContent = $('#FileOpenModal #modalContents').html();
var urlStack = new Array();
var eventSource;
$(".modalOpen").click(function(){
	$('#FileOpenModal #title').html('Open File<hr/>');
        eventSource = this;
	getDirContentsCompare($(this).attr('data-drive-url'), $('#rootFolderListItem'));
	//$('#editorModal #modalContents').html('aw');
	$('#FileOpenModal').modal();
        
	$("#FileOpenModal").on($.modal.CLOSE, function(event, modal) {
		clearModalContents();
	});

	function clearModalContents() {
		$('#FileOpenModal #title').html(null);
		$('#FileOpenModal #modalContents').html(origContent);
	}
});

function getDirContentsCompare(url, element) {


	var jqXHR = $.getJSON(url, {contents: 'true'},function(data) {
		urlStack.push(url);
		var items = [];
		var folders = [];
		var files = [];
    
		$.each(data, function(key, val) {
				if (val.type=="Folder" && val.name != ".publications") {
					folders.push('<li class="' + val.type + '"><a rel="'+url+val.name+'/" href="javascript:;"><span class="icon">&#128193;</span>' + val.name + '</a></li>');
				} else if (val.editable == 1 && val.name != ".submitted") {	 

					files.push('<li class="' + val.type + '"><a rel="'+val.path+'" href="javascript:;" class="comparable"><span class="icon">&#128196;</span>' + val.name + '</a></li>');
				}
		});
	  	items = folders.concat(files);
		var appendList = $('<ul/>', {
		'class': 'subMenu',
		html: items.join('')
		}).appendTo(element);
		bindListItemListenerCompare($(appendList).children('li.Folder').children('a'));
                $(appendList).children('li.File').children('a').each(function(index, element){
                    console.log(index);
                    console.log(element);
                    bindFileLoaderItemListenerCompare(element);
                });
                
		loader = $("#loaderImage").detach();
		$.modal.resize();
	
	});
	jqXHR.fail(function(a,b,c) {

		console.log('aw>>> '+c);
	});

}
function bindListItemListenerCompare(element) {
	$(element).toggle(
		function(){
			var url = $(this).attr('rel');
			$(this).append(loader);
			getDirContentsCompare(url, $(this).parent());
			$(this).parent().addClass('hasSubMenu');
			$(this).addClass('animated');
			$(this).addClass('flash');
		},
		function(){
			console.log('hide');
			$(this).siblings('.subMenu').remove();
			$(this).removeClass('animated');
			$(this).removeClass('flash');
			$(this).parent().removeClass('hasSubMenu');
		}
	);
	$(element).click(function(){

	});
}

function bindFileLoaderItemListenerCompare(element) {
  $(element).click(function(){
    if ($(eventSource).is("#compareFile")) {
      url = $(element).attr('rel');
      $("#hdnCompareFile").val(url);
      $("#compareFileForm").submit();
    }
  });
}