$("#shareFolderModal .panel:not(.panel:nth-child(1))").hide();
$("#txtSharedFolderFilter").keydown(function(eventData, obj){
    if (eventData.keyCode === 13) {
        $("#addSharedFolderFilter").click();
        return false;
    }
});
$("#addSharedFolderFilter").click(function(){
    var extension = $("#txtSharedFolderFilter").val().toString().trim();
    if (extension.length == 0) {
        return false;
    }
    if (extension.charAt(0) === '.') {
        extension = extension.substr(1);
    }
    if ($('#ext_'+extension).size() > 0) {
        alert('extension already added');
        return;
    }
    $("#myTable").append('<tr><td>'+extension+'</td><td><a href="javascript:;" class="button red ExtRemove"><span class="icon small">&#10006;</span></a><input type="hidden" id="ext_'+extension+'" name="sharedFolderFilters[]" value="'+extension+'"></td></tr>');
    $("#txtSharedFolderFilter").val('');
    $(".ExtRemove").click(function(){
        $(this).parent().parent().remove();
    });
});
$("#shareFolderModal .Next").click(function(){
    if ($(this).is("#btnNextPanel2") && $("#readOnlyFolder").prop("checked")) {
        $("#submitSharedFolder").click();
        return;
    }
    $(this).parent().hide();
    $(this).parent().next().show();
    $.modal.resize();
});
$("#shareFolderModal .Back").click(function(){
    $(this).parent().hide();
    $(this).parent().prev().show();
    $.modal.resize();
});

$(".useLastName").click(function(){
    //$(this).next("input[type=text]").val("{lastname}");
    $(this).parent().next("input[type=text]").val("{lastname}");
});
$('#sharedFolderDeadline').focus(function(){
    $('#sharedFolderDeadline').appendDtpicker();
});

$(".calendarLink").click(function(){
   
});

$("#toolbarShareFolder").click(function(){
  
    if ($("li.folder.selected").size() < 1) {
        alert('Please select a folder to share');
        return;
    }
    if ($("li.folder.selected").size() > 1) {
        alert('Please select only 1 folder to share');
        return;
    }
     
    if ($("li.folder.selected").size() === 1) {

        initModalForm();
        $("#shareFolderModal").modal();
        $.modal.resize();
        $("#shareFolderModal .panel").hide();
        $("#shareFolderModal .panel").eq(0).show();
        $("#shareFolderModal #sharedFolderPath").val($("li.folder.selected").attr('data-rel-path'));
        var folderName = $("li.folder.selected").children("span").html();
        $("#shareFolderModal #sharedFolderName").val(folderName);
        
    }
    if ($("li.folder.shared.selected").size() === 1) {
        $("#shareFolderModal .panel").hide();   
        $("#shareFolderModal .panel").eq(3).show();
        editSharedFolder($("li.folder.shared.selected").attr('data-id'));
    }
});

function editSharedFolder(id) {
    $("body").addClass("wait");
    checkAccessMode($("input[name='sharedFolderMode']"));
    var xhr = $.getJSON('/faculty/drive',{"sharedFolderID": id});
    xhr.done(function(folder, message, xhr){
        if (folder == null) {
            alert('no record of shared folder');
            $.modal.close();
            return;
        } else {
            $("#modalContents").append('<input type="hidden" id="folderID" name="folderID" value="'+folder.id+'">');
            $("#shareFolderModal .panel").hide();
            $("#shareFolderModal .panel").eq(0).show();
            if (folder.accessMode == 2) {
                $("#readOnlyFolder").attr('checked', 'checked');
            } 
            if (folder.accessMode == 1) {
                $("#writableFolder").attr('checked', 'checked');
            }
            if (folder.deadline != null) {
                $("#sharedFolderDeadline").val(folder.deadline);
            } 
            if (folder.password != null) {
                $("input#sharedFolderPassword").val(folder.password);
            }
            if (folder.prefix != null) {
                $("input#sharedFolderPrefix").val(folder.prefix);
            }
            if (folder.suffix != null) {
                $("input#sharedFolderSuffix").val(folder.suffix);
            }
            if (folder.filterType == 1) {
                $("#rejectFilters").attr('checked', 'checked');
            }
            $(folder.filters).each(function(index, element){
                $("#txtSharedFolderFilter").val(element.extension);
                $("#addSharedFolderFilter").click();
            });
        }
        
    });
    
    xhr.always(function(){
        $("body").removeClass("wait");
    });
}

function initModalForm() {
    $("#shareFolderModal #folderID").remove();
    $("#shareFolderModal input[type=text]").val('');
    $("#shareFolderModal #writableFolder").attr("checked", '');
    $("#shareFolderModal #readOnlyFolder").removeAttr( "checked" );
    $("#shareFolderModal #acceptFilters").attr("checked", '');
    $("#shareFolderModal #rejectFilters").removeAttr( "checked" );
    $("#shareFolderModal #myTable tr").remove();
    
}

$("button#submitSharedFolder").click(function(){
    var flag = 0;
    if ($("#writableFolder:checked").length == 1) {


        
        if ($("#sharedFolderPassword").val() == "") {
            flag = 2;
        }
        
        if ($("#sharedFolderDeadline").val() == "") {
            flag = 1;
        }
        
        if (flag == 0 && $("#acceptFilters:checked").length == 1 && $("input[name='sharedFolderFilters[]']").length == 0) {
            if (confirm('You have not specified any filters. This will reject all file extensions. Continue?')) {
                flag = 0;
            } else {
               flag  = 3;
            }
        }
    }

    switch (flag) {
        case 1:
            alert('deadline must be set');
            return false;
            break;
        case 2:
            alert('password cannot be empty');
            return false;
            break;
        case 3:
            return false;
        default:
            return true;
    }
    return false;
});
var days_full = [
                    'Sunday',
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday'
                ];
var month_abbrs = [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ];
$("#toolbarSubmissionReport").click(function(){

    if ($("li.folder.shared.selected").length == 1) {
        
        $('body').addClass('wait');
        var selectedFolder = $("li.folder.shared");
        var id = $("li.folder.shared.selected").attr('data-id');
        var reportsURL = '/faculty/submission/report/'+id;
        
        var xhr = $.getJSON(reportsURL,{}, function(data){
            
            $("#reportDateStart").val(data.shareDate);
            $("#reportDateStart").focus();
            $("#reportDateEnd").val(data.deadline);
            $("#reportDateEnd").focus();
            $("#reportDateEnd").focusout();
            $("#totalSubmissions").html(data.total);
            $("#submissionReportsTable tbody").html('');
            $.each(data.reports, function(index, report){
                var action = '<span class="icon icon-dark" title="Submitted">&#10150;</span>';
                if (report.action == 'resubmitted') {
                    action = '<span class="icon icon-dark" title="Resubmitted">&#9998;</span>';
                }
                var date = new Date(report.date);
                var fDate = days_full[date.getDay()] + ', ' + month_abbrs[date.getMonth()] + ' ' + date.getDate() + ' ' + (date.getHours()==0 ? 12:Math.floor(date.getHours()/2))  + ':'+date.getMinutes(); // formatted date
                var dateID = date.getFullYear()+''+date.getMonth()+''+date.getDate();
                var row = '<tr data-date="'+dateID+'">'+
                            '<td>'+report.name+'</td>'+
                            '<td>'+action+'</td>'+
                            '<td>'+report.folder+'</td>'+
                            '<td>'+fDate+'</td>'+
                          '</tr>';
                $(row).appendTo($("#submissionReportsTable tbody"));
            });
            
        });
        xhr.always(function(){
             $('body').removeClass('wait');
             $.modal.resize();
        });
        
        
        
        
        $("#submissionReports").modal();
        $("#submissionReports").css("width", "60%");
        $.modal.resize();

    }
    
});

$('.SubmissionReportDatePicker').focus(function(){
    $(this).appendDtpicker({
        'dateFormat' : 'YYYY/MM/DD',
        'dateOnly' : true,
    });
});
$(".SubmissionReportDatePicker").change(function(){
    
    var start =   new Date($("#reportDateStart").val());
    start = start.getFullYear()+''+start.getMonth()+''+start.getDate();
    var end = new Date($("#reportDateEnd").val());
    end = end.getFullYear()+''+end.getMonth()+''+end.getDate();
    $("#submissionReportsTable tbody tr[data-date]").each(function(index, element){
        var date = $(element).attr('data-date');
        if (date < start || date > end) {
            $(element).hide();
        } else {
            $(element).show();
        }
    });
});

$("input[name='sharedFolderMode']").click(function(){
    checkAccessMode($("input[name='sharedFolderMode']"));
});

function checkAccessMode(element) {
    if ($(element).is("#writableFolder:checked")) { // enable all fields
        console.log('write');
        $("#sharedFolderDeadline").prop('disabled', false);
        $("#sharedFolderPrefix").prop('disabled', false);
        $("#sharedFolderSuffix").prop('disabled', false);
    } else if ($(element).is("#readOnlyFolder:checked")) {
        $("#sharedFolderDeadline").prop('disabled', true);
        $("#sharedFolderPrefix").prop('disabled', true);
        $("#sharedFolderSuffix").prop('disabled', true);
        console.log('read');
    }
}
$("#toolbarUnshare").click(function(){
    if ($("li.folder.shared.selected").length == 1) {
        var driveUrl = $("input#url").val();
        var shared = $("li.folder.shared.selected");
        var shareID = $("li.folder.shared.selected").attr('data-id');

        var unshareXhr = $.getJSON(driveUrl, {unshare: shareID});
        unshareXhr.done(function(response){
            if (response.status == true) {
                CustomDialog.Alert(CustomDialog.INFO, "Success", "Sharing is now removed.");
                $(shared).removeClass("shared");
                $(shared).removeClass("private");
            } else {
                CustomDialog.Alert(CustomDialog.ERROR, "Error", "Error with unshare process");
            }
        });
    } else {
        CustomDialog.Alert(CustomDialog.ERROR, "Error", "Please select a shared folder");
    }

});