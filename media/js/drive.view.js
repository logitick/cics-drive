/**
*	@Oject DriveView
*/
var DriveView = {
	driveItems:[],//holds all the items
	displayAllItem:function(){//displaying all items in the drive
		$('#dropzone #fileSystem').empty();
		this.setVIewSetting(0);
		this.populate("all");
	},
	displayFolder:function(){//displaying all folders
		$('#dropzone #fileSystem').empty();
		this.setVIewSetting(1);
		this.populate("folder");
	},
	displayFile:function(){//display all files
		$('#dropzone #fileSystem').empty();
		this.setVIewSetting(2);
		this.populate("file");
	},
	populate:function(type){//populate the drive
		for(var i=this.driveItems.length-1;i>-1;i--){
			if(this.driveItems[i].itemType === type){
				$('#dropzone #fileSystem').prepend(this.driveItems[i].liElement); 
			}else if(this.driveItems[i].itemType === type){
				$('#dropzone #fileSystem').prepend(this.driveItems[i].liElement); 
			}else if(type == "all"){
				$('#dropzone #fileSystem').prepend(this.driveItems[i].liElement); 
			}	
		}
		liEvent();
	},
	setVIewSetting:function(val){//setting the view menu 
		$('#alldrive').attr('checked',val==0?true:false);
		$('#folderdrive').attr('checked',val==1?true:false);
		$('#filedrive').attr('checked',val==2?true:false);
	},
	isEmpty:function(){
		return (this.driveItems.length == 0);
	},
	isAllDrive:function(){
		return $('#alldrive').prop('checked');
	},
	isFolderDrive:function(){
		return $('#folderdrive').prop('checked'); 
	},
	isFileDrive:function(){
		return $('#filedrive').prop('checked');
	}
};

$('#dropzone #fileSystem').css({'display':'none'});//make all items invisible

for(var i=0;i<$('.dItem').length;i++){//traverse drive items, if not empty
	var item = $($('.dItem').get(i));

	DriveView.driveItems.push({	liElement:'<a href="'+item.attr('href')+'">'+item.html()+'</a>',
								itemName:item.find('#driveItem').text(),
								itemType:item.children('li').hasClass('folder')?"folder":"file"
							  });
}

$('#dropzone #fileSystem').empty();//clear all current items in the drive
$('#dropzone #fileSystem').css({'display':'block'});//make all items appear
$('#dropzone').css({display:'block'});

var viewdata = {view:($Util.url.indexOf('/admin/users/drive')>-1)?$('#viewUserID').val():''};
var forview = ($Util.url.indexOf('/admin/users/drive')>-1)?true:false;

var view = $.ajax({//get the current drive view of the user
			url:'/driveview',
			type:"POST",
			data: viewdata,
			async:true,
			xhr:function(){
				var myXhr = $.ajaxSettings.xhr();//new XMLHttpRequest()||new ActiveXObject("Microsoft.XMLHTTP");
				if( DriveView.driveItems.length > 0){
					$('#flash').text('Initializing Drive...');
				}else{
					$('#flash').remove();
				}
				return myXhr;
			}
		});

view.done(function(data){//set the view of the drive
	//console.log(data);
	switch(parseInt(eval(data))){
		case 0: DriveView.displayAllItem();
				break;
		case 1: DriveView.displayFolder();
				break;
		case 2: DriveView.displayFile();
	}
	
	$('#flash').text('Done');
	$('#flash').css({'background':'none'});
	$('#flash').fadeOut(1000);
});

/**
*	drive view events
*/
$('#alldrive').click(function(){
	DriveView.displayAllItem();
	updateView(3).done(function(data){
		console.log(data);
	});
});

$('#folderdrive').click(function(){
	DriveView.displayFolder();
	updateView(1).done(function(data){
		console.log(data);
	});
});

$('#filedrive').click(function(){
	DriveView.displayFile();
	updateView(2).done(function(data){
		console.log(data);
	});
	
});//end of drive view event

var updateView = function(driveview){//update the current drive view of the user
	if(forview){
		return $.post('/driveview',{data:driveview,view:$('#viewUserID').val(),toView:true});
	}else{
		return $.post('/driveview',{data:driveview,toView:false});
	}
}