//global variable that will hold the items in the current drive
var arcItems;
var arcCount = [];
var arcRoot = [];
var count;
var ctr;
var url = window.location.pathname;
var total;
var request;

$('#archiveBtn').click(function(){
    CustomDialog.Confirm(CustomDialog.INFO, "Archiving the current DRIVE", 
                            "In archiving, the current drive will be emptied. Proceed?", 
                            CustomDialog.YESNO, "archive");
});

/**
 * button CANCEL click
 */
$('#arc_cancel').click(function(){
   
    if( $('#arc_ok').text() == 'Done' ){
        window.location.href = window.location.href+"?done=true";
    }else{
        request.abort();
        request=null;
    }
    clearModalDesigns();
    $.modal.close();
});

/**
 *  button OK click
 */
$('#arc_ok').click(function(){
    var okBtn = $(this);
    if(okBtn.text() == 'Unable to archive'){
        clearModalDesigns();
        $.modal.close();
    }
    if(okBtn.text() == 'Done'){
        window.location.href = window.location.href+"?done=true";
    }
    if(okBtn.text() == 'Start'){
        request = $.post(url,{items:arcRoot});
        
        $('#archiveModal div:first').addClass('arcflash_init');
        $('#archiveModal span').text(count +' item(s) remaining...');
        $('#archiveModal button:last').text('Archiving...');
        
        request.success(function(res,status,xhr){
            if(JSON.parse(res)){
                ctr = 0; 
                process_archive(arcRoot[ctr]);
            }
        });      
     }
});

function clearModalDesigns(){
    $('#archiveModal div:first').removeClass();
    $('#archiveModal div:first').addClass('archiveModalContent');
    $('#archiveModal span').removeClass();
    $('#archiveModal span').html('');
    $('#archiveModal button:last').text('');    
}

function init_archive(){
    request = $.post(url,{archive:true});
    
    request.process = function(){
        $('#archiveModal div:first').addClass('arcflash_init');
        $('#archiveModal span').text('Initializing...');
        $('#archiveModal button:last').text('Please wait...');
    }
    
    request.process();
    request.success(function(result, status, xhr){        
        if(status == 'success'){
            
            $('#archiveModal div:first').removeClass('arcflash_init');
            if(JSON.parse(result) !== 0){
                arcItems = JSON.parse(result);
                arcRoot=[];
                arcCount=[];
                count=0;

                for(var i=0;i<Object.keys(arcItems).length;i++){
                    arcRoot.push( arcItems[i].rootItem );
                    arcCount.push( arcItems[i].countContents );
					console.log(arcItems[i].rootItem);
                }
                
                arcCount.forEach(function(value){
                    count += value;
                });
                total=count;
                
                $('#archiveModal span').addClass('ready');
                $('#archiveModal span').text(count +' item(s) to be archive');
                $('#archiveModal button:last').text('Start');
            }else{
                $('#archiveModal span').addClass('icon empty');
                $('#archiveModal span').html('&#9888; Current Drive is empty.');
                $('#archiveModal button:last').text('Unable to archive');
            }
        }
    });
}


function process_archive(driveitem){
     request = $.post(url,{item:driveitem});

     request.progress = function(){
        $('#archiveModal div:first').addClass('arcflash_init');
        $('#archiveModal span').text(count +' item(s) remaining...');
        $('#archiveModal button:last').text('Archiving...');
     };
     
     request.progress();
     request.success(function(result, status, xhr){   
        if(status == 'success' && (xhr.status == 200 || xhr.readyState == 1)){
            console.log(result);
            var res = JSON.parse(result);
            count -= res;
            ctr++;
            
            if(count>0){
                $('#archiveModal span').addClass('ready');
                $('#archiveModal span').text(count +' item(s) remaining...');
                $('#archiveModal button:last').text('Archiving...');
            }else{
                $('#archiveModal div:first').removeClass('arcflash_init');
                $('#archiveModal span').addClass('icon doneArchive');
                $('#archiveModal span').html('&#10003; Archived successfully.');
                $('#archiveModal button:last').text('Done');
            }
            if(ctr<Object.keys(arcItems).length){
                process_archive(arcItems[ctr].rootItem);
            }
        }
    });
}