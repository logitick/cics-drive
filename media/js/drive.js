//load/Initializing defaults functions
	var _url = window.location.pathname;
	
	if( _url.indexOf('/item_recovery') > -1 || _url.indexOf('/faculty/archive') > -1 ){
		$('#dropzone').css({display:'block'});
	}
	
	toolbarEvents();
	var drive = new Drive(); // create an object of the Drive Class
	
	if(_url.indexOf('item_recovery') != -1){
		contextMenuItemRecovery();
	}else if(_url.indexOf('archive') == -1){
		
		if(_url.indexOf('/student/drive') != -1){
			contextMenuStudent();
		}
		else if(_url.indexOf('/faculty/drive') != -1 ){
			contextMenuFaculty();
		}else if( _url.indexOf('/admin/drive') != -1 	 ){
			contextMenuAdmin();
		}
		
		$Util.getKnownFileTypes().done(function(data){
			$Util.knownFileTypes = eval(data);
		});
		driveSearch();
	}
        liEvent();		
        deselecting();

	
$(window).on('beforeunload',function(e){
	if(_url.indexOf('/admin/drive') != -1){
		$.post('/DriveView.php/',{killUserViewSession:true});
	}
});

var removeDriveitemState = {		
		remove:function(){
			$("#dropzone #fileSystem li").removeClass("selected");
			drive.removeSelectedItem();
		}
}
			
/** 
*contextmenu for student
*/
function contextMenuStudent(){
	$.contextMenu({
		selector: '#dropzone #fileSystem li', 
		callback:function(key, options) {
					
				switch(key){
				
					case '1':	drive.open(this);
								break;
							
					case '2': 	$('html').off();
								$('#download_button').click();
								deselecting();
								break; 
							
					case '3': 	drive.rename(this);
							console.log(this);
                                                        break;
						
					case '4': 	$("button#submit").click();
								break;
								
					case '5':	$('html').off();
								$('#remove_button').click();
							
				}
		},
		items: {
				1: {name: "Open", icon: "open"},
				2: {name: "Download", icon: "download"},
				3: {name: "Rename", icon: "rename"},
				4: {name: "Submit", icon: "submit"},
				5: {name: "Remove", icon: "remove"},
                                6: "---------",
                                7: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
		}
	});
}
//end of context menu for student


/** 
*contextmenu for faculty/admin
*/
function contextMenuAdmin(){
        
        
	$.contextMenu({
		selector: '#dropzone #fileSystem li', 
                build: function($trigger, e){
                          trigger = $trigger;
                          if ($trigger.hasClass('shared')) { // shared folder context menu
									return { 
									  items: {
												1: {name: "Open"},
												2: {name: "Download"},
												3: {name: "Rename"},
												4: {name: "Remove"},
                                                                                                6: "---------",
                                                                                                7: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
											}
									};        
                          }
                          if ($trigger.hasClass('folder')) { // regular folder context menu
                            return { 
                              items: {
										1: {name: "Open"},
										2: {name: "Download"},
										3: {name: "Rename"},
										4: {name: "Remove"},
                                                                                6: "---------",
                                                                                7: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
									}
								};
                          }
                          if ($trigger.hasClass('file')) { // file context menu
                            return { 
                              items: {
									1: {name: "Open"},
									2: {name: "Download"},
									3: {name: "Rename"},
									4: {name: "Remove"},
                                                                        6: "---------",
                                                                        7: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
								  }
                            };
                          }
                          
                        },
		callback:function(key, options) {
					
				switch(key){
				
					case '1':	drive.open(this);
								break;
							
					case '2': 	$('html').off();
								$('#download_button').click();
								deselecting();
								break; 
							
					case '3': 	drive.rename(this);
								break;
							
					case '4':	$('html').off();
								$('#remove_button').click();
				}
		},
	});
}
//end of context menu for faculty/admin

//admin context
function contextMenuFaculty(){
        
        
	$.contextMenu({
		selector: '#dropzone #fileSystem li', 
                build: function($trigger, e){
                          trigger = $trigger;
                          if ($trigger.hasClass('shared')) { // shared folder context menu
                            return { 
                              items: {
				1: {name: "Open"},
				2: {name: "Download"},
				3: {name: "Rename"},
				4: {name: "Sharing", 
                                    items: { 
                                      40: {name: "Reports", callback: function(){$("#toolbarSubmissionReport").click();}}, 
                                      41: {name: "Share Options", callback: function(){$("#toolbarShareFolder").click();}}, 
                                      42: {name: "Unshare", callback: function(){$("#toolbarUnshare").click();}},
                                      43: {name: "Publish", disabled: true},
                                           }
                                   },
				5: {name: "Remove"},
                                6: "---------",
                                7: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
                              }
                            };        
                          }
                          if ($trigger.hasClass('folder')) { // regular folder context menu
                            return { 
                              items: {
				1: {name: "Open"},
				2: {name: "Download"},
				3: {name: "Rename"},
				4: {name: "Sharing", 
                                    items: { 
                                      41: {name: "Share", callback: function(){$("#toolbarShareFolder").click()}}, 
                                      42: {name: "Unshare", disabled: true},
                                      43: {name: "Publish", disabled: true},
                                           }
                                   },
				5: {name: "Remove"},
                                6: "---------",
                                7: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
                              }
                            };
                          }
                          if ($trigger.hasClass('file')) { // file context menu
                            return { 
                              items: {
				1: {name: "Open"},
				2: {name: "Download"},
				3: {name: "Rename"},
				4: {name: "Sharing", 
                                    items: { 
                                      41: {name: "Share", disabled: true}, 
                                      42: {name: "Unshare", disabled: true},
                                      43: {name: "Publish", callback: function(){$("#toolbarPublish").click();}},
                                           }
                                   },
				5: {name: "Remove"},
                                6: "---------",
                                7: {name: "Compare", callback: function(){
                                    if (!$trigger.hasClass('editable')) {
                                        CustomDialog.Alert(CustomDialog.INFO,'Cannot compare','Only editable files can be compared.');
                                    } else {
                                        var path = $trigger.attr('data-rel-path');
                                        window.location = "/faculty/compare"+path;                                        
                                    }
                                }},
                                8: {name: "Move to", callback: function(){$("#btnToolbarMove").click();}}
                              }
                            };
                          }
                          
                        },
		callback:function(key, options) {
					
				switch(key){
				
					case '1':	drive.open(this);
								break;
							
					case '2': 	$('html').off();
								$('#download_button').click();
								deselecting();
								break; 
							
					case '3': 	drive.rename(this);
								break;
						
					case '4': 	alert('share');
								break;
							
					case '5':	$('html').off();
								$('#remove_button').click();
							
				}
		},
	});
}
//end admin context

/** 
*contextmenu for item recovery
*/
function contextMenuItemRecovery(){
	$.contextMenu({
		selector: '#dropzone #fileSystem li', 
		callback:function(key, options) {
					
				switch(key){
					case '1':	$('html').off('click');
								$('#restore_button').click();
								break;
							
					case '2': 	$('html').off('click');
								$('#delete_button').click();
								 
				}
		},
		items: {
				1: {name: "Recover", icon: "recover"},
				2: {name: "Delete", icon: "delete"},
		}
	});
}
//end of context menu for item recovery

/**
*	@object - a static object use for the actions to be done in the drive toolbar
*/
var toolbarActions={
	toDownload:function(){
		if( drive.itemSize() == 0 ){
			CustomDialog.Alert(CustomDialog.INFO,'Sorry..','Drive is empty');
			return false;
		}else if(drive.empty()){
			CustomDialog.Alert(CustomDialog.INFO,'Oppss...','Select an item');
			return false;
		}else{
			$('html').off('click');
			$('#download_button').click();
			deselecting();
		}
	},
	toRemove:function(){
		if( drive.itemSize() == 0 ){
			CustomDialog.Alert(CustomDialog.INFO,'Sorry..','Drive is empty');
			return false;
		}else if(drive.empty()){
			CustomDialog.Alert(CustomDialog.INFO,'Oppss...','Select an item');
			return false;
		}else{
			var name=drive.firstSelectedItem().name;
			var type=drive.firstSelectedItem().type;
			CustomDialog.setName(name);
			type = type=='folder'?'Remove folder "'+name+'" and its contents?':'Remove file "'+name+'"';
			
			if(drive.size() > 1){
				CustomDialog.Confirm(CustomDialog.WARNING,'Remove all selected items','Are you sure?',CustomDialog.YESNO,'remove');
			}else{
				CustomDialog.Confirm(CustomDialog.WARNING,type,'Are you sure?',CustomDialog.YESNO,'remove');
			}
		}
	},
	toDelete:function(){
		if( drive.itemSize() == 0 ){
			CustomDialog.Alert(CustomDialog.INFO,'Sorry..','Drive is empty');
			return false;
		}else if(drive.empty()){
			CustomDialog.Alert(CustomDialog.INFO,'Oppss...','Select an item');
			return false;
		}else{
			var name=drive.firstSelectedItem().name;
			var type=drive.firstSelectedItem().type;
			CustomDialog.setName(name);
			type = type=='folder'?'Delete folder "'+name+'" and its contents?':'Delete file "'+name+'"';
			
			if(drive.size() > 1){
				CustomDialog.Confirm(CustomDialog.WARNING,'Delete all selected items','Are you sure?',CustomDialog.YESNO,'delete');
			}else{
				CustomDialog.Confirm(CustomDialog.WARNING,type,'Are you sure?',CustomDialog.YESNO,'delete');
			}
		}
	},
	toRestore:function(){
		if( drive.itemSize() == 0 ){
			CustomDialog.Alert(CustomDialog.INFO,'Sorry..','Drive is empty');
			return false;
		}else if(drive.empty()){
			CustomDialog.Alert(CustomDialog.INFO,'Oppss...','Select an item');
			return false;
		}else{
			var name=drive.firstSelectedItem().name;
			var type=drive.firstSelectedItem().type;
			CustomDialog.setName(name);
			type = type=='folder'?'Restore folder "'+name+'" and its contents?':'Restore file "'+name+'"';
			
			if(drive.size() > 1){
				CustomDialog.Confirm(CustomDialog.WARNING,'Restore all selected items','Are you sure?',CustomDialog.YESNO,'restore');
			}else{
				CustomDialog.Confirm(CustomDialog.WARNING,type,'Are you sure?',CustomDialog.YESNO,'restore');
			}
		}
	}
	
}
/**
* Toolbar events
*/
	function toolbarEvents(){
		//create folder
		$("#createFolder_button").click(function(){
			$('#folder_name').val('');
			$('#create_folder').modal();
		});
		
		//upload file
		$('#upload_button').click(function(){
			$('#upload_file #fileSystem').empty();
			$('#upload_file').modal({clickClose:false,escapeClose:false});
		}); 
		
		//download
		$('#dummyDownload_button').click(function(e){
			toolbarActions.toDownload();
		});
		
		//remove
		$('#dummyRemove_button').click(function(e){		
			toolbarActions.toRemove();
		});
		
		//delete
		$('#dummyDelete_button').click(function(e){
			toolbarActions.toDelete();
		});
		
		//restore
		$('#dummyRestore_button').click(function(e){
			toolbarActions.toRestore();
		});
		
		//browse file
		$('#choose_file a').click(function(){
			$(':file').click();
		});
		
		//file selected
		$(':file').change(function(){
			for(var i=0;i<this.files.length;i++){
				new Upload(this.files[i]);
			}
		});
	}
//end of toolbar events

/**
*set events to each item in the drive
*/      
	function liEvent(){
		var ctrl = false;
		var shifted = false;
		
		/**
		*	drive item left click
		*/
		$('#fileSystem li').click(function(e){
			
			if (e.ctrlKey){	//when selecting an item along with the ctrl key
				$(this).toggleClass('selected');
				
				if($(this).hasClass('selected')){
					drive.setSelectedItem(this);
				}else{
					drive.removeReadyItem(this);
				}		
			}else{
				drive.removeSelectedItem();
				$(this).parent('a').siblings().find('.selected').removeClass('selected');//removing previous selected item
				$(this).removeClass("appended");
				
				$(this).toggleClass('selected');
				
				if($(this).hasClass('selected')){
					drive.setSelectedItem(this);
				}else{
					console.log(11);
					drive.removeSelectedItem(this);
				}
                                
			}
			
		});
		
		/**
		*	drive item right click
		*/
		$('#fileSystem li').on('mousedown',function(e){
			switch(e.which){	
				case 3:	{	removeDriveitemState.remove();
							$(this).parent('a').siblings().find('.selected').removeClass('selected');//removing previous selected item
							$(this).addClass('selected');
							drive.setSelectedItem(this);
						}
			}
		}); 

		/**
		*	activiting ctr and shift keys, and select all drive item
		*/
		$('html').keydown(function(e) {	
			
			if(e.which == 16){	
				shifted = true;
			}
			
			if(e.which == 17){
				ctrl = true;
			}
			
			if(document.activeElement.id == 'folder_name'){
				return;
			}else if( document.activeElement.id == 'searchDrive'){
				return;
			}else if( document.activeElement.id == 'to_rename' ){
				return;
			}else if( $(document.activeElement).hasClass('select2-input') ){
				return;
			}
			
			
			
			if(ctrl && e.keyCode == 65){ //selecting all item drive hitting ctrl + a/A
				
				e.preventDefault();
				
				$('#fileSystem li').removeClass("appended");
				
				if($('body').children().hasClass('blocker')){
					return false;
				}
				
				$("#dropzone #fileSystem li").addClass("selected");
				
				for(var i=0;i<$('#fileSystem li').length;i++){
					drive.setSelectedItem($($('#fileSystem li').get(i)));
				}
			}
			
		});
		
		//double clickss
		$('#dropzone #fileSystem li').dblclick(function(){
			drive.open(this);
		});
		
		//key up
		$('html').keyup(function(e) {
				if (e.which == 16) {
					shifted = false;
				}
				if (e.which == 17) {
					ctrl = false;
				}
		});
		
		//disable redirection when anchor element is click
		$("#dropzone a").click(function(){
				return false;
		});
	};
//end liEvent

/**
*	deselecting all selected drive item
*/
function deselecting(){
		$('html').on('click',function(evt){
			
			if(evt.target.id == 'dummyRemove_button'){
				return false;
			}else if(evt.target.id == 'dummyDownload_button'){
				return false;
			}else if(evt.target.id == 'modal_Yes'){
				return false;
			}else if(evt.target.className == 'jquery-modal blocker'){
				return false;
			}else if(evt.target.className == 'modal'){
				return false;
			}else if(evt.target.className == 'modal_icon'){
				return false;
			}else if(evt.target.className == 'modal_header'){
				return false;
			}else if(evt.target.className == 'modal_Content'){
				return false;
			}else if(evt.target.className == 'modal_Button'){
				return false;
			}else if(evt.target.id == 'dialogAlert'){
				return false;
			}else if(evt.target.id == 'dialogConfirm'){
				return false;
			}else if(evt.target.id == 'dummyDelete_button'){
				return false;
			}else if(evt.target.id == 'dummyRestore_button'){
				return false;
			}else if(evt.target.id == 'restore_button'){
				return false;
			}else if(evt.target.id == 'download_button'){
				return false;
			}else if(evt.target.tagName == 'SPAN'){
				return false;
			}else{
				removeDriveitemState.remove();
			} 
		});
}
	
/**
*	Drag and drop dropzone
*/
	$(function(){
		$('#dropzone').on({
			dragenter: function dragEnter(e){
					e.preventDefault();
				},
			dragover: function dragOver(e){
					e.preventDefault(e);
					$(this).css({'box-shadow':'inset 0 0 5px rgb(59, 62, 64)'});
				},
			drop: function drop(e){
					$('#upload_file #fileSystem').empty();
					e.preventDefault();
					$(this).css({'box-shadow':'none'});
						if(e.originalEvent.dataTransfer.files.length > 0){
							$('#upload_file').modal();
							for(var i=0;i<e.originalEvent.dataTransfer.files.length;i++){
								new Upload(e.originalEvent.dataTransfer.files[i]);
								/* var f = e.originalEvent.dataTransfer.files[i];
								if (f) {
									var r = new FileReader();
									r.onload = function(e) { 
										new Upload(f);
									}
									  r.readAsArrayBuffer(f);
								}  */
								/*if(!e.originalEvent.dataTransfer.items[i].webkitGetAsEntry().isDirectory){ 
									new Upload(e.originalEvent.dataTransfer.files[i]);
								} */
							}
						}
				},
			dragend: function dragEnd(e){
					e.preventDefault();
					$(this).css({'box-shadow':'none'});
				},
			dragleave: function dragLeave(e){
					e.preventDefault();
					$(this).css({'box-shadow':'none'});
				}
		});
	});
	
	function reads(){
		
	}
	
	
	$(function(){
		$('#upload_file').on({
			dragenter: function dragEnter(e){
					e.preventDefault();
				},
			dragover: function dragOver(e){
					e.preventDefault(e);
					$(this).css({'box-shadow':'inset 0 0 5px rgb(85, 77, 80)'});
				},
			drop: function drop(e){
					e.preventDefault();
					$(this).css({'box-shadow':'none'});
						if(e.originalEvent.dataTransfer.files.length > 0){
							for(var i=0;i<e.originalEvent.dataTransfer.files.length;i++){
								new Upload(e.originalEvent.dataTransfer.files[i]);
								/* var f = e.originalEvent.dataTransfer.files[i];
								if (f) {
									var r = new FileReader();
									r.onload = function(e) { 
										new Upload(f);
									}
									  r.readAsArrayBuffer(f);
								} 
								  */
								 /* new Upload(e.originalEvent.dataTransfer.files[i]);  */
							}
						}
				},
			dragend: function dragEnd(e){
					e.preventDefault();
					$(this).css({'box-shadow':'none'});
				}
		});
	});
	
//end of drag and drop

	$(function(){
		$('html').on({
			dragenter: function dragEnter(e){
					e.preventDefault();
				},
			dragover: function dragOver(e){
					e.preventDefault(e);
				},
			drop: function drop(e){
					e.preventDefault();
				},
			dragend: function dragEnd(e){
					e.preventDefault();
				}
		});
	});


/**
*	search item drive
*/

function driveSearch(){
	var searchDisplay;
	var regularUrl = $('#dirPath').attr('value');
	var editorUrl = regularUrl.replace('drive','editor');
	
	function format(searchedItem){
		var drivePath = 'MyDrive'+searchedItem.path;
		
		if(drivePath.length>32){
			drivePath = drivePath.substr(0,drivePath.length-20)+'...';
		}
		
		var formattedResult = 	'<ul id="fileSystem">'
								+'<li title="Path: MyDrive'+searchedItem.path+'" class="'+searchedItem.type+' '+searchedItem.extension+' searchResults">'
								+searchedItem.id
								+'<br/><strong>Path:</strong> '+ drivePath
								+'</li></ul>'; 
		
		return formattedResult; 
	};
	
	function format2(searchedItem){
		var url = $Util.useEditor(searchedItem.extension)?regularUrl.substr(0,regularUrl.indexOf('/drive'))+'/editor':regularUrl;
		
		
		var formattedResult = 	'<a href="'+url+searchedItem.path+'">'
								+'<li class="'+searchedItem.type+' '+searchedItem.extension+'">';
		
		var dis = 	'<ul id="fileSystem">'
					+'<li title="Path: MyDrive'+searchedItem.path+'" class="'+searchedItem.type+' '+searchedItem.extension+' searchResults">'
					+searchedItem.id
					+'</li></ul>'; 
								
		if(searchedItem.type == 'folder'){
			formattedResult +='<span id="driveItem">'+searchedItem.id+'</span>';
			formattedResult += '</li></a>';
		}else{
			formattedResult +='Name: <span id="driveItem">'+searchedItem.id+'</span>';
			formattedResult += '<br>'+'Size: <strong>'+searchedItem.size+'</strong>'
			formattedResult +='<br>'+'Last Modified: <strong>'+searchedItem.modified+'</strong>'
			formattedResult +='</li></a>';
		}
		
		$('#fileSystem li').remove();
		$('#dropzone #fileSystem').prepend(formattedResult);
		liEvent();
		
		return dis;
	};
	
	$('#hid').on('change',function(){
		if($(this).val().length==0){
			if(DriveView.isAllDrive()){
				DriveView.displayAllItem();
			}else if(DriveView.isFolderDrive()){
				DriveView.displayFolder();
			}else if(DriveView.isFileDrive()){
				DriveView.displayFile();
			}
		}
	});
	
	$('#hid').select2({
		placeholder:"Search drive item",
		allowClear: true,
		minimumInputLength:1,
		ajax:{
			url:$Util.url,
			dataType:'json',
			data: function(term,page){
					return { 
								search:term
							};
			},
			results: function(data,page){
				return {
					results:data
				};
			}
		},
		formatResult:format,
		formatSelection:format2
	});
}