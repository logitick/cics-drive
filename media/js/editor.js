String.prototype.hashCode = function(){
	var hash = 0;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		char = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}

var origContent = $('#editorModal #modalContents').html();
var urlStack = [];
var loader = null;
var codeEditor = document.getElementById('codeEditor');
{ // code editor main
	 var myCodeMirror = CodeMirror.fromTextArea(codeEditor,
		{	
                    lineNumbers: true,
		    smartIndent: false,
		    theme: 'eclipse',
		    mode: $("#mime").val()
		}
	);
        checkChanges = function() {
            if (!myCodeMirror.isClean()) {
                return confirm("All unsaved changes will be lost. Continue?");
            }
            return true;
        }
        $("button[name=btnDownload]").click(function(){
            return checkChanges();
        });
 

	$("#editorModal").on($.modal.CLOSE, function(event, modal) {
		clearModalContents();
	});

	function clearModalContents() {
		$('#editorModal #title').html(null);
		$('#editorModal #modalContents').html(origContent);
	}
} // end of code editor



// code editor button actions
var urlStack = new Array();

$("button[name=btnOpen]").click(function(){
        if (checkChanges()) {
	//$('#editorModal #title').html('Open File<hr/>');
        $("#editLink").click();
	//getDirContents($('input[name=root]').val(), $('#rootFolderListItem'));
	//$('#editorModal #modalContents').html('aw');
	//$('#editorModal').modal();
	
	
        }
        return false;
});

function getDirContents(url, element) {


	var jqXHR = $.getJSON(url, {contents: 'true'},function(data) {
		urlStack.push(url);
		var items = [];
		var folders = [];
		var files = [];

		$.each(data, function(key, val) {					
				if (val.type=="Folder" && (val.name.indexOf(';recycle;')==-1)) {
					folders.push('<li class="' + val.type + '"><a rel="'+url+val.name+'/" href="javascript:;"><span class="icon">&#128193;</span>' + val.name + '</a></li>');
				} else if (val.editable == 1 && (val.name != ".submitted" && val.name != ".shared")) {	 

					files.push('<li class="' + val.type + '"><a rel="'+val.name+'" href="'+url.replace(/[/]drive[/]/g, '/editor/', 1)+val.name+'"><span class="icon">&#128196;</span>' + val.name + '</a></li>');
				}
		});
	  	items = folders.concat(files);
		var appendList = $('<ul/>', {
		'class': 'subMenu',
		html: items.join('')
		}).appendTo(element);
		
		bindListItemListener($(appendList).children('li.Folder').children('a'));
		loader = $("#loaderImage").detach();
		$.modal.resize();
	
	});
	jqXHR.fail(function(a,b,c) {
		/*urlStack.pop();
		urlStack.pop();
		console.log('aw:->>>>>'+urlStack.pop());
		getDirContents(urlStack.pop());*/
		console.log('aw>>> '+c);
	});

}
function bindListItemListener(element) {
	$(element).toggle(
		function(){
			var url = $(this).attr('rel');
			$(this).append(loader);
			getDirContents(url, $(this).parent());
			$(this).parent().addClass('hasSubMenu');
			$(this).addClass('animated');
			$(this).addClass('flash');
		},
		function(){
			console.log('hide');
			$(this).siblings('.subMenu').remove();
			$(this).removeClass('animated');
			$(this).removeClass('flash');
			$(this).parent().removeClass('hasSubMenu');
		}
	);
	$(element).click(function(){

	});
}
