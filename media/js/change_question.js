var ctr = 0;
var question_list=[];

$('.question').click(function(){
        
        if(ctr > 4){
            alert('You can only change the question 5 times.');
            return;
        }	
        
        if($(this).text()=='Change'){
            ctr++;
            
            var value = $(this).siblings('input[type=hidden]').val();
            changeQ(this,value);
        }
});

function changeQ(obj,value){
    var index = $(obj).siblings('input[type="hidden"]').val();
    var xhr = $.post('/register', {change:value});
		
    $(obj).text('Changing...');
    
    xhr.done(function(data){
        $(obj).prev('label').html('Q'+(parseInt(value)+1)+'. '+data);
        $(obj).text('Change');
        
    });
}
