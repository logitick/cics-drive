$("button#submit").click(function(){
    
    if ($("li.folder.selected").size() < 1) {
        alert('Please select a folder to share');
    }
    if ($("li.folder.selected").size() > 1) {
        alert('Please select only 1 folder to share');
    }
    
    if ($("li.folder.selected.shared").size() > 1) {
        alert('The selected folder has already been shared');
    }
   
    if ($("li.folder.selected").size() === 1) {
        $("#submitFolderModal").modal();
        
        $("#submissionFolderPath").val($("li.folder.selected").attr('data-rel-path'));
        submitModalInit();
    }
});

listening = false;  

$("#submissionForm a.button.Back").click(function(){
     $("#submitFolderModal #modalContents .panel").hide();
     $("#submitFolderModal #modalContents .panel").eq(0).show();
});
$("#submissionInstructor").change(function(){
    $("#btnSubmitFolder").off();
    $("#loader").show();
    $('.DriveItemsList').children().remove();
    var id = $(this).val();
    var request = $.getJSON('/student/shared/'+id);
    request.done(function(data, message, xhr){
        $("#loader").hide();
        var items = new Array();
        
        $.each(data, function(key, val) {
            deadline = new Date(val.deadline);
            today = new Date();
            if (today < deadline) {
                items.push('<li class="Folder"><a class="FolderLink animated flash" href="javascript:;" data-id="'+val.id+'"><span class="icon icon-dark">&#128193;</span> ' + val.name + '</a></li>');
            }
        });
        console.log(items);
        $('ul.DriveItemsList').append(items);
        
        
        listenStart();
        
        
    });
    var force = false;
    $("#btnSubmitFolder").click(function(evnt){
        
        listening = true;
        var data = $("#submissionForm").serialize();
        if (force) {
            data += '&force=true';
        }
        var postRequest = $.post('/student/shared/'+id, data);
        postRequest.done(function(data, message, xhr){
            console.log(data);
            data = $.parseJSON(data);

            if (data.error == 0) {
                //alert('Folder submitted.');
                $.modal.close();
                
                var message = '';
                var submittedPath = $("#submissionFolderPath").val();
                $("li.folder[data-rel-path='"+submittedPath+"']").addClass('submitted');
                
                if (data.accepted && data.accepted.length > 0) {
                  message += '<h1>Accepted Files:</h1>';
                  message += '<ul>'
                  $.each(data.accepted, function(index, value){
                    message += '<li>'+value+'</li>';
                  });             
                  message += '</ul>';
                }
                
                if (data.rejected && data.rejected.length > 0) {
                  message += '<h1>Rejected by file filter:</h1>';
                  message += '<ul>'
                  $.each(data.rejected, function(index, value){
                    message += '<li>'+value+'</li>';
                  });             
                  message += '</ul>';
                }

                CustomDialog.Alert('', "Folder Submitted<hr/>", message);
                $.modal.resize();
            }
            if (data.error == 1) {
                alert('wrong password');
            }
            if (data.error == 2) {
                alert('The deadline for this folder has passed.');
                $.modal.close();
            }
            if (data.error == 3) {
                alert('Cannot submit to a read-only folder');
                $.modal.close();
            }
            if (data.error == 4) {  
                alert('Unknown student account');
                $.modal.close();
            }
            if (data.error == 5) {  
                alert('Your folder has been removed. This page will be reloaded.');
                $.modal.close();
                window.location.reload(true);
            }
            if (data.error == 6) { 
                alert('Could not create a record.');
                $.modal.close();
            }
            if (data.error == 7) { 
                if(confirm('You have already submitted to this folder. Do you want to overwrite last submission?')) {
                    force = true;
                    $("#btnSubmitFolder").click();
                } else {
                    $.modal.close();
                }

            }
             if (data.error == 8) { 
                alert('Could overwrite last submission.');
                $.modal.close();
             }
            
            
        });
        postRequest.always(function(){
            $("body").removeClass('wait');
            force = false;
        });
        evnt.stopPropagation();
    });
});

function listenStart() {
    $('a.FolderLink').click(function(evt){
        var folderID = $(this).attr('data-id');
        $("#submitToSharedFolderID").val(folderID);
        $("#submitFolderModal #modalContents .panel").eq(0).hide();
        $("#submitFolderModal #modalContents .panel").eq(1).show();
        evt.stopPropagation();
    });
}

function submitModalInit() {
    listening = false;
    $("#submitFolderModal #modalContents .panel:not(.panel:first-child)").hide();
    $("#submitFolderModal #modalContents .panel:first-child").show();
    $("#loader").hide();
    $("#submissionInstructor").select2();
    $("#sharedFolderPassword").val('');
    $('.DriveItemsList').children().remove();
}