
/*var sse = new EventSource('../notification');

sse.onmessage=function(evt){
    console.log(evt.data);
    console.log(evt.lastEventId);
}

sse.close();
*/
/**
*	@Object Drive - class that implements basic functionality to the drive system 	 
*	Note: properties with the 'var' prefix are private, and properties having 'this 'prefix are public 
*/
var Drive = function(){
	var selectedItem=[];//hold the item that the user selects
	
	/**
	*	@method open - use to open a drive item
	*	@parameter object - drive item
	*/
	this.open=function(element){
		var uri = $(element).parent('a').attr('href');
		var item = uri.substr(uri.lastIndexOf('/')+1,uri.length);
		
		if(item.lastIndexOf('.')==-1){
			window.location = $(element).parent('a').attr('href');
			return false;
		}
		if(uri.search('/editor')>-1){
			window.location = $(element).parent('a').attr('href');
		}
		if(uri.search('editor')==-1){
                        
			if($Util.isImageFile(item)){
				var loc = $(element).parent('a').attr('href');
//				$('#previewpicture').attr('action',loc);
//				$('#previewpicture button').click();
                                
                                $('#imgModal').modal();
                                $('#imgModal').html(''); 
                                var getImg = $.get(loc);
                                getImg.done(function(){
                                  $('#imgModal').html('<img style="max-width:400px;" src="'+loc+'">'); 
                                  $.modal.resize();
                                });
				
				//window.location = $(element).parent('a').attr('href');
			}else{ 
                                window.location = $(element).parent('a').attr('href');
				//CustomDialog.Alert(CustomDialog.INFO,"Sorry....","This item cannot be read");
                               // $("button#dummyDownload_button").click();
			}
		} 
	}
	
	/**
	*	@method rename - use to rename a drive item
	*	@parameter object - drive item
	*/
	this.rename=function(element){
                var name='';
		$('#rename').modal();
		if($(element).hasClass('file')){
                  name = $(element).attr('title');
                  name = name.substr(0,name.lastIndexOf("."));
                }
                if($(element).hasClass('folder')){
                  name = $(element).find('#driveItem').text();
                }
                $('#to_rename').val( name );
		
                $('#to_rename').focus();
		document.getElementById("to_rename").select();
		$('#current_name').val($(element).attr('title'));
                console.log($(element).attr('title'));
	}
	
	/**
	*	@method setSelectedItem - set all selected drive item into the selectedItem array
	*	@parameter object - li element 
	*/
	this.setSelectedItem=function(element){
		var itemName = $Util.getDriveItemName(element);
		var itemType = $Util.getDriveItemType(element);
		
		if( !isItemNameExist(itemName) ){
			selectedItem.push({name:itemName,
								type:itemType});
			initItemIntoHTMLInput( itemName );
		}
	}
	
	/**
	*	@method firstSelectedItem - get the first selectedItem (public)
	*	@object object
	*/
	this.firstSelectedItem=function(){
		return selectedItem[0];
	}
	
	/**
	*	@method getSelectedItem - get the last selectedItem (public)
	*	@parameter int - position in the array;
	*	@return object
	*/
	this.getSelectedItem=function(index){
		return selectedItem[index];
	}
	
	/**
	*	@method size - get the size of the selectedItem 
	*/
	this.size = function(){
		return selectedItem.length;
	}
	
	/**
	*	@method items - get all the item in the selectedItem
	*/
	this.items=function(){
		return selectedItem;
	}
		
	
	/**
	*	@method itemSize - return the number of li elements from the drive
	*/
	this.itemSize=function(){
		return $('#fileSystem li').length;
	}
	
	/**
	*	@mthod removeSelectecItem - clear all data to be sent to the server
	*								and set selectedItem to empty
	*/
	this.removeSelectedItem=function(){
		$('#toDownload_item').attr('value','');
		$('.resName').remove();
		$('.delName').remove();
		$('.revName').remove();
		$('.donName').remove();
		selectedItem = [];
	}
	
	/**
	*	@method removeReadyItem - removing selected data to be sent to the server
	*	@parameter object - drive item
	*/
	this.removeReadyItem=function(element){
		var n = $Util.getDriveItemName(element);
		var rev,del,res,don;
		var len = $('.revName').length || $('.delName').length || $('.resName').length || $('.donName').length;
		
		for(var i=0;i<len;i++){
			rev = $($('.revName').get(i));
			del = $($('.delName').get(i));
			res = $($('.resName').get(i));
			don = $($('.donName').get(i));
			
			if( rev.attr('value') == n || res.attr('value') == n || del.attr('value') == n || don.attr('value') == n){
				rev.remove();
				del.remove();
				res.remove();
				don.remove();
				break;
			}
		}
		
		for(var i=0;i<selectedItemSize();i++){
			if(selectedItem[i].name === n){
				selectedItem.splice(i,1);	
			}
		}
		
		if(!this.empty()){
			$('#toDownload_item').attr('value',selectedItem[0].name);
                        $('button[name=dlArchive]').attr('value',selectedItem[0].name);
                        $('button[name=deleteArchive]').attr('value',selectedItem[0].name);
		}else{
			$('#toDownload_item').attr('value','');
                        $('button[name=dlArchive]').attr('value','');
                        $('button[name=deleteArchive]').attr('value','');
		}
	}
	
	
	/**
	*	@method empty - check if selectecItemSize is empty and return true,otherwise false
	*/
	this.empty = function(){
		return (selectedItemSize() == 0);
	}
	
	/**
	*	@method selectedItemSize - return the size of selectedItem
	*/
	function selectedItemSize(){
		return selectedItem.length;
	}
	
	/**
	*	@method isItemNameExist - check if a drive item is already exist and return true,otherwise false
	*	@parameter string - drive item name
	*/
	function isItemNameExist(name){
		var res=false;
		for(var i=0;i<selectedItemSize();i++){
			if(name == selectedItem[i].name){
				res= true;
				break;
			}
		}
		return res;
	}
	
	/**
	*	@method initItemIntoHTMLInput - creating input elements to be use to sent it to the server
	*	@parameter string - drive item name
	*/
	function initItemIntoHTMLInput(name){//for delete as for now
		var removeItem = '<input class="revName" type="hidden" style="display:none;" name="remove[]" value="'+name+'"/>';
		var deleteItem = '<input class="delName" type="hidden" style="display:none;" name="delete[]" value="'+name+'"/>';
		var restoreItem = '<input class="resName" type="hidden" style="display:none;" name="restore[]" value="'+name+'"/>';
		var downloadItem = '<input class="donName" type="hidden" style="display:none;" name="download[]" value="'+name+'"/>';
                
		$('#restore_button').before(restoreItem);
		$('#delete_button').before(deleteItem);
		$('#remove_button').before(removeItem);	
		/* $('#toDownload_item').attr('value',name); */
		$('#download_button').before(downloadItem);
		$('button[name=dlArchive]').attr('value',name);
		$('button[name=deleteArchive]').attr('value',name);
                
	}
};//EOC Upload

/**
*	@Object Upload - Uploads the selected file to the server
*	@Parameter file object
*/
var Upload=function(file){
	
	var fileName = file.name;//holds the file name
	var fileSize = file.size;//holds the file size
	var fileType = file.type;//holds the file type
	var id = (fileName.substr(0,fileName.indexOf('.'))+fileSize).replace(/\s/g,'').replace(/_/g,'').replace(/\(/g,'').replace(/\)/g,'');//generates a unique id selector
	var uploadEvent;//holds the ajax object
	var toUpload = new FormData();//use to hold the file to be able to be uploaded using ajax
	var overw = false;//flag for file to be overwrite
	var states = ['uploading..','verifying file...'];
        
	parseFile();
	
	/**
	*	@method parseFile - setting the file information into the LIElement 
	*/
	function parseFile(){
		toUpload.append('file[]',file);
		var layout = '<li class="file '+fileName.substr(fileName.lastIndexOf('.')+1,fileName.length) +'" style="border-bottom:1px solid #555;">Name : <span id="driveItem">'+fileName+'</span>'
					+'<br/>Size : <span id="fsize'+id+'">'+$Util.bytesToSize(file.size,2)+'</span>'
					+'<br/><span class="upload_prop upload_flash" id="percent'+id+'">0%</span>'
					+'<button style="padding:2px;float:right;" class="orange" id="'+id+'">Cancel</button>'
					//+'<div class="upload_flash"></div>'
                                        +'</li>'; //<progress id="progress'+id+'" value="0" max="100"></progress>
		$('#upload_file #fileSystem').prepend(layout);
		
		setEvent();
		
		if(!$Util.notExceedSize(fileSize)){
			$('#percent'+id).removeClass();
			$('#percent'+id).addClass('upload_cancel');
			$('#percent'+id).html('&#9888; exceeds the size requirement');
			$('#fsize'+id).css({'color':'#FF0000','font-style':'italic','font-decoration':'underline'});
			$('#'+id).text('Remove');
			return;
		}
		
		/* if( fileName,indexOf('.') === -1 ){
			$('#percent'+id).removeClass();
			$('#percent'+id).addClass('upload_cancel');
			$('#percent'+id).html('&#9888; exceeds the size requirement');
			$('#fsize'+id).css({'color':'#FF0000','font-style':'italic','font-decoration':'underline'});
			$('#'+id).text('Remove');
			return;
		} */
		
		uploadEvent = ajaxUpload(toUpload);
                ajaxdone();
	}
	
	/**
	*	@method ajaxdone - used to when an ajax has been done and process 
	*						the result
	*/
	function ajaxdone(){
		uploadEvent.done(function(result,statusText,xhr){
						console.log(result);
						var ress = (JSON.parse(result));
                                                
						if(overw){
							$('#'+id).css({display:'block'});
						}
						
						if(statusText == 'success'){
							$('#percent'+id).removeClass();
							switch(parseInt(ress[0])){
									case 0:         $('#percent'+id).addClass('upload_done');
													$('#percent'+id).html('&#10003; Successfully uploaded.');
													display(ress[1]);
													break;
									case 1:	
													break;
									case 2:
													break;
									case 3:	
													break;
									case 4:         $('#percent'+id).addClass('upload_cancel');
													$('#percent'+id).html('&#9888; File exceeds the size requirement.');
													break;
									case 6:	
													break;
									case 7:	
													break;
									case 8:	
													break;
									case 9:         $('#percent'+id).addClass('upload_cancel');
													$('#percent'+id).html('&#9888; File type is not allowed in the system.');
													break;
									case 10:        $('#percent'+id).addClass('upload_cancel'); 
													$('#percent'+id).html('&#9888; Detected as a malicious file, please check the file.');	
													break;
									case 11:        $('#percent'+id).addClass('upload_cancel');
													$('#percent'+id).html('&#9888; Insufficient storage space.');	
													break;
									case 12:        $('#percent'+id).addClass('upload_cancel');
													$('#percent'+id).html('&#127748; File already exist.');
													overw = true;
													$('#'+id).after('<button class="blue" style="padding:2px;float:right;" id="'+id+'">Overwrite</button>');	
													break;
									case 13:        $('#percent'+id).addClass('upload_cancel');
													$('#percent'+id).html('&#59140; Unknown file type.');;
							}                        
						 }else{
							 $('#percent'+id).html('&#59140; An error occurred while uploading.');;
						 }
                        
                        if(!$('#percent'+id).hasClass('upload_prop')){
                                $('#'+id).text('Remove');
                        }
		});
	}
	
	/**
	*	@method setEvent - use to set the button events 
	*/
	function setEvent(){
		$('#upload_file').on('click','#'+id,function(){
			var str = $(this).text();
			
			if(str == 'Remove'){
				$(this).parent('li').remove();
			}
			if(str == 'Cancel'){
				ajaxAbort(fileName,true);
			}
			if(str == 'Overwrite'){
				$(this).siblings('#'+id+'.orange').css({display:'none'});
				$('#progress'+id).attr({value:0});
				$('#percent'+id).removeClass('upload_cancel');
				$('#percent'+id).addClass('upload_prop');
				$('#percent'+id).addClass('upload_flash');
				$(this).remove();
				
				//upload the file that will over write 
				toUpload = new FormData();
				toUpload.append('file[]',file);
				toUpload.append('overwrite',true);
				uploadEvent = ajaxUpload(toUpload);
				ajaxdone();
			}
		});
	}
	
	/**
	*	@method ajaxUpload - use to uplaod the file to the server
	*	@parameter object -  holds all the file information
	*/
	function ajaxUpload(formdata){
              
		return $.ajax({
				url: $Util.url,
				type: "POST",
				async :true,
				cache: false,
				contentType: false,
				processData: false,
				data:formdata,
                                beforeSend:function(xhr){
                                    if(xhr.readyState != 4){
                                        $('#percent'+id).text(states[0]);
                                    }
                                },
				xhr: function() {  // custom xhr
                                        var myXhr = $.ajaxSettings.xhr(); //new XMLHttpRequest()||new ActiveXObject("Microsoft.XMLHTTP");//$.ajaxSettings.xhr();  

                                        myXhr.onloadstart = function(e){
                                            $('#percent'+id).text(states[1]);
                                        }
                                        
                                        /*if(myXhr.upload){
                                            myXhr.upload.onprogress = function(e){
                                               console.log(e.loaded);
                                            }
                                        } */                                      
                                        return myXhr;
				}});
	}
       
	/**
	*	@method ajaxAbort - use to abort the file being upload
	*	@parameter 	string - name of the file
	*				boolean - default true, use as an indicator to the server
	*/
	function ajaxAbort(name,abort){
		$('#percent'+id).text('Aborting...');
		$('#'+id).text('...');
		
		uploadEvent.abort();
		
		var abort = $.post($Util.url,{del:name,overwrite:overw});
		abort.done(function(data){
			if(JSON.parse(data)){
				$('#percent'+id).removeClass();
				$('#percent'+id).addClass('upload_cancel');
				$('#percent'+id).html('&#10006; Upload Cancelled');
				$('#'+id).text('Remove');
			}	
		});
	}
	
	/**
	*	@method display - use to format the file information and 
	*						render it to the page
	*/	
	function display(relpath){
		var date = new Date().toString();
		var regularUrl = $('#dirPath').attr('value');
		var editorUrl = regularUrl.replace('drive','editor');
		var extn=fileName.substr(fileName.lastIndexOf('.')+1,fileName.length);
		var url = $Util.useEditor(extn)?regularUrl.substr(0,regularUrl.indexOf('/drive'))+'/editor':regularUrl;
		var realName = fileName;
                if (fileName.length > 25) {
                  fileName = fileName.substr(0, 25);
                }
		if(!overw){
			var newAppend = '<a  href="'+url+'/'+fileName+'">'
							+'<li class="file '+ extn.toLowerCase() +' appended" data-rel-path="'+relpath+'/'+fileName+'" title="'+realName+'">Name : <span id="driveItem">'+fileName+'</span>'
							+'<input type="hidden" value="'+fileSize+'" id="itemSize">'
							+'<br/>Size : <strong>'+$Util.bytesToSize(file.size,2)+'</strong>'
							//+'<br/>Last Modified: <strong>'+date.substr(0,date.indexOf('G')-1)+'.</strong>'
							+'</li></a>';
			$('#dropzone #fileSystem').prepend(newAppend);
			liEvent();
			DriveView.driveItems.push({liElement:'<a  href="'+url+'/'+fileName+'">'+'<li class="file '+ extn +'"  title="'+realName+'">Name : <span id="driveItem">'+fileName+'</span>'+'<input type="hidden" value="'+fileSize+'" id="itemSize">'+'<br/>Size : <strong>'+$Util.bytesToSize(file.size,2)+'</strong>'+'<br/>Last Modified: <strong>'+date.substr(0,date.indexOf('G')-1)+'.</strong>'+'</li></a>',
								itemName:fileName,
								itemType:"file"
							  });
		}else{
			var liElement;
			for(var i=0;i<DriveView.driveItems.length;i++){
				if(fileName == DriveView.driveItems[i].itemName){
					liElement=DriveView.driveItems[i].liElement;
					break;
				}
			} 
			for(var i=0;i<$('#fileSystem li').length;i++){
				if(fileName == $($('#fileSystem li').get(i)).find('#driveItem').text()){
						$($('#fileSystem li').get(i)).remove();
						$('#dropzone #fileSystem').prepend(liElement);
						$($('#fileSystem li').get(0)).addClass('appended');
					break;
				}	
			}
		}
	}
};//EOC Upload
	
/**
*	@Object $Util - An auxilliary object 
*/
var $Util = {
	arrImgFiles:['img','gif','jpg','jpeg','png','bmp'],//list of images that the system supported
	url: $('#url').val(),//holds the url of the current page
	knownFileTypes:'',// holds known file type that the code editor recognize
	isImageFile:function(name){ // use to check the drive item if it is an image and it will return true,otherwise false
				var ext = name.substr(name.lastIndexOf('.')+1,name.length);
				var result=false;
				for(var i=0;i<this.arrImgFiles.length;i++){
					if(ext == this.arrImgFiles[i])
						result=true;
				}
				return result;
			},
	getDriveItemName:function(element){//gets the name of the drive item
				return $(element).attr('title');
			},
	getDriveItemSize:function(element){//gets the size of the drive item
				return $(element).find('#itemSize').text()==''?'FOLDER':$(element).find('#itemSize').text();
			},
	getDriveItemType:function(element){//gets the type of the drive item (folder or file)
				return $(element).hasClass('file')?'file':'folder';
			},
	bytesToSize:function(bytes, precision){//convert byte(s) into KB,MB,GB, or TB
					var kilobyte = 1024;
					var megabyte = kilobyte * 1024;
					var gigabyte = megabyte * 1024;
					var terabyte = gigabyte * 1024;
								
					if ((bytes >= 0) && (bytes < kilobyte)) {
						return bytes + ' B';
								
					} else if ((bytes >= kilobyte) && (bytes < megabyte)) {
						return (bytes / kilobyte).toFixed(precision) + ' KB';
								
					} else if ((bytes >= megabyte) && (bytes < gigabyte)) {
						return (bytes / megabyte).toFixed(precision) + ' MB';
								
					} else if ((bytes >= gigabyte) && (bytes < terabyte)) {
						return (bytes / gigabyte).toFixed(precision) + ' GB';
								
					} else if (bytes >= terabyte) {
						return (bytes / terabyte).toFixed(precision) + ' TB';
								
					} else {
						return bytes + ' B';
					}
				},
	notExceedSize:function(size){//check if the file size to be uploaded does not exceed to the predefiend max size and it will return true, otherwise false
					if ( size > parseInt($('input[name=MAX_FILE_SIZE]').val()) ){
						return false;
					}
					return true;
				},
	getKnownFileTypes: function (){//retrive list of file that the code editor supported
		return $.post('/known_file_types');
	},
	useEditor:function(item){ //checks if an drive item can be editted and it will return true,otherwise false
		var result = false;
			for(var i=0;i<$Util.knownFileTypes.length;i++){
				if(item == $Util.knownFileTypes[i]){
					return true;
					break;
				}
			}
		return result;
	},
	getBrowserName:function(){ // gets the browser name
		var nVer = navigator.appVersion;
		var nAgt = navigator.userAgent;
		var browserName  = navigator.appName;
		var fullVersion  = ''+parseFloat(navigator.appVersion); 
		var majorVersion = parseInt(navigator.appVersion,10);
		var nameOffset,verOffset,ix;
		
		// In Opera, the true version is after "Opera" or after "Version"
		if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
			browserName = "Opera";
			fullVersion = nAgt.substring(verOffset+6);
			if ((verOffset=nAgt.indexOf("Version"))!=-1) 
			fullVersion = nAgt.substring(verOffset+8);
		}
		// In MSIE, the true version is after "MSIE" in userAgent
			else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
			browserName = "Microsoft Internet Explorer";
			fullVersion = nAgt.substring(verOffset+5);
		}
		// In Chrome, the true version is after "Chrome" 
		else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
			browserName = "Chrome";
			fullVersion = nAgt.substring(verOffset+7);
		}
		// In Safari, the true version is after "Safari" or after "Version" 
		else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
			browserName = "Safari";
			fullVersion = nAgt.substring(verOffset+7);
			if ((verOffset=nAgt.indexOf("Version"))!=-1) 
			fullVersion = nAgt.substring(verOffset+8);
		}
		// In Firefox, the true version is after "Firefox" 
		else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
			browserName = "Firefox";
			fullVersion = nAgt.substring(verOffset+8);
		}
		// In most other browsers, "name/version" is at the end of userAgent 
		else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/')) ) {
			browserName = nAgt.substring(nameOffset,verOffset);
			fullVersion = nAgt.substring(verOffset+1);
			if (browserName.toLowerCase()==browserName.toUpperCase()) {
				browserName = navigator.appName;
			}
		}
		// trim the fullVersion string at semicolon/space if present
		if ((ix=fullVersion.indexOf(";"))!=-1)
			fullVersion=fullVersion.substring(0,ix);
		if ((ix=fullVersion.indexOf(" "))!=-1)
			fullVersion=fullVersion.substring(0,ix);
		
		majorVersion = parseInt(''+fullVersion,10);
		if (isNaN(majorVersion)) {
			fullVersion  = ''+parseFloat(navigator.appVersion); 
			majorVersion = parseInt(navigator.appVersion,10);
		}
		
		return browserName;
	}
};