<?php

require_once PROCESS.'StudentProcess.php';
require_once LIBRARY.'Editor.php';
abstract class EditorProcess extends Process {
	private $editor;

	private $root;
	public function run() {


            
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('jquery.modal.min.js');
		$editor = null;
		$user = Session::getUser();
		$root = DRIVE.$user['userId'];
		$uri = $this->request->params();
		if (count($uri) > 1) {
			$uri[1] = '/'.urldecode($uri[1]);
			$uri[1] = rtrim($uri[1], '/');

			$filePath = $root.$uri[1];
			if (!file_exists($filePath))
				$this->response->redirect('/404');
			$file = new File($filePath);
			$this->editor = new Editor($this->page, $file);
		} else {
			$this->editor = new Editor($this->page);
		} // end initialization
		
		if (Input::post('btnSave') != null) {
			$this->editor->overwriteFile(Input::post('codeEditor', false));
			$this->page->addAlert('File saved', 'success');
		}		

		$this->editor->getEditorContents($uri[0]);
	
	}
} 