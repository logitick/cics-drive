<?php

require_once PROCESS.'StudentProcess.php';
/**
 * @package process
 * @subpackage student
 */
class EditAccount extends StudentProcess {
	
        private $firstName;
        private $middleInitial;
        private $lastName;
        private $avatarFile;
        
        public function run() {
		parent::run();
                
                if(Session::get('addQuestion')){
                    $this->response->redirect(SITE_URL.'student/drive');
                    die();
                }
                
		if (Input::hasPost()) {
                    $this->updateProfile();
		}
		
		$pageData['user'] = $this->getUser();
		$this->page->setTitle('Edit Account - CICS Drive');
		$this->page->setContent('student/edit_account.php', $pageData);
		echo $this->page;
	}

	private function updateProfile() {
		$this->firstName = Input::post('firstName');
		$this->middleInitial = Input::post('middleInitial');
		$this->lastName = Input::post('lastName');
		
     
		
                if ($this->middleInitial != null && !empty($this->middleInitial)){
					if(ctype_alnum($this->middleInitial)){
						$this->initial = strtoupper(substr($this->middleInitial,0,1));
						$this->getUser()->setMiddleInitial($this->initial);
					}else{
						$this->page->addAlert('Special character has been is not allowed.');
					}
                }
		
        
		
                if (Input::hasFile() && $_FILES['avatar']['error'] != 4) {
			$avatar = Input::file('avatar');
			$ext = pathinfo($avatar['name'], PATHINFO_EXTENSION);
			if($ext=='jpeg' || $ext=='jpg' || $ext=='png'|| $ext=='gif'){
				if (!empty($avatar['name'])) {
					$userfile_name = $avatar['name'];
									$extn = substr($userfile_name, strripos($userfile_name, '.')+1);
					$fileName = $this->getUser()->getUserId().'.'.$extn;
					
					if ($this->getUser()->getAvatarFileName() != null) {

						$oldAvatar = ROOT.'media/images/avatars/'.$this->getUser()->getAvatarFileName();
						if (file_exists($oldAvatar))
							unlink($oldAvatar);
					}
					move_uploaded_file($avatar['tmp_name'], ROOT.'media/images/avatars/'.$fileName);
                                
				$this->getUser()->setAvatarFileName($fileName);
                                $this->avatarFile = $userfile_name; 
                                
                                require_once LIBRARY.'Image.php';
                                $image = new Image(ROOT.'media/images/avatars/'.$fileName);
                                $image->createThumbnail(40, 40, ROOT.'media/images/avatars/'.$fileName);
				}
			} else {
              $this->page->addAlert('Unacceptable image format', 'notice');
              return;
            }
			
		}
		
		$a = $this->getUser()->updateRecord();
		if ($a){
			$this->page->addAlert('Your account was successfuly updated', 'success');
                        $this->setUserLog();
                }else
			$this->page->addAlert('Error during update.', 'error');
		
	}
        
        /**
         *  setUserLog
         */
        private function setUserLog(){
            UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_UPDATE, $this->getUser()->getUserID());
        }
}