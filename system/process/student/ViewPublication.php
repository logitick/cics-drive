<?php

require_once PROCESS.'StudentProcess.php';
require_once MODEL.'Publication.php';
require_once LIBRARY.'Publish.php';
require_once LIBRARY.'drive/PublishedFile.php';
require_once LIBRARY.'drive/PublicationsFolder.php';
class ViewPublication extends StudentProcess{ 
  private $lib;
  public function run() {
    parent::run();
    
    if(Session::get('addQuestion')){
        $this->response->redirect(SITE_URL.'student/drive');
        die();
    }
                
    $publicationID = $this->request->id;
    $publication = Publication::getById($publicationID);
    $file = PublishedFile::factory($publication);
    $filePath = DRIVE.$publication->getUserID().'/'.  PublicationsFolder::FOLDER_NAME.$publication->getFilePath();
      $muted = $publication->getMutes();
      $isUserMuted = false;
      if ($muted != null) {
        foreach ($muted as $mutedUser) {
          if ($this->getUser()->getUserID() == $mutedUser->getUserID()) {
            $isUserMuted = true;
          }
        }
      }
      if (Input::post('txtAddComment') != null) {
        if ($this->addComment() && !$isUserMuted) {
          $this->page->addAlert('Comment added', 'success');
        } else {
          
          if ($isUserMuted) {
            $this->page->addAlert('You have been muted by the instructor', 'error');
          } else {
            $this->page->addAlert('Cannot add comment as this time', 'error');
          }
        }
      }
      

    
    if (file_exists($filePath)) {
      $file = File::createDriveItem($filePath);
      
      $this->lib = new Publish($this->page, $publication, $file, '/student/publication.php');
      
      if (Input::post('removeComment') != null) {
          $comment = Comment::getById(Input::post('removeComment'));
          $status = false;
          if ($comment->getUserID() == $this->getUser()->getUserID()) {
            $status = $this->lib->removeComment(Input::post('removeComment'));
          }
          if ($this->request->isAjax()) {
            $arr['status'] = $status;
            echo json_encode($arr);
            die();
          }
          
          if ($status) {
            $this->page->addAlert('Comment removed', 'success');
          } else {
            $this->page->addAlert('Cannot remove comment at this time', 'warning');
          }
        }
      
      
      $pageData = $this->lib->getPageData();
      $pageData['comments'] = $this->renderComments();
      $pageData['muted'] = $isUserMuted || $this->getUser()->getStatus() == User::STATUS_SUSPICIOUS;
      $this->page->setContent('/student/publication.php', $pageData);
    }
    echo $this->page;
  }
  
  public function renderComments() {
        $comments = $this->lib->getComments();
        $str = '';
        if ($comments == null) {
          return $str;
        }
        foreach ($comments as $comment) {
          $user = User::getByID($comment->getUserID());
          if ($user == null) {
            continue;
          }
          $commentStr = $comment->getComment();
          if ($comment->getStatus() == Comment::STATUS_INACTIVE) {
            $commentStr = '[comment removed]';
          }
          $str .= '<div class="commentContainer">';
          if ($this->getUser()->getUserID() == $comment->getUserID()) {
            $str .= '<a href="#comment_'.$comment->getCommentID().'" class="deleteComment" data-comment-id="'.$comment->getCommentID().'" id="comment_'.$comment->getCommentID().'"><span class="icon icon-dark">❎</span></a>';
          }
          
          $str .= '<div class="owner"><img src="'.$user->getAvatarUrl().'" width="40" height="40"> '.$user->getFirstName().' '.$user->getLastName().'</div>';
          $str .= '<div class="date">'.$comment->getCommentDate()->format('l, M d, Y - h:i a').'</div>';
          $str .= '<div class="comment">'.$commentStr.'</div>';
          $str .= '</div>';
        }
        return $str;
  }
  
  public function addComment() {
    $comment = Input::post('txtAddComment');
    $inst = Comment::factory($this->getUser()->getUserID(), $this->request->id, $comment);
    return $inst->storeRecord();
  }
}
