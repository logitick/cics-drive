<?php

require_once PROCESS.'StudentProcess.php';
require_once MODEL.'Studentinfo.php';
/**
 * @package process
 * @subpackage student
 */
class EditQuestions extends StudentProcess {
	private $resultQuestion;
	private $answers = array();
	private $sid = array();
	private $ID;
	
	
	private $listOfQuestions;
	private $question = array();
	private $answer = array();
	private $questionID = array();
	
	public function run() {
		parent::run();
		
                if(Session::get('addQuestion')){
                    $this->response->redirect(SITE_URL.'student/drive');
                    die();
                }
                
		$pageData['user'] = $this->getUser();
		$this->ID = substr($pageData['user']->getUserID(), -5);
		
                
                
		if(!empty($_POST) && isset($_POST) )
		{	
			$c = 0;
			 foreach($_POST as $key => $a){
				if($c%2 == 0){
					$this->sid[] = $a;
				}
				else
				{
					$this->answers[] = $a;
				}
				++$c;
			}
			
			if($this->updateAnswer() == 1){
				$msg = 'Security answer has been succesfully updated!.';
				$this->page->addAlert($msg,'success');
			}		
		}
		 
        $this->listOfQuestions = Studentinfo::resetAnswer($this->ID);   
		$pageData['questions'] = $this->listOfQuestions;
		
		$this->page->setTitle('Edit Security Questions - CICS Drive');
		$this->page->setContent('student/edit_security_question.php', $pageData);
		echo $this->page;
	
	}
	public function updateAnswer(){
              
		$ctr = count($this->sid);
		$res = false;
		foreach($this->sid as $key => $SecId){
			if(trim($this->answers[$key]!=null)){
				if(!empty($this->answers[$key])){   
                                    $this->setUserLog($this->listOfQuestions[$key]['question']);
                                    $res = Studentinfo::updateAnswer($this->ID,$this->answers[$key],$SecId);
                                    $a = true;
				}
			}
		}
		if($res){
			$msg = 'Security answer has been succesfully updated!.';
			$this->page->addAlert($msg,'success');
		}else{
			return $res = false;
		}
	}
        
        /**
         *  @method setUserLog
         */
        public function setUserLog($value=null){
            if(!empty($value)){
                $actid = $this->setUserActivityLog(UserActivityLogs::CATEGORY_SECURITY_QUESTION_ANSWER_CHANGE, $this->getUser()->getUserID());
                $this->setUserActivityProperties($actid, UserActivityProperties::CHANGE_SECURITY_ANSWER, $value);
            }
        }
}