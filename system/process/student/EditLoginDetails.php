<?php
require_once MODEL.'User.php'; 
require_once PROCESS.'StudentProcess.php';
/**
 * @package process
 * @subpackage student
 */
class EditLoginDetails extends StudentProcess {
        private $temp;
        
	public function run() {
		parent::run();
		
                if(Session::get('addQuestion')){
                    $this->response->redirect(SITE_URL.'student/drive');
                    die();
                }
                
                $this->temp = $this->getUser()->getEmail();
                
		if ( Input::hasPost() ) {
                    $this->updateProfile();
		}
                
		$pageData['user'] = $this->getUser();
		$this->page->setTitle('Edit Login Details - CICS Drive');
		$this->page->setContent('student/changeAccount_details.php', $pageData);
		echo $this->page;
	}

	private function updateProfile() {

 		$email = trim(Input::post('email'));
                $c_pass = trim(Input::post('c_password'));
                $n_pass = trim(Input::post('n_password'));
                $nc_pass = trim(Input::post('cn_password'));
                $msg='';

                if(!filter_var($email, FILTER_VALIDATE_EMAIL) && !empty($email)){
                    $this->page->addAlert('Invalid Email address','notice');
                    return;
                }else if( $this->getUser()->getEmail() != $email && User::existingEmail($email) > 0 ){
                    $this->page->addAlert('Email address already exists','notice');
                    return;
                }else if( empty($c_pass) ){
                    //return;
                }
                
                if(!empty($c_pass) && (md5($c_pass) != $this->getUser()->getPassword()) ){
                    $this->page->addAlert('Invalid Password','notice');
                    return;
                }  else if ( !empty($c_pass) && (empty($n_pass) || empty($nc_pass)) ) {
                    $this->page->addAlert('Fill up new password/confirm password.','notice');
                    return;
                }
                
                if((!empty($c_pass) || !empty($nc_pass)) && ($n_pass !== $nc_pass)){
                    $this->page->addAlert('New password does not match','notice');
                    return;
                }else if(!empty($c_pass)  && (strlen($n_pass)<6 || strlen($nc_pass)<6) ){
                    $this->page->addAlert('Password must not be less than 6 characters','notice');
                    return;
                }else{
                    $this->getUser()->setPassword($nc_pass);
                }
                
                if(( $email != $this->getUser()->getEmail() && empty($nc_pass) )){
                    $msg = 'Email has been successfully changed.';    
                }else if(strlen($nc_pass) > 0 && $nc_pass==$n_pass && $email == $this->getUser()->getEmail()){
                     $msg = 'Password has been successfully changed.';   
                }else if(($email!=$this->getUser()->getEmail()) && ($nc_pass == $n_pass)){
                    $msg = 'Email and Password has been successfully changed';
                }else {
                   $this->page->addAlert('Nothing was updated','notice');
                  return;
                }
                
                $this->getUser()->setEmail($email);
                
                if($this->getUser()->updateRecord()){
                    /* $this->setUserLog($email, $nc_pass); */
                    $this->page->addAlert( $msg,'success' );
                }else{
                    $this->page->addAlert('Updated failed','notice');
                }
                
        }   
        
        /**
         * setUserLog
         */
        public function setUserLog($email=null,$pass=null){
            
            if( $email!=null && !empty($email) && $email!=$this->temp ){
                $actid = $this->setUserActivityLog(UserActivityLogs::CATEGORY_CHANGE, $this->getUser()->getUserID());
                $this->setUserActivityProperties($actid, UserActivityProperties::CHANGE_EMAIL, $email);
            }
            if($pass!=null && !empty($pass)){
                $actid = $this->setUserActivityLog(UserActivityLogs::CATEGORY_CHANGE, $this->getUser()->getUserID());
                $this->setUserActivityProperties($actid, UserActivityProperties::CHANGE_PASSWORD, md5($pass));
            }
            
        }
	
}