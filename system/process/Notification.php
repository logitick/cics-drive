<?php
  header("Content-Type: text/event-stream");
  header("Cache-Control: no-cache");
  header("Access-Control-Allow-Origin: *");
  
  require_once MODEL.'User.php';
  require_once MODEL.'User.php';
  require_once MODEL.'';
  
  class Notification{
      
      private $lastEventId;
      
      private $notification;
      
      private $delay;
      
      public function __construct($delay=null) { 
          $this->setDelay($delay);
          $this->lastEventId = $this->generateLastEventID();
          $this->setNotification("sample server sent");
      }
      
      public function generateLastEventID(){
          $i = $this->lastEventId;
          $c = $i + 100;
          return $i;
      }
      
      public function checkDataUpdates(){
          
      }
      
      public function setNotification($note){
          $this->notification = $note;
      }
      
      public function getNotification(){
          return "data:".$this->notification."\n";
      }
      
      public function getLastEventID(){
          return "id:".$this->lastEventId."\n";
      }
      
      public function setDelay($delay){
          $this->delay = $delay;
      }
      
      public function getDelay(){
          return "retry:".$this->delay."\n";
      }
      
      public function __toString(){
          return $this->getDelay().$this->getLastEventID().$this->getNotification()."\n";
      }
  }
 