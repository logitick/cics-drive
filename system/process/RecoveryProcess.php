<?php
require_once LIBRARY.'Input.php';
require_once MODEL.'User.php'; 
require_once MODEL.'Studentinfo.php'; 
class RecoveryProcess extends Process{

	private static $state;
	private $answer;
	
	public function run() {
	
		if( $this->request->uri() != '/recovery' AND $this->request->uri() != '/recovery?state=0' AND $this->request->uri() != '/recovery?state=1'){ 
			$this->getQuestion();
			die();
		}
		$this->stepOne();
		$this->stepTwo();
		$this->stepThree();
		$this->back();
		
		$this->page->setTitle('Password Recovery - CICS Drive');
		$this->page->setHeader('guest/header.php');
		$this->page->setFooter('guest/footer.php');
		$this->page->addAsset('change_question.js');

		switch (RecoveryProcess::$state) {
			case 3:
                                $userSession = Session::get('recovery');
                                $id = $userSession['userID'];
                                
				$this->recovery();
				$alert['message'] = 'Password recovery successful. You may now login.';
				$alert['type'] = 'success';
				Session::setFlash('alert', $alert);
                                
                                $uact = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_PASSWORD_RECOVER, $id);
                
				$this->response->redirect(SITE_URL.'login');
				break;
			case 2:
				$this->page->setContent('guest/recovery3.php');
				break;
			case 1:
				$templateData['inputName'] = md5(time());
				$this->page->setContent('guest/recovery2.php', $templateData);
				break;
			default:
				$this->page->setContent('guest/recovery.php');
		}
		
		echo $this->page;

	}
	
	/**
	*
	*	stepOne
	*
	*/
	private function stepOne(){
		$userSession = null;
		if(Input::post('remail')!=null && Input::post('ridNumber')!=null){
			$email = Input::post('remail');
			$id = Input::post('ridNumber');
			
			User::getRecoveryValidation($id,$email);
			
			$userSession = Session::get('recovery');
			// debug_print_r($userSession);
			if($userSession){
				self::$state = 1;
			}else if($userSession['idNumber'] != null){
				$this->page->addAlert('Invalid Email Address');
			}else{
				$this->page->addAlert('Invalid ID Number or Email Address');
			}
		//self::$state = 1;
		}
	}
	
	/*
	*
	*	stepTwo
	*
	*/
	private function stepTwo(){
		$userSession = Session::get('recovery');
		//debug_print_r($userSession);
		if(Input::post('txtInputName')!=null && Input::post(Input::post('txtInputName')) != null){
			$answer = Input::post(Input::post('txtInputName'));
			if($answer == $userSession['answer']){
				self::$state = 2;
			}else{
				$this->page->addAlert('Incorrect Answer');
			}
		}
	}
	
	/*
	*
	*	stepThree
	*
	*/
	private function stepThree(){
		$userSession = Session::get('recovery');
		$id = $userSession['idNumber'];
		if(Input::post('password') != null && Input::post('vPassword') != null){
			$password = Input::post('password');
			$vpassword = Input::post('vPassword');
			
			if($password == $vpassword){
			
			$userID = $userSession['userID'];
			$emails = $userSession['email'];
			//$password = $userSession['password'];
			$idNumber = $userSession['idNumber'];
			$firstName = $userSession['firstName'];
			$middleInitial = $userSession['middleInitial'];
			$lastName = $userSession['lastName'];
			$usedStorage = $userSession['usedStorage'];
			$userType = $userSession['userType'];
			$avatarFileName = $userSession['avatarFileName'];
			$status = $userSession['status'];
			
			$user = new User($userSession);
			$user->setStatus($status);
			$user->resetPassword($vpassword,$userID);
			self::$state = 3;
			
			}else{
			$this->page->addAlert('Password did not match');
				self::$state = 2;
			}
		}
	}
	
	/**
	*	recovery
	*	
	*/
	private function recovery() {
		$userSession = Session::get('recovery');
		
		 // if(User::recovery($userSession) ){
			Session::remove('recovery');
		// } 
	}
	
	/**
	*
	*	back
	*/
	private function back(){
		if(Input::get('state') != null){
			self::$state = Input::get('state');
			if( Input::get('state') == 0){
				Session::remove('recovery');
			}
		}
	}
	
}