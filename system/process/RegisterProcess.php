<?php
require_once LIBRARY.'Input.php';
require_once MODEL.'User.php'; 
require_once MODEL.'StudentsQuestions.php'; 
require_once MODEL.'Studentinfo.php'; 
class RegisterProcess extends Process{

	private static $state;
	private $answer;
	
	public function run() {
		
		if( Input::post('change')!=null ){
			$val = $this->changeQuestion( (int)Input::post('change') );
			
                        die( json_encode($val) );
		}
		
		if( $this->request->uri() == '/register?state=1' ){
			self::$state = 1;
                        unset($_SESSION['registration']['question']);
		}
		
		if(isset($_POST['step1'])){
			Session::remove('registration');
			$this->stepOne();
		}
		
		if(isset($_POST['step2'])){
			$this->stepTwo();
		}
		
		if(isset($_POST['step3'])){
			$this->stepThree();
		}
		
		$this->page->setTitle('Register - CICS Drive');
		$this->page->setHeader('guest/header.php');
		$this->page->setFooter('guest/footer.php');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('change_question.js');

		switch (self::$state) {
			case 3:
					$user = Session::get('registration');
					$user = $user['userID'];
					$this->register();
					$alert['message'] = 'Registration successful. You may now login.';
					$alert['type'] = 'success';
					Session::setFlash('alert', $alert);
									
					UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_ACTIVATE, $user);

					if(!file_exists(DRIVE.$user.'/'.'Cics_Drive_Student_Manual.pdf')){
						mkdir(DRIVE.$user,0777,true);
						copy(ROOT.'Cics_Drive_Student_Manual.pdf', DRIVE.$user.'/'.'Cics_Drive_Student_Manual.pdf');
					}

					Session::remove('registration');
					$this->response->redirect(SITE_URL.'login');
					break;
			case 2:
					$pageData['user'] = Session::get('registration');
					$this->page->setContent('guest/register_3.php',$pageData);
					break;
			case 1:
					$pageData['user'] = Session::get('registration');
					$this->page->setContent('guest/register_2.php',$pageData);
					break;
			default:
					$this->page->setContent('guest/register_1.php');
		}
		
		echo $this->page;

	}
	
	/**
	* stepOne() 
	*/
	private function stepOne(){
		$userSession;
		$id = Input::post('idNumber');
		$lastName = strtolower(Input::post('lastName', false));
		
		User::getValidation($id,$lastName);
		
		$userSession = Session::get('registration');

		if( $userSession['status'] != 'A' && $userSession['idNumber'] == $id && strtolower($userSession['lastName']) == $lastName ){
			self::$state = 1;
		}else if($userSession['status'] == 'A'){
			$this->page->addAlert('Student is already registered');
			self::$state = 0;
		}else if( $userSession['lastName'] != $lastName ){
			$this->page->addAlert('Invalid Last Name');
		}else{
			$this->page->addAlert('Invalid ID Number or Last Name');
		}	
	}
	
	/*
	*	stepTwo()
	*/
	private function stepTwo(){
		$userSession = Session::get('registration');
		
		$email = Input::post('email');
		$password = Input::post('password');
		$Vpassword = Input::post('vPassword');
		
			
		if( filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			
			if( User::existingEmail($email) > 1 ){
				$this->page->addAlert('Email Address already exist');
				self::$state = 1;
			} 
			else{
				if(strlen($password)>5){
					Session::addInfo('registration','email',$email);
					
					if($password == $Vpassword ){	
					
					Session::addInfo('registration','password',Input::post('vPassword'));
					
					/*
					* ready the student security questions and displaying it to the page
					*/
					if( !isset($userSession['question']) && empty($userSession['question']) ){
						StudentsQuestions::readyQuestions();  
					}
					
					self::$state = 2;
					}
					else{
						$this->page->addAlert('Password does not match');
						self::$state = 1;
					} 
				}else{
					$this->page->addAlert('Password must be in a minimum of 6 characters');
					self::$state = 1;
					
				}
			}
		}else{
			Session::addInfo('registration','email',"");
			$this->page->addAlert('Invalid Email Address');
			self::$state = 1; 
		}
		
	}
	
	/*
	*
	*	stepThree
	*
	*/
	private function stepThree(){
		$user = Session::get('registration');
		$flag = 0;
		foreach($user['question'] as $key => $question){
			$res = trim(Input::post('Q'.$key));//PARA MA CHECK KONG SPACE BAR SAY GE INPUT
			if($res!=null){
				// $user['question'][$key]['answer'] = Input::post('Q'.$key);
				$flag = $flag + 1;
				if($flag == 5){//PARA MA TRAP NGA TANAN NGA QUESTIONS NA AY ANSWER
					foreach($user['question'] as $key => $question)
					{
						$user['question'][$key]['answer'] = Input::post('Q'.$key);
					}
					
					self::$state = 3;
					return $user;
				}
			}else{
				$this->page->addAlert('Questions must be filled');
				self::$state = 2;
				$flag = $flag - 1;
			}
		}	
	}
	
	/**
	*	register
	*	
	*/
	private function register() {
		$user = $this->stepThree();
		User::register($user);
		Studentinfo::populateStudentInfo($user['userID'],$user['question']);
                
	}
	
	/*
	*
	*	changeQuestion()
	*/
	private function changeQuestion($value){
		StudentsQuestions::changeQuestion($value);
		$question = Session::get('registration');
		return $question['question'][$value]['question'];
	}
	
}