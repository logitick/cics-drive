<?php
/**
 * @package process
 */	
 
require_once MODEL.'User.php';

class FacultyProcess extends Process {
	
        
	public function __construct(_Request $request = null , _Response $response = null) {

		parent::__construct($request, $response);
		
	}

	protected function loadDefaultTemplate() {
		
		try { 	// get the user data
			$user = Session::get('user');
			$this->setUser(User::getById($user['userId']));
			$headerData['user'] = $this->getUser();	
		} catch(UserException $e) {
			die('Error: User not found');
		}
			
		
		$headerData['notifications'] = $this->getNotifications();
		$headerData['storageindicator'] = '<div class="storageIndicator">'.
											$this->getUser()->getUsedStoragePercent().'%'.
											'<div class="innerIndicator" style="width:'.round($this->getUser()->getUsedStoragePercent()).'%;">'.
											'</div>'.
										  '</div>';
		
		
		$this->addDefaultAssets();
		$this->page->setTitle('CICS Drive');
		$this->page->setHeader('faculty/header.php', $headerData);// default header and footer for student process
		$this->page->setFooter('faculty/footer.php');
	}

	public function run() {
		$this->allowedUserType = UserType::FACULTY;
		$this->authenticate();
		$this->loadDefaultTemplate();
	}


	private function addDefaultAssets() {
		// add css assets

		// add JS assets
		$this->page->addAsset('jquery-1.9.js');
		$this->page->addAsset('custom.js');
                $this->page->addAsset('jquery.modal.js');
	}

        public function getArchiveFolder(){
            return ARCHIVE.$this->getUser()->getUserID().'/';
        }
        
        public function getArchiveFolderLog(){
            return ARCHIVE.$this->getUser()->getUserID().'/LOGS/';
        }
        
	public function getNotifications() {
		$str = '
				<li class="unread">
					<hgroup>
						<h1>An admin has removed a file from your drive.</h1>
						<h2>File: My Drive/Test/kamandag.c</h2> 
					</hgroup>
					<p><span>14:24</span></p>
				</li>
		';
		return $str;
	}
}