<?php
require_once MODEL.'User.php';
require_once MODEL.'Logins.php';
require_once MODEL.'StudentsQuestions.php';//akoa ge add para ma usab2x ang question taga sayup
require_once MODEL.'StudentInfo.php';
require_once MODEL.'StudentInfo.php';
require_once PROCESS.'Login.php';

/**
 * @package process
 */
class LoginValidation extends Process {
	
	private $question;
	private $answer;
	const ALLOWED_TIME = 20;
	const MAX_ATTEMPTS = 3;

	private function timeOut(&$validation, &$questionSet) {

	}
	
	public function run() {
		$user = Session::getUser();
		$questionSet = Session::get('questionSet'); // the set of questions from the session data
		$validation = Session::get('validationData');
		if ($validation == null)  {// initialize validation data
			$validation['errorCount'] = 0;
		}
		$current = time(); // current time
		if (isset($validation['startTime']) && $current - $validation['startTime'] > self::ALLOWED_TIME) { // check kung wala ni lapas sa allowed time
			unset($validation['startTime']); // unset ni para ma re-initialize (re-initialize before mu output sa page)
			$this->page->addAlert('You ran out of time. You have '.(self::MAX_ATTEMPTS - ++$validation['errorCount']). ' attempt(s) left.', 'warning');
			array_pop($questionSet); // tangtang sa question
			shuffle($questionSet); // randomize
		}
        
		if (Input::post('InputName') && isset($validation['startTime']) && $current - $validation['startTime'] <= self::ALLOWED_TIME) { // kung nag submit nya wala ka lapas sa time allowed
                                
			$fieldName = Input::post('InputName');
			$answer = strtolower(Input::post($fieldName)); // kuhaon ang answer (in lower case)
			if ($answer == strtolower($questionSet[count($questionSet) - 1]['answer'])) { // compare sa answer gikan sa database
                                $uid = Session::getUser();
                                $uid = $uid['userId'];
                                
                                User::removeValidationFlag();     // sakto kay tangtangon ang mga flag
				Session::remove('questionSet');
                                Session::remove('validationData');
                                
                                Login::logLogin($uid,$this->request->ip(), session_id(),$this->request->getReadyBrowser());
                                
                                Session::remove('tq');
                                $this->response->redirect(SITE_URL.'student/drive');
			} else {
				$this->page->addAlert('Wrong answer! You have '.(self::MAX_ATTEMPTS - ++$validation['errorCount']). ' attempt(s) left.', 'warning');
				unset($validation['startTime']);
				array_pop($questionSet);
				shuffle($questionSet);
                                $logID = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_MISSED_QUESTION, $user['userId']);
                                UserActivityProperties::logProperties($logID, UserActivityProperties::SECURITY_QUESTION_ID, $questionSet[count($questionSet) - 1]['studentQuestionID']);
			}
		}


		if ($validation['errorCount'] >= self::MAX_ATTEMPTS) { // security question unanswered 3 times
			$userInst = User::getByID($user['userId']); // an instance of the user
			$userInst->setStatus(User::STATUS_SUSPICIOUS);
			$userInst->updateRecord();
			$alert['message'] = 'Your account has been marked for suspicious activity. The administrators have been notified.';
			$alert['type'] = 'warning';
			Session::setFlash('alert', $alert);
			User::removeValidationFlag();
			Session::remove('questionSet');
			Session::remove('validationData');
			Session::remove('user');

			$this->response->redirect(SITE_URL);
		}

		if (!isset($user['validate']))
			$this->response->redirect(SITE_URL.'student/drive');


		$this->page->setHeader('guest/header.php');
		$this->page->setFooter('guest/footer.php');
		$this->page->setTitle('Security Question - CICS Drive');
		if ($questionSet == null) {
			$questionSet = StudentInfo::getQuestionAnswerSet($user['userId']);
			shuffle($questionSet);
		}
		$question = $questionSet[count($questionSet) - 1];
		$pageData['question'] = $validation['question'] =  $question['question'];
		$pageData['hint'] = strlen($question['answer']);
		$pageData['firstName'] = $user['firstName'];
		$pageData['inputName'] = time();
		Session::set('tq',$question['question']);
		$this->page->addAsset('security.timer.js');
	
		Session::set('questionSet', $questionSet);

		if (!isset($validation['startTime']))
			$validation['startTime'] = time();
		$current = time();
		$difference = $current - $validation['startTime'];

		$pageData['time'] = self::ALLOWED_TIME - $difference;
		Session::set('validationData', $validation);
		$this->page->setContent('guest/question.php', $pageData);
		echo $this->page;
	}
}
