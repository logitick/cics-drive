<?php

require_once PROCESS.'FacultyProcess.php';
require_once LIBRARY.'Diff.php';
require_once LIBRARY.'Diff/Renderer/Html/SideBySide.php';


class CompareProcess extends FacultyProcess {
  
  private $baseFile;
  private $compareFile;
  
  public function run() {
    parent::run();
    $filePath = trim(urldecode($this->request->param('path', '/')));
    if (substr($filePath, 0, 1) != '/') {
      $filePath = '/'.$filePath;
    }

    $userID = $this->getUser()->getUserID();
    $this->baseFile = new File(DRIVE.$userID.$filePath);
    
    if (!file_exists($this->baseFile->getPath())) {
      $this->response->redirect('/404');
    }
    
    $this->compareFile = new File(DRIVE.'dummy');
    file_put_contents($this->compareFile->getPath(), '');
    if (Input::post('compareFile')) {
      $this->compareFile = new File(DRIVE.$userID.Input::post('compareFile'));
    }
    
     $templateData['diff'] = '';
    
    $templateData['diff'] .= $this->getDiffHtml();
    $templateData['baseFileName'] = $this->baseFile->getRelPath();
    
    if ($this->compareFile->getName() == 'dummy')
      $templateData['compareFileName'] = '';
    else
      $templateData['compareFileName'] = $this->compareFile->getRelPath();
    
    $templateData['url'] = SITE_URL.'faculty/drive/';
    
    $this->page->addAsset('compare.css');
    $this->page->addAsset('jquery.modal.min.js');
    $this->page->addAsset('jquery.modal.css');
    $this->page->addAsset('compare.js');
    $this->page->setContent('faculty/compare.php', $templateData);
    echo $this->page;
  }
 
  
  public function getDiffHtml() {
    $baseFileArray = explode("\n", $this->baseFile->getContents());
    $this->compareFileArray = explode("\n",$this->compareFile->getContents());
    $max = max(count($baseFileArray), count($this->compareFileArray));

    $diff = new Diff($baseFileArray, $this->compareFileArray, array(
        'context'=>$max,

        'ignoreWhitespace' => true,
        'ignoreCase' => true
    ));
    return $diff->Render(new Diff_Renderer_Html_SideBySide);
    
  }
  
}

