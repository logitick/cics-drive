<?php

require_once PROCESS.'FacultyProcess.php';
/**
 * @package process
 * @subpackage student
 */
class EditAccount extends FacultyProcess {
	public function run() {
		parent::run();

		if (Input::hasPost()) {
			$this->updateProfile();
		}
		
		$pageData['user'] = $this->getUser();
		$this->page->setTitle('Edit Account - CICS Drive');
		$this->page->setContent('faculty/edit_account.php', $pageData);
		echo $this->page;
	}

	private function updateProfile() {
		$firstName = Input::post('firstName');
		$middleInitial = Input::post('middleInitial');
		$lastName = Input::post('lastName');
		$idno = Input::post('idno');
                $avatars='';
		
		if ($idno != null && !empty($idno)){
			if(ctype_digit($idno)){
				$this->getUser()->setIdNumber($idno);
			}
		}
		if ($firstName != null && !empty($firstName)){

				$this->getUser()->setFirstName($firstName);
	
		}
		
		if ($middleInitial != null && !empty($middleInitial)){
				if(ctype_alnum($middleInitial)){
					$initial = strtoupper(substr($middleInitial,0,1));
					$this->getUser()->setMiddleInitial($initial);
				}else{
					$this->page->addAlert('Special character has been is not allowed.');
				}
			}

		if ($lastName != null && !empty($lastName)){
				$this->getUser()->setLastName($lastName);

		}

		if (Input::hasFile() && $_FILES['avatar']['error'] != 4) {
			$avatar = Input::file('avatar');
			$ext = pathinfo($avatar['name'], PATHINFO_EXTENSION);
			if($ext=='jpeg' || $ext=='jpg' || $ext=='png'|| $ext=='gif'){
				if (!empty($avatar['name'])) {
					$userfile_name = $avatar['name'];
					$avatars = $userfile_name;
									$extn = substr($userfile_name, strrpos($userfile_name, '.')+1);
					$fileName = $this->getUser()->getUserId().'.'.$extn;
					//debug_echo($this->getUser()->getAvatarFileName());
					if ($this->getUser()->getAvatarFileName() != null) {

						$oldAvatar = ROOT.'media/images/avatars/'.$this->getUser()->getAvatarFileName();
						if (file_exists($oldAvatar))
							unlink($oldAvatar);
					}
					move_uploaded_file($avatar['tmp_name'], ROOT.'media/images/avatars/'.$fileName);
					$this->getUser()->setAvatarFileName($fileName);
					move_uploaded_file($avatar['tmp_name'], ROOT.'media/images/avatars/'.$fileName);
				$this->getUser()->setAvatarFileName($fileName);
                                
                                require_once LIBRARY.'Image.php';
                                $image = new Image(ROOT.'media/images/avatars/'.$fileName);
                                $image->createThumbnail(40, 40, ROOT.'media/images/avatars/'.$fileName);
				}
			} else {
              $this->page->addAlert('Unacceptable image format', 'notice');
              return;
            }
		}
		
		$a = $this->getUser()->updateRecord();
		
                if ($a){
                        $this->setUserLog();
			$this->page->addAlert('Your account was successfuly updated', 'success');
                }else
			$this->page->addAlert('Error during update.', 'error');
		
	}
        
        private function setUserLog(){
            UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_UPDATE, $this->getUser()->getUserID());
        }
}