<?php

require_once PROCESS.'FacultyProcess.php';
require_once LIBRARY.'drive/PublicationsFolder.php';
require_once LIBRARY.'drive/StudentDrive.php';

class ViewPublications extends FacultyProcess {
  public function run() {
    parent::run();
    
    if(!file_exists(DRIVE.$this->getUser()->getUserID().'/.publications')){
        mkdir(DRIVE.$this->getUser()->getUserID().'/.publications', 0777, true);
    }
    
    $delete = Input::post('delete')?Input::post('delete'):null;
    
    $folder = new PublicationsFolder($this->getUser(),$delete);    
    $drive = new StudentDrive($folder);
    $uri = $this->request->params();
    $templateData['contentString'] = $drive->getContentsFormatted(SITE_URL.'faculty/publication');
    $this->page->setContent('faculty/viewPublications.php', $templateData);
    
    if(Input::post('delete')!=null){
      $file = new File(DRIVE.$this->getUser()->getUserID().'/.publications/'.Input::post('delete'));
      if($file->delete(null, null, null, null, null, true)){
          $this->page->addAlert($file->getName().' has been deleted.','success');
      }
    }
    
    $this->page->addAsset('drive.css');
    echo $this->page;
  }
}
