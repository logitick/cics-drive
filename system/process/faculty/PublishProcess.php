<?php

require_once PROCESS . 'FacultyProcess.php';
require_once MODEL.'Publication.php';
require_once MODEL.'Comment.php';
require_once LIBRARY.'Publish.php';
require_once LIBRARY.'/drive/File.php';
require_once LIBRARY.'/drive/PublicationsFolder.php';
class PublishProcess extends FacultyProcess {
  
  private $publication;
  private $lib;
  public function run() {
    parent::run();

    if (Input::post('publishPath')) {
      if (Publication::publicationExists($this->getUser(), Input::post('publishPath'))) {
        $this->page->addAlert('A publication with the same name already exists. Go <a href="javascript:history.back();">Back</a>.', 'warning');
      } else {
        $id = $this->createPublication();
        if ($id > 0) {
          $this->response->redirect(SITE_URL.'faculty/publication/'.$id);
          die();
        } else {
          $this->page->addAlert('Error creating publication at this time', 'error');
        }
      }


    }
    
    
    if ($this->request->id != null) {
      $id = $this->request->id;
      $this->publication = Publication::getById($id);
      if (Input::post('txtDescriptionUpdate') != null) {
        $this->updateDescription();
      }
      
      if (Input::post('muteUser')) {
        $mutes = $this->publication->getMutes();
        $user = User::getByID(Input::post('muteUser'));
        $isMuted = false;
        if ($mutes != null) {
          foreach ($mutes as $mutedUser) {
            if ($user->getUserID() == $mutedUser->getUserID()) {
              $isMuted = true;
            }
          }
        }

        if (!$isMuted) {
          $status = $this->publication->muteUser(Input::post('muteUser'));
        } else {
          $status = false;
        }
        if ($this->request->isAjax()) {
            
            $arr['user'] = $user->getLastName().', '.$user->getFirstName();
            $arr['status'] = $status;
            echo json_encode($arr);
            die();
        }
      }
      
      if (Input::post('unmuteUser')) {
        $status = $this->publication->unmuteUser(Input::post('unmuteUser'));
        if ($this->request->isAjax()) {
            $arr['status'] = $status;
            echo json_encode($arr);
            die();
        }
      }
      
      if (Input::post('txtAddComment') != null) {
        if ($this->addComment()) {
          $this->page->addAlert('Comment added', 'success');
        } else {
          $this->page->addAlert('Cannot add comment as this time', 'error');
        }
      }
      
  
      
      
      if ($this->publication == null) {
        $this->response->redirect('/404', 404);
      }
      $this->page->addAsset('select2.css');
      $this->page->addAsset('select2.min.js');
      $this->page->addAsset('jquery.modal.css');
      $this->page->addAsset('jquery.modal.min.js');
      $filePath = DRIVE.$this->getUser()->getUserID().'/'.PublicationsFolder::FOLDER_NAME.$this->publication->getFilePath();
      if (file_exists($filePath)) {
        $file = File::createDriveItem($filePath);
        $this->lib = new Publish($this->page, $this->publication, $file, '/faculty/publication.php');
        
      if (Input::post('removeComment') != null) {
          $status = $this->lib->removeComment(Input::post('removeComment'));
          if ($this->request->isAjax()) {
            $arr['status'] = $status;
            echo json_encode($arr);
            die();
          }
          
          if ($status) {
            $this->page->addAlert('Comment removed', 'success');
          } else {
            $this->page->addAlert('Cannot remove comment at this time', 'warning');
          }
        }
        
        $pageData = $this->lib->getPageData();
        
        $pageData['comments'] = $this->renderComments();
        $pageData['commenters'] = $this->renderCommenterOptions();
        $pageData['mutes'] = $this->renderMutes();
        $this->page->setContent('/faculty/publication.php', $pageData);
      }
    }
    
    echo $this->page;
  }
  
  public function renderComments() {
        $comments = $this->lib->getComments();
        $str = '';
        if ($comments == null) {
          return $str;
        }
        foreach ($comments as $comment) {
          $user = User::getByID($comment->getUserID());
          if ($user == null) {
            continue;
          }
          $commentStr = $comment->getComment();
          if ($comment->getStatus() == Comment::STATUS_INACTIVE) {
            $commentStr = '[comment removed]';
          }
          $str .= '<div class="commentContainer">';
          $str .= '<a href="#comment_'.$comment->getCommentID().'" class="deleteComment" data-comment-id="'.$comment->getCommentID().'" id="comment_'.$comment->getCommentID().'"><span class="icon icon-dark">❎</span></a>';
          $str .= '<div class="owner"><img src="'.$user->getAvatarUrl().'" width="40" height="40"> '.$user->getFirstName().' '.$user->getLastName().'</div>';
          $str .= '<div class="date">'.$comment->getCommentDate()->format('l, M d, Y - h:i a').'</div>';
          $str .= '<div class="comment">'.$commentStr.'</div>';
          $str .= '</div>';
        }
        return $str;
  }
  
  public function renderCommenterOptions() {
    $str = '';
    $commenters = $this->lib->getCommenters();
    if ($commenters != null) {
      foreach ($commenters as $user) {
        if ($this->user->getUserID() == $user->getUserID()) {
          continue;
        }
        $str .= '<option value="'.$user->getUserID().'">'.$user->getLastName().', '.$user->getFirstName().'</option>';
      }
    }
    return $str;
  }
  
  public function renderMutes() {
    $mutes = $this->lib->getMutes();
    $str = '';
    if ($mutes != null) {
      foreach ($mutes as $user) {
        $str .= '<tr><td>'.$user->getLastName().', '.$user->getFirstName().'</td><td><a href="javascript:;" data-id="'.$user->getUserID().'" class="button small unmute">unmute</a></td></tr>';
      }
    }
    return $str;
  }
  
  public function updateDescription() {

    $desc = Input::post('txtDescriptionUpdate');
    $this->publication->setDescription($desc);
    if ($this->publication->updateRecord()) {
      $this->page->addAlert('Description updated', 'success');
    } else {
      $this->page->addAlert('Error updating the database. Try again later', 'error');
    }
  }
  
  public function createPublication() {
    $path = Input::post('publishPath');
    $desc = Input::post('txtPublishDescription');
    $date = new DateTime();
    $file = new File(DRIVE.$this->getUser()->getUserID().$path);
    $publicationsFolder = DRIVE.$this->getUser()->getUserID().'/'.PublicationsFolder::FOLDER_NAME;
    if (!file_exists($publicationsFolder)) {
      mkdir($publicationsFolder);
    }
    $copiedFile = $file->copy($publicationsFolder);
    
    $pub = Publication::factory($date, '/'.$copiedFile->getName(), $desc, $this->user->getUserID(), $path);
    return $pub->saveRecord();
  }
  
  public function addComment() {
    $comment = Input::post('txtAddComment');
    $inst = Comment::factory($this->getUser()->getUserID(), $this->request->id, $comment);
    return $inst->storeRecord();
  }

}