<?php

require_once MODEL.'User.php';
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Paginator.php';
/**
 * @package process
 * @subpackage student
 */
class AcountList extends AdminProcess {
    	
    public function run() {
            parent::run();
            
            if ( Input::hasPost() ){
				if(Input::post('sConfirm')==3){
					$this->createStudentAccount();
				}elseif(Input::post('fConfirm')==2){
					$this->createFacultyAccount();
				}elseif(Input::post('aConfirm')==1){
					$this->createAdminAccount();
				}else{
					$this->resetPassword();
				}
			}
			
            if (Input::post('setUserStatus') && Input::post('userIdForStatus')) {
              $user = $this->updateUserStatus(Input::post('userIdForStatus'), Input::post('setUserStatus'));
              if ($user instanceof User) {
                $this->page->addAlert("{$user->getLastName()} account changed to {$user->getStatusString()}", 'success');
                  $logID = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_UPDATE_STATUS, $this->getUser()->getUserID());
                  UserActivityProperties::logProperties($logID, UserActivityProperties::USER_ID, $user->getUserID());
                  UserActivityProperties::logProperties($logID, UserActivityProperties::USER_STATUS, $user->getStatus());
              } else {
                $this->page->addAlert('Status not updated. Please try again.', 'error');
              }
            }
            
            $maxRows = 15;
            
            if ($this->request->page == null) {
                $four = '4';
                $this->request->$four = 'page';
                $this->request->page = 1 ;
            }
            
            $flags = 0;
            $sessionFlags = Session::get('admin_flags');
            
            if ($sessionFlags!=null) 
                $flags = $sessionFlags;
            
            if (UserFlags::flagExists(Input::post('userStatus')))
                $flags = (int)Input::post('userStatus');
            if (Input::post('userType') != null && is_array(Input::post('userType'))) {
                $types = Input::post('userType');
                foreach ($types as $type) {
                    if (UserFlags::flagExists($type))
                        $flags += (int)$type;
                }
            }
            $sortColumn = 1;
            $pageData['sortColumn'] = $sortColumn;
            $pageData['sortOrder'] = "DESC";
            if (Input::post('sortColumn') && Input::post('sortOrder')) {
                $pageData['sortColumn']  = $sortColumn = (int)Input::post('sortColumn');
                if (Input::post('sortOrder') == "ASC")  {
                    $flags = $flags | UserFlags::ASC;
                    $pageData['sortOrder'] = "ASC";
                }
                else if ($flags & UserFlags::ASC) {
                    $flags -= UserFlags::ASC;
                    
                }
                
            }
           
            $flags = $flags == 0 ? UserFlags::STUDENT + UserFlags::ACTIVE + UserFlags::ASC: $flags;
            
            $params = $this->request->params();
            
            $baseUrl = SITE_URL.ltrim($params[0], '/');

            $baseUrl = preg_replace('/\/page\/[0-9]*/', '/', $baseUrl);
            
            $totalRows = User::getUserListCount($flags);
            $totalRows = $totalRows == null ? 0 : $totalRows;
            $pageData['pagination'] = new Paginator($baseUrl, $totalRows, $maxRows, $this->request->page);            
            Session::set('admin_flags', $flags);
            $offset = ($this->request->page - 1) * $maxRows;
            $pageData['users'] = User::getUserList($flags, $maxRows, $offset, $sortColumn);
            $pageData['flags'] = $flags;
            
            if (Input::get('q')) {
              $pageData['users'] = User::search(Input::get('q', false));
              $pageData['pagination'] = '';
              
            }
            $this->page->addAsset('admin.js');
            $this->page->addAsset('jquery.modal.css');
            $this->page->addAsset('jquery.modal.js');
            $this->page->addAsset('admin.css');
            $this->page->setTitle('Accounts List - CICS Drive');
            $this->page->setContent('admin/account_list.php',$pageData);
            echo $this->page;
    }
    
    private function updateUserStatus($userId, $newStatus) {
        if (!is_numeric($userId))
          return false;
        $userId = (int)$userId; 
        $user = User::getByID($userId);
        $user->setStatus($newStatus);
        return $user->updateRecord() ? $user:null;
    }
	
	private function resetPassword(){
		$newUser = new User();
		if(Input::post('resetPassword')){
			$password = strtolower(Input::post('password'));
			$a = $newUser->resetUserPassword(Input::post('resetPassword'),$password);
			
			 if ($a){
				$this->page->addAlert('Account password was successfuly reset to "'.$password.'"', 'success');
			}else{
				$this->page->addAlert('Error during update.', 'error');
			}
		}
	}
	
	private function createStudentAccount(){
		$newStudent = new User();
		if(Input::post('idno')!=null && Input::post('fname')!=null && Input::post('lname')!=null)
		{
			if(ctype_digit(Input::post('idno'))){
				if(User::validateUser(Input::post('idno'))!=null){
					$this->page->addAlert('Student Already Exist');
				}else{
					if(ctype_alnum(Input::post('idno')) && Input::post('fname') && Input::post('lname') ){
						$newStudent->setIdNumber(Input::post('idno'));
						$newStudent->setFirstName(Input::post('fname'));
						$newStudent->setLastName(Input::post('lname'));
						$newStudent->setUserType(3);
							
						$s = $newStudent->createStudentRecord();
							
						if ($s){
							$this->page->addAlert('Student account was successfuly created', 'success');
                                                        $newUser = User::getByID($s);
                                                        $id = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_CREATE, $this->getUser()->getUserID());
                                                        UserActivityProperties::logProperties($id, UserActivityProperties::USER_ID, $newUser->getUserID());
						}else{
							$this->page->addAlert('Error during update.', 'error');
						}
					}else{
						$this->page->addAlert('Special character has been Detected.');
					}
				}
			}else{
				$this->page->addAlert('Id Number must not contain special character.');
			}
		}else{
			$this->page->addAlert('Must fill-up all student fields');
		}
	}
	
	private function createFacultyAccount(){
		$newUser = new User();
		if(Input::post('fpassword') != null && Input::post('femail') != null){
                  $exists = User::existingEmail(Input::post('femail'));
			if( $exists != false || $exists > 1 ){
				$this->page->addAlert('Email Address already exist');
			}else{
					if(strlen(Input::post('fpassword')) > 5){
						$newUser->setPassword(Input::post('fpassword'));
						$newUser->setUserType(2);
						$newUser->setEmail(Input::post('femail'));
					
						$a = $newUser->createRecord();
			
						 if ($a){
							$this->page->addAlert('Faculty account was successfuly created', 'success');
                                                        $newUser = User::getByID($a);
                                                        $id = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_CREATE, $this->getUser()->getUserID());
                                                        UserActivityProperties::logProperties($id, UserActivityProperties::USER_ID, $newUser->getUserID());
						}else{
							$this->page->addAlert('Error during update.', 'error');
						 }
					}else{
						$this->page->addAlert('Password must be in a minimum of 6 characters');
					}
			}
		}else{
			$this->page->addAlert('Must fill-up all email and password fields');
		}
	}
	
	private function createAdminAccount(){
		$newUser = new User();

		if(Input::post('apassword') != null && Input::post('aemail') != null){
			$exists = User::existingEmail(Input::post('aemail'));
			if( $exists != false || $exists > 1 ){
				$this->page->addAlert('Email Address already exist');
			}else{
					if(strlen(Input::post('apassword')) > 5){
						$newUser->setPassword(Input::post('apassword'));
						$newUser->setUserType(1);
						$newUser->setEmail(Input::post('aemail'));
					
						$a = $newUser->createRecord();
			
						 if ($a){
                                                    $this->page->addAlert('Administrator account was successfuly created', 'success');
                                                    $newUser = User::getByID($a);
                                                    $id = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_CREATE, $this->getUser()->getUserID());
                                                    UserActivityProperties::logProperties($id, UserActivityProperties::USER_ID, $newUser->getUserID());
                                                                
						}else{
                                                    $this->page->addAlert('Error during update.', 'error');
						 }
					}else{
						$this->page->addAlert('Password must be in a minimum of 6 characters');
					}
			}
		}else{
			$this->page->addAlert('Must fill-up all email and password fields');
		}
	}
	
}