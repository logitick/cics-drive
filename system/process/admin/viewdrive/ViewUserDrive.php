<?php
/**
 * @package process
 * @subpackage student
 */

require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'/drive/StudentDrive.php';
require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'ItemRecovery.php';

class ViewUserDrive extends AdminProcess {

	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $drive;
	
	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $itemrecovery;
	
	/**
	*	@var use as a path of the item that was removed 
	*/
	private $remove_path;
	
	/**
	* @var holds the data of the user
	*/
	private $User;
	
	/**
	*
	*
	*/
	public function run() {
		
		parent::run();
		
		if(Input::post('user_id')){
			Session::set('view_user_id',Input::post('user_id'));
		}
			
		if(Session::get('view_user_id') != null){
			$this->User = User::getByID(Session::get('view_user_id'));	
		}else{
			$this->response->redirect('/404');
			die();
		}
		
		$uri = $this->request->params();
		
		$storageCopacity = UserType::getByID($this->User->getUserType())->getStorageCapacity();	
				
		if (count($uri) > 1) {
			if( array_key_exists(1,$uri) ){
				$uri[1] = '/'.urldecode($uri[1]);
				$uri[1] = rtrim($uri[1], '/');
				
				try {
					$this->drive = new StudentDrive($uri[1],2,$this->User);
				} catch(NoSuchFolderException $e) {
					$this->response->redirect('/404');
				}
			}else{
				$this->drive = new StudentDrive(null,2,$this->User);
			}
		} else { 
			$this->drive = new StudentDrive(null,2,$this->User);
		} 
		
		$this->itemrecovery = new ItemRecovery( $this->User, array('viewdrive' => true, 'viewtype' => 2 )); 
		
		//search item
		if(Input::get('search')){
			
			$folder = new Folder( DRIVE.$this->User->getUserID(),$this->User );
			$this->drive->searchDeeper($folder,Input::get('search'));
			
			echo json_encode($this->drive->resultArray);
			die();
		}
		
        $deleteFlag = $this->drive->getRoot()->getPath().'/.deleted';

		//remove folder/file
		if(Input::post('remove')){	
			$items = Input::post('remove');
			$count=0;
			
                        foreach($items as $item){
				if($this->drive->remove($item)){
                  file_put_contents($deleteFlag, $item."\n", FILE_APPEND);
					$count++;
				}
			}
			$this->page->addAlert($count.' Item(s) has been moved to the <a href="'.SITE_URL.'admin/users/item_recovery/admin'.'" style="color:blue;">Item Recovery</a>','success');
		} 
		
		//delete item	
		if(Input::post('delete')){	
			$items = Input::post('delete');
			$count=0;
			foreach($items as $item){
				if($this->itemrecovery->delete( $item )){
					$count++;
				}
			}
			$this->page->addAlert($count.' Item(s) has been deleted permanently','success');
		}
		
		// used by code editor	
        /* if ($this->request->isAjax() && Input::get('contents')) { // for ajax requests
                $this->page->setHeader(null);
                $this->page->setFooter(null);
       
                die($this->drive->toJSON());
        } */
	
		$pageData['contentString'] = $this->drive->getContentsFormatted(SITE_URL.trim($uri[0], '/'));
		$pageData['dir'] = urldecode($uri[0]);
		
		$this->page->addAsset('drive.css');
		$this->page->addAsset('select2.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('jquery.contextMenu.css');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('drive.menu.css');
		$this->page->addAsset('our.modal.css');
			
		
		$this->page->addAsset('select2.min.js');
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom_dialog.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('drive.functions.js');
		$this->page->addAsset('drive.js');
		$this->page->addAsset('drive.view.js');
		
		if (!isset($uri[1]))
			$uri[1] = null;
		$pageData['breadcrumb'] = $this->drive->getBreadCrumb(urldecode(SITE_URL.'admin/users/drive'), $uri[1],true); 
		
		$pageData['user'] = $this->User;
		
		$this->page->setHeader('admin/viewdrive/header.php',$pageData);
		$this->page->setFooter('');
		$this->page->setContent('admin/viewdrive/view_drive.php',$pageData);
		
		echo $this->page;
	
	}
	
}