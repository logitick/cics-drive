<?php
/**
 * @package process
 * @subpackage student
 */

require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'ItemRecovery.php';

class ItemRecoveryProcess extends AdminProcess {

	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $itemrecovery;
	
	/**
	*	@var use as a path of the item that was restored 
	*/
	private $restore_path;
	
	public function run() {
		
		parent::run();
		$uri = $this->request->params();
	
		$this->itemrecovery = new ItemRecovery( $this->getUser() );
		
		switch($this->getUser()->getUserType()){
			case 1: $this->restore_path = SITE_URL.'admin/drive';
					break;
			case 2:	$this->restore_path = SITE_URL.'faculty/drive';
					break;
			case 3:	$this->restore_path = SITE_URL.'student/drive';
		}
		
		//delete item	
		if(Input::post('delete')){	
			$items = Input::post('delete');
			$count=0;
			foreach($items as $item){
				if($this->itemrecovery->delete( $item )){
					$count++;
				}
			}
			$this->page->addAlert($count.' Item(s) has been deleted permanently','success');
		}
		
		//restore item
		if(Input::post('restore')){
			$items = Input::post('restore');
			$count=0;
			foreach($items as $item){
				$restored = $this->itemrecovery->restore( $item );
				if($restored['0']){
					$count++;
				}  
			}
			$this->page->addAlert($count.' Item(s) has been restored.','success');
		}
		
		$pageData['contentString'] = $this->itemrecovery->getTrashedContents();
		$pageData['dir'] = urldecode($uri[0]);
		$pageData['status'] = $this->getUser()->getStatus();
		
		$this->page->addAsset('drive.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('jquery.contextMenu.css');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('drive.menu.css');
		$this->page->addAsset('our.modal.css');
		
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom_dialog.js');
		$this->page->addAsset('drive.functions.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('drive.js');
		
		
		$this->page->setContent('student/item_recovery.php',$pageData);
		
		echo $this->page;
	
	}
	
}