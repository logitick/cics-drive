<?php

require_once MODEL.'User.php';
require_once PROCESS.'AdminProcess.php';
/**
 * @package process
 * @subpackage student
 */
class CreateAccount extends AdminProcess {
	
	public function run() {
		parent::run();
		
		if ( Input::hasPost() ) {
			if(Input::post('usertype')!=null){
				if(Input::post('usertype')!= 3){
					$this->createUserAccount();
				}else{
					$this->createStudentAccount();
				}
			}else{
				$this->page->addAlert('Please select user type','error');
			}
		}
		
		$pageData['user'] = $this->getUser();
		$this->page->setTitle('Create Account - CICS Drive');
		$this->page->setContent('admin/create_account.php', $pageData);
		echo $this->page;
	}
	
	private function createUserAccount(){
		$newUser = new User();
		if(Input::post('Password') != null && Input::post('email') != null){
			if( User::existingEmail(Input::post('email')) != null ){
				$this->page->addAlert('Email Address already exist');
			}else{
					$newUser->setPassword(Input::post('Password'));
					$newUser->setUserType(Input::post('usertype'));
					$newUser->setEmail(Input::post('email'));
				
					$a = $newUser->createRecord();
		
					 if ($a){
						if(Input::post('usertype')!=2){
							$this->page->addAlert('Admin account was successfuly created', 'success');
						}else{
							$this->page->addAlert('Faculty account was successfuly created', 'success');
						}
					}else{
						$this->page->addAlert('Error during update.', 'error');
					 }
			}
		}else{
			$this->page->addAlert('Must fill-up all email and password fields');
		}
	}
	private function createStudentAccount(){
		$newStudent = new User();
		if(Input::post('idno')!=null && Input::post('fname')!=null && Input::post('lname')!=null)
		{
			if(User::validateUser(Input::post('idno'))!=null){
				$this->page->addAlert('Student Already Exist');
			}else{
				$newStudent->setIdNumber(Input::post('idno'));
				$newStudent->setFirstName(Input::post('fname'));
				$newStudent->setLastName(Input::post('lname'));
				$newStudent->setUserType(Input::post('usertype'));
					
				$s = $newStudent->createStudentRecord();
					
				if ($s){
					$this->page->addAlert('Student account was successfuly created', 'success');
				}else{
					$this->page->addAlert('Error during update.', 'error');
				}
			}
		}else{
			$this->page->addAlert('Must fill-up all student fields');
		}
	}
	
}