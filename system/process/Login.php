<?php
require_once MODEL.'User.php';
require_once MODEL.'Logins.php';
require_once LIBRARY.'Session.php';

/**
 * @package process
 */
class Login extends Process {
    
	public function run() {	
		
		Session::remove('registration');
		
		$this->page->setHeader('guest/header.php');
		$this->page->setFooter('guest/footer.php');
		$this->page->setTitle('Login - CICS Drive');
		$this->page->setContent('guest/login.php');
		$this->page->addAsset('compatibility.checker.js');
		$this->page->addAsset('compatibility.checker.css');
		
		if (Input::post('email') != null && Input::post('password')!= null) {
			$valid = User::login(Input::post('email'), Input::post('password'));
			if(  $valid!= true ){
				
                                $user = User::getByEmail(Input::post('email'));
                                switch($user->getStatus()) {
                                  case User::STATUS_SUSPENDED:
                                    $this->page->addAlert('Your account is suspended. Approach a lab supervisor', 'error');
                                    break;
                                  default:
                                    $this->page->addAlert('Invalid email or passwords');
                                }
			}
		}
                
		if (Session::isLoggedin()) {
			
                        $user = Session::getUser();
			
                        switch((int)$user['userType']) {
				case 1:
                                        self::logLogin($user['userId'],$this->request->ip(), session_id(),$this->request->getReadyBrowser());
					$this->response->redirect(SITE_URL.'admin');
					break;
				case 2:
                                        self::logLogin($user['userId'],$this->request->ip(), session_id(),$this->request->getReadyBrowser());
					$this->response->redirect(SITE_URL.'faculty');
					break;
				case 3:
					if (isset($user['validate']) && $user['validate'] == true) // make user to answer a security question
						$this->response->redirect(SITE_URL.'question');
					break;
			} 	
		}
		 
		echo $this->page;
	}
        
        public static function logLogin($userid,$ip,$sessioid,$useragent){
            $user = Session::getUser();
           
            Logins::setLoginInfo( $userid, $ip, $sessioid, $useragent );
        }
}