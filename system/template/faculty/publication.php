	<section class="widget">
		<header>
			<span class="icon">&#59153;</span>
			<hgroup>
				<h1><?php echo $document;?></h1>
			</hgroup>
		</header>
		<div class="content">
			
				<div class="toolbar">
                  <a class="button" id="btnMute"><span class="icon">&#128263;</span>Muting</a>						
				</div>
                                <div id="publicationDescription" style="padding: 10px; margin-bottom: 50px; font-size: 1.3em;">
                                  <form method="post" >
                                    <textarea maxlength="255" name="txtDescriptionUpdate"><?php echo $description;?></textarea>
                                    <button>Update description</button>
                                  </form>
                                </div>
				<div id="drive-editor">
						<textarea id="codeEditor" name="codeEditor"><?php echo $fileContents;?></textarea>
                        <input type="hidden" name="mime" id="mime" value="<?php echo $mimeType;?>">
				</div>
                <div id="publicationComments" style="margin:20px 0;">
                  <?php echo $comments;?>
                  <form method="post">
                    <input type="text" maxlength="140" name="txtAddComment" autocomplete="off">
                    <button>Add Comment</button>
                  </form>
                </div>
                <div style="clear:both;"></div>
		</div>
	</section>
<div class="modal" id="mutingModal">
        <h3 id="title">Mute Student</h3>
        <hr/>
         <form method="post">
           <div id="modalContents">
             <select id="commenterList">
               <option></option>
               <?php echo $commenters;?>
             </select>
             <button class="small">Mute Student</button>
             <table id="myTable">
               <?php echo $mutes;?>
             </table>
             
           </div>
         </form>
</div>