﻿
	<section class="widget">
		<header>
			<span class="icon">&#59153;</span>
			<hgroup>
				<h1>Compare Code</h1>
				<!-- <h2>Drag &amp drop file upload</h2> -->
			</hgroup>
		</header>
		<div class="content">
                  <table class="headerTable">
                    <tr>
                     
                      <td>Legend: 
                         <span class="compareLegend" style="background:#DDFFDD;">Added</span> 
                         <span class="compareLegend" style="background:#FF8888;">Removed</span> 
                         <span class="compareLegend" style="background:#FFCC00;">Non-identical</span> 
                         <span class="compareLegend" style="background:#FFF;">Match</span>
                      </td>
                       <td></td>
                    </tr>
                    <tr>
                      <td> <span id="sourceAFileName"><?php echo $baseFileName;?></span></td>
                      <td><a id="compareFile" class="modalOpen" href="javascript:;" data-drive-url="<?php echo $url;?>"><span class="icon icon-dark">&#128196;</span></a> <span id="sourceBFileName"><?php echo $compareFileName;?></span></td>
                    </tr>
                  </table>
                  <?php echo $diff;?>
                  <form action="" method="post" id="compareFileForm">
                    <input type="hidden" id="hdnCompareFile" value="" name="compareFile">
                  </form>
		</div>
	</section>
        <div class="modal" id="FileOpenModal">
                <h3 id="title"></h3>
                <div id="modalContents">
                        <ul class="DriveItemsList">
                                <li class="Folder hasSubMenu" id="rootFolderListItem"><a class="animated flash" rel="<?php echo SITE_URL.uri(1).'/drive/';?>" href="javascript:;"><span class="icon">&#128193;</span>My Drive <img id="loaderImage" src="<?php echo IMAGES.'loader16.gif';?>"></a></li>

                </ul>
                </div>
        </div>

