
	<section class="widget">
		<header>
			<span class="icon">&#128188;</span>
			<hgroup>
				<h1>Archiving</h1>
				<!-- <h2>Drag &amp drop file upload</h2> -->
			</hgroup>
		</header>
           <div class="content">
                <div class="toolbar">
                    <button name="btnOpen" id="archiveBtn" class="orange"> <span class="icon" style="color:#fae8f1;">&#128188;</span> Create Archive</button>
                    <input type="hidden" id="url" value="<?php echo SITE_URL.'faculty/drive/'; ?>" >
                    <form action="<?php echo SITE_URL.'faculty/archive'; ?>" method="post" style="display:inline;">
                        <button class="blue" name="dlArchive" value="" > <span class="icon">&#128229;</span> Download Archive</button>
                    </form>
                    <form action="<?php echo SITE_URL.'faculty/archive'; ?>" method="post" style="display:inline;">
                        <button class="pull-right" name="deleteArchive" value="" > <span class="icon">&#59177;</span> Delete Archive</button>
                    </form>
                </div>
            
                <div id="dropzone" class="recovery">
			<ul id="fileSystem">
				<?php echo $contentString;?>
			</ul>
		</div>
          </div>
            
            <div class="modal archiveModal" id="archiveModal">
                <h4>Archiving current Drive</h4>
                <div class="archiveModalContent ">
                    <span></span>
                </div>
                <div class="pull-right">
                    <button class="red" id="arc_cancel" >Cancel</button>
                    <button class="blue" id="arc_ok" >Start</button>
                </div>
            </div>
	</section>

