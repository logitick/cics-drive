
<section class="widget">
	
	<header>
		<span class="icon">&#59159;</span>
		<hgroup>
			<h1 style="letter-spacing:1px">Item Recovery</h1>
			<h2>Trashed Items</h2>
		</hgroup>

	</header>
	<div class="content">
		<div class="toolbar">
<!--restore-->			
			<?php echo $status!='K'?'<form action="'.$dir.'" method="post" style="display:inline;">':"" ?>
				<button style="display:none;" id="restore_button"></button>
			<?php echo $status!='K'?'</form>':"" ?>
			<button class="blue" <?php echo $status!='K'?"":'disabled="disabled"'; ?> id="dummyRestore_button"><span class="icon"><?php echo $status!='K'?'&#128281':'&#128683;'; ?></span> Recover </button>
<!---delete-->
			<form style="display:inline;" action="<?php echo $dir;?>" method="post">
				<button style="display:none;" id="delete_button"></button>
			</form>
			<button class="orange" id="dummyDelete_button"><span class="icon">&#59177;</span> Delete </button>
		</div>
		
		<div id="dropzone" class="recovery" >
			<ul id="fileSystem">
				<?php echo $contentString;?>
			</ul>
		</div>
	</div>
</section>

	
	