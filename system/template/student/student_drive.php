﻿<section class="widget">
	<!--<span class="icon breadcrumb">&#59234;</span>test-->
	<header>
		<span class="icon">&#9729;</span>
		<hgroup>
			<h1>My Drive</h1>
			<h2><?php echo $breadcrumb;?></h2>
		</hgroup>
		
		<aside>
			<span>
				<a id="option" >&#9881;</a>
				<ul class="settings-dd">
					<li><label for="alldrive">Display all drive item(s)</label> <input type="radio"  id="alldrive" name="drive_view" /></li>
					<li><label for="folderdrive" >Display folder(s) only</label> <input type="radio"  id="folderdrive" name="drive_view" /></li>
					<li><label for="filedrive" >Display file(s) only</label> <input type="radio"  id="filedrive" name="drive_view" /></li>
				</ul>
			</span>
		</aside>
		<div class="searchContainer">
			<input type="hidden" id="hid" class="search">
		</div>
		
	</header>
	<div class="content">
		<div class="toolbar">
			<input id="url" type="hidden" value="<?php echo $dir;?>" />
			<input id="dirPath" type="hidden" value="<?php echo rtrim(SITE_URL,'/').$dir?>" />
			<button id="createFolder_button" ><span class="icon" style="color:rgb(152,197,227);">&#128193;</span> Create Folder </button>
            <?php 
            if ($userType == 2): ?>
            <button id="toolbarShareFolder"><span class="icon" style="color:rgb(255,247,178);">&#59196;</span> Share Folder </button>
            <button id="toolbarPublish"><span class="icon">&#9733;</span> Publish</button>
            <button id="toolbarSubmissionReport" style="display:none;">Report</button>
            <button id="toolbarUnshare" style="display:none;">Unshare</button>
            <?php elseif ($userType == 3): ?>
			<button id="submit"><span class="icon" style="color:rgb(255,247,178);">&#59213;</span> Submit Folder </button>
            
            <?php endif; ?>
			<button id="upload_button" <?php echo $status!='K'?"":'disabled="disabled"'; ?> class="green"> <span class="icon" style="color:#3D4DB4;"><?php echo $status!='K'?'&#128228;':'&#128683;'; ?></span> Upload </button>
<!--download-->
			<?php echo $status!='K'?'<form action="'.$dir.'" method="post" style="display:inline;">':"" ?>
				<!--<input style="display:none;" id="toDownload_item" type="hidden" name="download" value=""/>-->
				<button id="download_button" style="display:none;"></button>
			<?php echo $status!='K'?'</form>':""; ?>
				<button id="dummyDownload_button"  <?php echo $status!='K'?"":'disabled="disabled"'; ?> name="download_button" class="blue"> <span id="dummyDownload_button2" class="icon" style="color:#CDE520;"><?php echo $status!='K'?'&#128229;':'&#128683;'; ?></span> Download </button>
<!--remove-->
			<form action="<?php echo $dir; ?>" method="post" style="display:inline;">
				<button id="remove_button"style="display:none;" >Remove</button>
			</form>
				<button id="dummyRemove_button" class="orange pull-right"><span class="icon" style="color:#4969E5;" >&#10006;</span> Remove Item </button>
                                <button id="btnToolbarMove" style="display:hidden;">Move</button>
		</div>
		
<!--flash-->
		<div id="flash"></div>

<!--drive-->
		<div id="dropzone">	
                      <ul id="fileSystem">
                              <?php echo $contentString;?>
                      </ul>
		</div>
	</div>
</section>

<!--Rename-->
<div class="modal" id="rename" style="width:360px;">
	<form action="<?php echo $dir;?>" method="post">
		<label for="to_rename" style="color:#333;font-weight: bold;font-size: 18px;">Rename</label>
		<input type="hidden" id="current_name" value="" name="current_name" />
		<input type="text" id="to_rename" name="renameItem" placeholder="type a new name here" required style="width:300px;height:30px;padding:0 0 0 10px;">
		<button id="ok" class="blue" name="renameOk" style="height:30px;padding:2px;width:50px;">OK</button>
	</form>
</div>
<!--End Rename-->

<!--Create Folder -->
<div class="modal" id="create_folder">
	<form method="post" style="display:inline">
		<label for="folder_name" autofocus>Create Folder</label>
		<input id="folder_name" type="text" value="" name="folder_name" placeholder="Enter a folder name" style="width:300px;height:30px;padding:0 0 0 10px;"/>
		<button name="create_folder" style="height:30px;padding:2px;width:50px;"> OK </button>
		<p></p>
	</form>
</div>
<!--End Create Folder-->
	
<!--Upload file-->
<div class="modal" id="upload_file">
	<ul id="fileSystem" class="filePreview">
	</ul>
	<div id="choose_file">
		<a>Choose Files</a>
		<input style="display:none;" type="file" name="file[]"  multiple/>
		<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
		<p>Reminder : Maximum file size/file is only <span>2 MB</span></p>
	</div>
</div>
<!--End Upload File-->
<?php if ($userType == 2) : ?>
<div class="modal" id="shareFolderModal">
        <h3 id="title">Share Folder</h3>
        <hr/>
         <form method="post">
        <div id="modalContents">
                
                <div class="panel">
                    <input type="hidden" id="sharedFolderPath" name="sharedFolderPath">
                    <label for="sharedFolderName">Folder name:</label><input type="text" id="sharedFolderName" name="sharedFolderName" readonly>
                    <div>
                        <input type="radio" id="writableFolder" name="sharedFolderMode" checked value="1"><label for="writableFolder">Writable</label>
                    </div>
                    <div>
                        <input type="radio" id="readOnlyFolder" name="sharedFolderMode" value="2"><label for="readOnlyFolder">Read-only</label>
                    </div>
                    <hr/>
                    <a id="btnNextPanel1" class="Next button green pull-right">Next</a>
                </div>
                <div class="panel">
                    <div id="calendarFilterBox"></div>
                    <div id="date_picker"> </div>
                    <label for="sharedFolderDeadline">Deadline: </label><input id="sharedFolderDeadline" name="sharedFolderDeadline" tabindex="1" >
                    <label for="sharedFolderPassword">Password:</label><input type="text" id="sharedFolderPassword" name="sharedFolderPassword" maxlength="10" tabindex="2">
                    <label for="shareFolderPrefix">Submission Prefix: <a href="javascript:;" class="useLastName pull-right" title="use submitter's last name"><span class="icon rounded">&#128100;</span></a></label><input tabindex="3" type="text" id="sharedFolderPrefix" name="sharedFolderPrefix" maxlength="25">
                    <label for="shareFolderSuffix">Submission Suffix: <a href="javascript:;" class="useLastName pull-right" title="use submitter's last name"><span class="icon rounded">&#128100;</span></a></label><input tabindex="4" type="text" id="sharedFolderSuffix" name="sharedFolderSuffix" maxlength="25">
                    <hr/> 
                    <a id="btnNextPanel2" class="Next button green pull-right">Next</a>
                    <a  class="button Back">Back</a>
                </div>
                <div class="panel">
                    <div>
                        <input type="radio" id="acceptFilters" name="filterMode" value="2" checked><label for="acceptFilters">Allow only these extensions</label>
                    </div>
                    <div>
                        <input type="radio" id="rejectFilters" name="filterMode" value="1"><label for="rejectFilters">Reject</label>
                    </div>
                    <label for="txtSharedFolderFilter">Extension:</label><input type="text" id="txtSharedFolderFilter" name="txtSharedFolderFilter"><a href="javascript:;" id="addSharedFolderFilter" class="button">Add</a>
                    <table id="myTable">
                        
                    </table>
                    <hr/>
                     <button class="button green pull-right" id="submitSharedFolder">Share Folder</button>
                      <a class="button Back">Back</a>
                </div>
                <div class="panel" style=" width: 24px; margin:0 auto;">
                  <img src="<?php echo IMAGES.'loader24.gif';?>">
                </div>
        </div>

        </form>
</div>
<div class="modal" id="publishModal">
        <h3 id="title">Publish File</h3>
        <hr/>
         <form method="post" action="/faculty/publication">
           <div id="modalContents">
             <input type="hidden" id="publishPath" name="publishPath">
             <input type="text" id="txtPublishName" name="txtPublishName" readonly>
             <textarea id="txtPublishDescription" name="txtPublishDescription" maxlength="255"></textarea>
             <button>Publish File</button>
           </div>
         </form>
</div>
<div class="modal" id="submissionReports">
  <h3 id="title">Submission Reports: <span id="totalSubmissions"></span> submission(s)</h3>
  <hr/>
  <div id="modalContents" style="max-height: 480px; overflow: auto;">

    <div>
      <div>
        <label for="reportDateStart">Start Date:</label><input id="reportDateStart" name="reportDateStart" type="text" class="SubmissionReportDatePicker"/>
        <label for="reportDateEnd">End Date:</label><input id="reportDateEnd" name="reportDateEnd" type="text" class="SubmissionReportDatePicker"/>
      </div>
      <table class="myTable" id="submissionReportsTable">
        <thead>
            <tr>
              <th>Name</th>
              <th>Action</th>
              <th>Folder</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody></tbody>
      </table>
    </div>
  </div>
</div>
<?php elseif ($userType==3) : ?>
<!--Submit Folder Modal-->
<div class="modal" id="submitFolderModal">
        <h3 id="title">Submit Folder</h3>
        <hr/>
         <form id="submissionForm">
        <div id="modalContents">
                
                <div class="panel">
                    <input type="hidden" id="submissionFolderPath" name="submissionFolderPath">
                    
                    <label for="submissionInstructor">Search Instructor:</label>
                       <select id="submissionInstructor" name="submissionInstructor" style="width:100%">
                         <option></option>
                         <?php echo $instructorOptions; ?>
                      </select>
                    <div>
                      <div id="loader" style="margin:5% auto; width:24px;"><img src="<?php echo IMAGES;?>loader24.gif"></div>
                          <ul class="DriveItemsList">
                          </ul>
                    </div>
                </div>
                <div class="panel">
                    <input type="hidden" id="submitToSharedFolderID" name="submitToSharedFolderID">
                    <label for="sharedFolderName">Password:</label><input type="password" id="sharedFolderPassword" name="sharedFolderPassword">
                    <a  class="button Back" style="display:inline-block; margin:5px 0;">Back</a>
                    <a id="btnSubmitFolder" style="display:inline-block; margin:5px 0;" class="button green ayawDakpa">Submit Folder</a>
                </div>
        </div>

        </form>
</div>
<!--End Submit Folder Modal-->
<?php endif; ?>
<div class="modal" id="moveItemModal">
  <h3 id="title">Move to <span id="moveToFolderName"></span></h3>
  <hr/>
  <div id="modalContents">
    <ul class="DriveItemsList">
      <li class="Folder hasSubMenu" id="rootFolderListItem_move"><a class="animated flash" data-name="My Drive" rel="<?php echo SITE_URL . uri(1) . '/drive/'; ?>" href="javascript:;"><span class="icon">&#128193;</span>My Drive <img id="loaderImage" src="<?php echo IMAGES . 'loader16.gif'; ?>"></a>
      </li>
      <input type="hidden" name="root" id="root" value="<?php echo SITE_URL . uri(1) . '/drive/'; ?>">
    </ul>
    <button id="btnMove">Move</button>
  </div>
</div>
<div class="modal" id="imgModal"></div>