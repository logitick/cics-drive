
	<section class="widget">
		<header>
			<span class="icon">&#9998;</span>
			<hgroup>
				<h1>Edit Account Login Details</h1>
				<!-- <h2>Drag &amp drop file upload</h2> -->
			</hgroup>
		</header>
		<div class="content">
			<form method="post" enctype="multipart/form-data">
				<div class="field-wrap">
					<label for="Current">Current E-mail address:</label>
					<input type="email" name="email" id="Email_address" value="<?php echo $user->getEmail();?>" placeholder="Type Email Address">
				</div>
				<div>
					<p style="text-align:right;font-style:italic;">*In changing the password,<strong>fill-up</strong> the current password and the new password must be the <strong>same</strong> with confirm password.</p>
				</div>
				<div class="field-wrap">
					<label for="Password">Current Password:</label>
					<input type="password" name="c_password" id="Password" placeholder="Type Current Password" maxlength="25">
				</div>
				<div class="field-wrap">
					<label for="Newpassword">New Password:</label>
					<input type="password" name="n_password" id="Newpassword" placeholder="Type New Password" maxlength="25">
				</div>
				<div class="field-wrap">
					<label for="Confirm">Confirm New Password:</label>
					<input type="password" name="cn_password" id="Confirm" placeholder="Re-type New Password" maxlength="25">
				</div>

				<button>Save Changes</button>
			</form>

		</div>
	</section>

