﻿	<section class="widget">
		<header>
			<span class="icon">&#10002;</span>
			<hgroup>
				<h1><?php echo $name;?>'s Shared folders</h1>
                <?php echo $breadcrumb;?>
			</hgroup>
		</header>
		<div class="content">
          <div id="dropzone">
                  <ul id="fileSystem">
                      <?php echo $contentString;?>
                  </ul>
          </div>
		</div>
	</section>

