<?php 	
		require_once LIBRARY.'Input.php';
		$userSession = Session::get('recovery');
?>
<section>
	<h1><strong>CICS</strong> Drive</h1>
	<h2>Password Recovery</h2>
	<form method="post" action="/recovery"> 
	    <label for="lastname">Email Address</label>
            <input id="email" autocomplete=off type="email" placeholder="Email address" required name="remail"/>
		<label for="idnumber" >ID Number</label>
		<input id="idnumber" autocomplete=off pattern="\d{1,8}" maxlength="8" name="ridNumber" value="" type="text" placeholder="ID Number" required />
		<a class="button "style="width:37%;display:inline-block;color:white;" href="<?php echo SITE_URL."login";?>">Back</a>
		<button style="width:49%;display:inline;" class="green" name="step1" >Next</button>
	</form>
</section>