<div id="notice" class="modal" style="text-align:center;">
	You can only change the question 5 times.
	<a href="#notice" rel="modal:close">Close</a>
</div>
<section style="width:500px; auto;">
	<h1><strong>CICS</strong> Drive</h1>
	<h2>Security question</h2>
	<p style="text-align:left;">For security purposes, you will be often asked to answer questions to verify your identity. Please answer the following shortly and honestly.<br/> <span id="notice" style="font-style:italic;float:right;">(You can change question(s) up to five(5) times only.)</span></p>
	<hr/>
	<form  method="post" action="/register">
                <?php foreach($user['question'] as $key => $question){ $num = $key + 1;?>
			<div>
				<label for="<?php echo 'Q'.$key;?>"><?php echo 'Q'.$num.'. '.$question['question'];?></label>
				<span class="question">Change</span>
				<input type="hidden" value="<?php echo $key;?>"/>
				<input type="text" name="<?php echo 'Q'.$key;?>" required id="<?php echo 'Q'.$key;?>" placeholder="Type your answer here"/>
			</div>
		<?php } ?>
		<a class="button "style="color:white;width:37%;display:inline-block;" href="<?php echo SITE_URL."register?state=1";?>" >Back</a>
		<button style="width:49%;display:inline;" class="green" name="step3">Next</button>
	</form>
	<h3>Step 3 of 3</h3>
</section>