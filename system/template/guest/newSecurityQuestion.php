    <section class="widget">
            
            <header>
                <span class="icon">&#9729;</span>
                    <hgroup>
                            <h1>Add Security Question</h1>
                    </hgroup>
            </header>
            <div class="content" id="cons">
                <span class="sec_0">You are required to answer the question. This will help in securing your account from malicious users.<span><br/>
                <span class="sec_1"><?php echo $temp_user->getFirstName();?>, please answer the question honestly.</span>
                <label class="sec_2"><?php echo $question['question'];?></label>
                <form action="<?php echo SITE_URL.'student/drive'?>" method="post">
                    <input type="text" name="answer" value="" placeholder="Type your answer here" style="width:300px"><br/>
                    <input type="hidden" name="sid" value="<?php echo $question['studentQuestionID']?>" placeholder="Type your answer here">
                    <button class="blue pull-left">OK</button>
                </form>
            </div>
    </section>
      <div class="comContent">
                <div class = "textModal">
                        <h2>The version of your current browser is outdated.</h2>
                        <p>*Some features may not be supported by your current browser, please choose the recommended versions of your browsers below. </p>
                </div>
                <div class="firefox">
                    <a href="http://www.mozilla.org/en-US/firefox/new/"><span>Mozilla FireFox</span> <br>16.0 or higher</a>
                </div>
                <div class="ie">
                    <a href="http://www.microsoft.com/en-us/download/internet-explorer-10-details.aspx">
					<span>Internet Explorer</span> <br>10</a>
                </div>
                <div class="chrome">
                    <a href="https://www.google.com/intl/en/chrome/browser/?hl=en&brand=CHMA&utm_campaign=en&utm_source=en-ha-sea-ph-bk&utm_medium=ha">
					<span>Google Chrome</span> <br>23.0 or higher</a> 
                </div>
                <div class="safari">
                    <a href="http://www.mozilla.org/en-US/firefox/new/">
					<span>Safari</span> <br>5.1 or higher</a>
                </div>
        </div>