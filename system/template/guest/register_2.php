<section>
	<h1><strong>CICS</strong> Drive</h1>
	<h2>Hello <?php echo $user['firstName'];?></h2>
	<form method="post" action="/register">
		<label for="email">Valid Email Address</label>
		<input id="email" type="email" placeholder="Valid email address" required name="email" value="<?php echo isset($user['email'])?$user['email']:""; ?>">
		<label for="password">Password</label>
		<input id="password" type="password" placeholder="Set password" required name="password"  maxlength="25"/>
		<label for="vPassword">Verify Password</label>
		<input id="vPassword" type="password" placeholder="Verify password" required name="vPassword" maxlength="25"/>
		<a class="button "style="width:37%;display:inline-block;color:white;"  href="<?php echo SITE_URL."register" ?>">Back</a>
		<button style="width:49%;display:inline;" class="green" name="step2" value="step2">Next</button>
	</form>
	<p id="note" style="font-style:italic;color:rgb(255,80,110);"></p>
	<h3>Step 2 of 3</h3>
</section>