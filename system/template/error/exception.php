        <div style="font-family: Helvetica, Arial, sans-serif; border:1px #333 solid; padding:10px; color:#333; margin:10px; background:#FFF;">
            <h1><?php echo $this->pageData['type'];?></h1>
            <p><?php echo $this->pageData['message'];?> at <?php echo $this->pageData['file'];?>:<?php echo $this->pageData['line'];?></p>
            <p>
<?php
//print_r($this->pageData['trace'] );
foreach ($this->pageData['trace'] as $key => $call) {
    echo '#'.$key;
    if (isset($call['file']))
        echo ' '.$call['file'];
    if (isset($call['line']))
        echo ':'.$call['line'];;
    echo '<br/>';
}
?>
</p>
            <p><a href="/">Go to the home page?</a></p>
        </div>