<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->getTitle();?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<?php echo $this->getCssAssets();?>
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" media="all" /><![endif]-->
</head>
<body>
<section class="user">
	<div class="profile-img">
		<?php $usertype='';
			switch($user->getUserType()){
				case 1:	$usertype = "Adminitrator ";
						break;
				case 2:	$usertype = "Faculty member ";
						break;
				case 3:	$usertype = "Student ";
			}?>
		<p><img src="<?php echo $user->getAvatarUrl();?>" alt="" height="40" width="40" /> <?php echo $usertype.$user->getFirstName();?></p>
	</div>
	<div class="searchContainer">
		<input type="hidden" id="hid" class="search">
	</div>
</section>

<nav>
	<ul>
		<li><a href="<?php echo SITE_URL.'admin/users/drive';?>"><span class="icon">&#59256;</span> User Drive</a></li>
		<li><a href="<?php echo SITE_URL.'admin/users/item_recovery/user';?>"><span class="icon">&#59249;</span> User Item Recovery</a></li>
		<li><a href="<?php echo SITE_URL.'admin/users/item_recovery/admin';?>"><span class="icon">&#59249;</span> Item Recovery<br><span style="float:right">(Removed by the Admin)</span></a></li>
		<li></li>
	</ul>
	
</nav>
<?php echo $this->getAlertHtml();?>
<section class="content">