﻿<section class="widget">
	<!--<span class="icon breadcrumb">&#59234;</span>test-->
	<header>
		<span class="icon">&#59159;</span>
		<hgroup>
			<h1 style="letter-spacing:1px">Item Recovery (Removed by the Admin)</h1>
			<h2>Trashed Items</h2>
		</hgroup>

	</header>
	<div class="content">
		<div class="toolbar">
				<input id="url" type="hidden" value="<?php echo $dir;?>" />
				<input id="dirPath" type="hidden" value="<?php echo rtrim(SITE_URL,'/').$dir?>" />
				<input id="viewUserID" type="hidden" value=<?php echo $user->getUserID() ?> />
<!--restore-->			
			<form action="<?php echo $dir;?>" method="post" style="display:inline;">
				<button style="display:none;" id="restore_button"></button>
			</form>
				<button class="blue" id="dummyRestore_button"><span class="icon">&#128281;</span> Restore </button>
<!---delete-->
			<form style="display:inline;" action="<?php echo $dir;?>" method="post">
					<button style="display:none;" id="delete_button"></button>
				</form>
					<button class="orange" id="dummyDelete_button"><span class="icon">&#59177;</span> Delete </button>
			</div>
		
		<div id="dropzone">
			<ul id="fileSystem">
				<?php echo $contentString;?>
			</ul>
		</div>
	</div>
</section>
<?php echo $this->getJsAssets();?>
</body>
	
	