
	<section class="widget">
		<header>
			<span class="icon">&#57349;</span>
			<hgroup>
				<h1>Accounts List</h1>
			</hgroup>
		</header>
                    <div class="toolbar">
                        <form method="get" action="/admin/users/list/">
                          <input name="q" type="searchString" placeholder="search" style="width:200px; margin-top:-10px;" class="pull-right">
                        </form>
                        <a class="button green" href="#createAccount" rel="modal:open"><span class="icon">&#59136;</span> Create Account</a>
                        <a class="button blue" href="<?php echo SITE_URL.'admin/users/import';?>" ><span class="icon">&#59213;</span> Import Accounts</a>
                        <a class="button" href="#settingsModal" rel="modal:open"><span class="icon">&#9881;</span> Settings</a>
                        
                    </div>

		
		<table id="myTable" class="clearfix" width="100px">
			<thead>
			<tr>
                            <th></th>
                            <th><a href="javascript:;"class="sortable">Last name</a></th>
                            <th><a href="javascript:;" class="sortable">First name</a></th>
                            <th><a href="javascript:;" class="sortable">Email</a></th>
                            <th><a href="javascript:;" class="sortable">Storage Used</a></th>
                            <th><a href="javascript:;" class="sortable">User Type</a></th>
                            <th><a href="javascript:;"></a></th>
                            <th></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($users as $key => $user){ ?>
			
				<tr>
					<td class="avatarColumn"><img src="<?php echo $user->getAvatarUrl();?>" width="20px"/></td>
					<td class="lastNameColumn"><?php echo $user->getLastName(); ?></td>
					<td class="firstNameColumn"><?php echo $user->getFirstName(); ?></td>
					<td class="emailColumn"><?php echo $user->getEmail(); ?></td>
                                      <?php if ($user->getUserType() != 1): ?>
					<td class="usedStorageColumn"><?php echo $user->getUsedStoragePercent(); ?>%</td>
                                      <?php else: ?>  
					<td class="usedStorageColumn"><?php echo $user->getUsedStorage(); ?> Bytes</td>
                                      <?php endif; ?>
					<td class="userTypeColumn"><?php echo $user->getUserTypeInstance()->getDescription(); ?></td>
					<td class="userStatusColumn"><?php echo $user->getStatusButton();?></td>
					
                                        <form method="post" action="" >
                                        <td>
                                            <a class="button small view_drive" title="Open drive"><input type="hidden" value="<?php echo $user->getUserID(); ?>" ><span class="icon">&#59256;</span></a>
											<input type="hidden" name="password" value="<?php echo $user->getLastName();?>" />
                                            <button button class="button small"  title="Reset password" onClick="javascript:if (!confirm('You are about to reset <?php echo $user->getFirstName().' '.$user->getLastName();?>\'s password. Please confirm.')){return false;}" name="resetPassword" value="<?php echo $user->getEmail();?>"><span class="icon">&#128275; </span></button>
                                            <a class="button small view_user_detail" title="Account Details"><input type="hidden" value=<?php echo $user->getUserID(); ?> ><span class="icon">&#128100; </span></a>
                                        </td>
					</form>
				</tr>
			<?php } ?>
                        </tbody>
		</table>
                <?php echo $pagination; ?>
	</section>
        <div class="modal" id="settingsModal">
                <h3 id="title">Settings</h3>
                <hr/>
                <div id="modalContents">
                    <div class="content">
                        <form method="post" action="/admin/users/list">
                            <div class="checkboxGroup">
                                <h4>User Types:</h4>
                                <label for="userTypeStudents">Students
                                    <input type="checkbox" id="userTypeStudents" name="userType[]" value="<?php echo UserFlags::STUDENT; ?>" <?php echo ($flags & UserFlags::STUDENT) ? 'checked':''?>>
                                </label>
                                
                                <label for="userTypeFaculty">Faculty
                                    <input type="checkbox" id="userTypeFaculty" name="userType[]" value="<?php echo UserFlags::FACULTY; ?>" <?php echo ($flags & UserFlags::FACULTY) ? 'checked':''?>>
                                </label>
                                
                                <label for="userTypeAdministrators">Administrators
                                    <input type="checkbox" id="userTypeAdministrators" name="userType[]" value="<?php echo UserFlags::ADMIN; ?>"  <?php echo ($flags & UserFlags::ADMIN) ? 'checked':''?>>
                                </label>
                            </div>
                            <div class="radioGroup">
                                <h4>User Status:</h4>
                                <label for="userStatusActive">Active
                                    <input type="radio" id="userStatusActive" name="userStatus" value="<?php echo UserFlags::ACTIVE; ?>"  <?php echo ($flags & UserFlags::ACTIVE) ? 'checked':''?>>
                                </label>
                                
                                
                                <label for="userStatusSuspicious">Suspicious
                                    <input type="radio" id="userStatusSuspicious" name="userStatus" value="<?php echo UserFlags::SUSPICIOUS; ?>"  <?php echo ($flags & UserFlags::SUSPICIOUS) ? 'checked':''?>>
                                </label>
                                
                                <label for="userStatusSuspended">Suspended
                                    <input type="radio" id="userStatusSuspended" name="userStatus" value="<?php echo UserFlags::SUSPENDED; ?>"  <?php echo ($flags & UserFlags::SUSPENDED) ? 'checked':''?>>
                                </label>
                                
                                <label for="userStatusInactive">Inactive
                                    <input type="radio" id="userStatusInactive" name="userStatus" value="<?php echo UserFlags::INACTIVE; ?>"  <?php echo ($flags & UserFlags::INACTIVE) ? 'checked':''?>>
                                </label>
                            </div>
                            <hr/>
                            <button class="green pull-right" id="submitButton">Ok</button>
                            <input type="hidden" id="sortColumn" name="sortColumn" value="<?php echo $sortColumn;?>">
                            <input type="hidden" id="sortOrder" name="sortOrder" value="<?php echo $sortOrder;?>">
                        </form>
                    </div>
                </div>
        </div>
        <div class="modal" id="setStatusModal">
                <h3 id="title">Edit <span id="userFullName"></span> Status</h3>
                <hr/>
                <div id="modalContents">
                    <div class="content">
                        <form method="post" action="">
                           <div class="radioGroup">
                                <label for="setUserStatusActive">Active
                                    <input type="radio" id="setUserStatusActive" name="setUserStatus" value="<?php echo User::STATUS_ACTIVE; ?>">
                                </label>
                                
                                <label for="setUserStatusSuspicious">Suspicious
                                    <input type="radio"id="setUserStatusSuspicious" name="setUserStatus" value="<?php echo User::STATUS_SUSPICIOUS; ?>">
                                </label>
                                
                                <label for="setUserStatusSuspended">Suspended
                                    <input type="radio" id="setUserStatusSuspended" name="setUserStatus" value="<?php echo User::STATUS_SUSPENDED; ?>">
                                </label>
                                
                               <input type="hidden" id="userIdForStatus" name="userIdForStatus" value="">
                            </div>
                            <hr/>
                            <button class="green">Edit Status</button>
                        </form>
                    </div>
            </div>
        </div>
		<div class="modal" id="createAccount">
			<h3 id="title">Choose User type</h3>
           
			<div id="modalContents" style="padding:30px 0px;">
                            <a class="button blue" href="#createStudent" rel="modal:open"><span class="icon">&#59170;</span> Student</a>
                            <a class="button black" href="#createFaculty" rel="modal:open"><span class="icon">&#127891;</span> Faculty</a>
                            <a class="button orange" href="#createAdmin" rel="modal:open"><span class="icon">&#9874;</span> Administrator</a>
			</div>
		</div>
		<div class="modal" id="createStudent">
                    <h3 id="title">Create Student Account</h3>
                    <hr/>
			<div id="modalContents">
				<form method="post" action="">
					<div class="field-wrap">
						<label for="idno">ID Number:</label>
						<input type="text" name="idno" id="idno" required placeholder="Type Id Number">
					</div>
					<div class="field-wrap">
						<label for="fname">First Name:</label>
						<input type="text" name="fname" id="fname" required placeholder="Type First Name">
					</div>
					<div class="field-wrap">
						<label for="lname">Last Name:</label>
						<input type="text" name="lname" id="lname" required placeholder="Type Last Name">
					</div>
					<button class="green" name="sConfirm" value="3"> Create Account</button>
					<a class="button black" href="#createAccount" rel="modal:open"><span class="icon">&#59154;</span>Back</a>
				</form>
			</div>
		</div>
		<div class="modal" id="createFaculty">
			<h3 id="title">Create Faculty Account</h3>
            <hr/>
			<div id="modalContents">
				<form method="post" action="">
					<div class="field-wrap">
					<label for="email">E-mail address:</label>
					<input type="email" name="femail" id="email"required placeholder="Type Email Address">
				</div>
				<div class="field-wrap">
					<label for="Password">Password:</label>
					<input type="password" name="fpassword" id="password"required placeholder="Type Password">
				</div>
					<button class="green" name="fConfirm" value="2">Create Account</button>
					<a class="button black" href="#createAccount" rel="modal:open"><span class="icon">&#59154;</span>Back</a>
				</form>
			</div>
		</div>
		<div class="modal" id="createAdmin">
			<h3 id="title">Create Administrator Account</h3>
            <hr/>
			<div id="modalContents">
				<form method="post" action="">
					<div class="field-wrap">
					<label for="email">E-mail address:</label>
					<input type="email" name="aemail" id="email" required placeholder="Type Email Address">
				</div>
				<div class="field-wrap">
					<label for="Password">Password:</label>
					<input type="password" name="apassword" id="password"required placeholder="Type Password">
				</div>
					<button class="green" name="aConfirm" value="1">Create Account</button>
					<a class="button black" href="#createAccount" rel="modal:open"><span class="icon">&#59154;</span>Back</a>
				</form>
			</div>
		</div>														
		<!---view user drive-->
		<form style="display:none" action="<?php echo SITE_URL.'admin/users/drive/'; ?>" method="post" target="view_drive">
			<input type="hidden" id="viewdriveid" name="user_id" value="">
			<button id="viewdrivebutton" style="display:none"></button>
		</form>
		<div class="modal" id="modalviedrive" style="padding:3px;">
			<iframe id="iframeviewdrive" name="view_drive" style="border-radius:5px">
			</iframe>
		</div>
		<!--view user detail-->
		<form style="display:none" action="<?php echo SITE_URL.'admin/users/details'; ?>" method="post" target="view_user_detail">
			<input type="hidden" id="viewuserdetailid" name="user_id" value="">
			<button id="viewuserdetailbutton" style="display:none"></button>
		</form>
		<div class="modal" id="modalviewuserdetail">
			<iframe id="iframeviewuserdetail" name="view_user_detail" style="border-radius:5px">
			</iframe>
		</div>