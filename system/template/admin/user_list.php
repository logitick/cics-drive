
	<section class="widget">
		<header>
			<span class="icon">&#128101;</span>
			<hgroup>
				<h1>User Activity Logs and Reports</h1>
			</hgroup>
		</header>
				
		<div class="toolbar">
                       <a class="button" href="#settingsModal" rel="modal:open"><span class="icon">&#9881;</span> Settings</a>
                       <a class="button" href="#daterange" rel="modal:open"><span class="icon">&#9881;</span> Date Range</a>
                       <form action="<?php echo SITE_URL.'admin/logs/users';?>" style="display:inline" method="post">
                           <!-- <input type="text" placeholder="Search User" name="user" value="" style="width: 300px;">-->
                       </form>
                </div>
		<table id="myTable" class="clearfix" width="100px">
			<thead>
				<tr>
					<th><a>Description</a></th>
					<th><a>Date</a></th>
					<th><a>Time</a></th>	
				</tr>
			</thead>
			<tbody>
                              <?php echo $results; ?>
			</tbody>
		</table>
		<?php echo $pagination; ?>
	</section>
	 <div class="modal" id="settingsModal">
                <h3 id="title">Settings</h3>
                <hr style="border: 1px solid black"/> 
                <div id="modalContents" style="margin-top: -10px;">
                    <div class="content">
                        <form method="post" action="<?php echo SITE_URL.'admin/logs/users';?>" >
                                <h4>User Types:</h4>
                                <div  class="checkboxGroup" id="usertypes">
                                  <ul>
                                    <li>
                                          <input type="checkbox" <?php echo array_search(3, $session['userType']) !== false ? 'checked':'';?> id="userTypeStudents" name="userType[]" value="3" >
                                      <label for="userTypeStudents">Students</label> 
                                    </li>
                                    <li>
                                          <input type="checkbox" <?php echo array_search(2, $session['userType']) !== false ? 'checked':'';?> id="userTypeFaculty" name="userType[]" value="2" >
                                      <label for="userTypeFaculty">Faculty</label>
                                    </li>
                                    <li>
                                          <input type="checkbox" <?php echo array_search(1, $session['userType']) !== false ? 'checked':'';?> id="userTypeAdministrators" name="userType[]"  value ="1">
                                      <label for="userTypeAdministrators">Administrators</label>   
                                    </li>
                                  </ul>

                                </div>
                            <hr/>
                            <h4>Category:</h4>
                                <div id="category">
                                     <select name="category">
                                            <option value="0">All</option>
                                            <?php foreach($catOptions as $key => $option) {
                                              if ($option == $session['category']) {
                                                echo '<option selected value="'.$option.'">'.$key.'</option>';
                                              } else {
                                                echo '<option value="'.$option.'">'.$key.'</option>';
                                              }
                                              
                                            }
                                            ?>
                                     </select>
                                </div>
                            <hr/>
                            <button class="green pull-right" id="submitButton">Ok</button>
                            <input type="hidden" id="sortColumn" name="sortColumn" ">
                            <input type="hidden" id="sortOrder" name="sortOrder" >
                        </form>
                    </div>
                </div>
        </div>
        <div class="modal" id="daterange">
             <form method="post" action="<?php echo SITE_URL.'admin/logs/users';?>" >
                    <h4>Date range:</h4>
                    <div id="date">
                         <label for="startdate"><strong>Start Date</strong></label>

                         <input id="startdate" type=""text name="startdate" value="<?php echo $session['start'] != null ? $session['start']->format('Y/m/d'):'';?>">
                      
                         <label for="enddate"><strong>End Date</strong></label>
                         <input id="enddate" type=""text name="enddate" value="<?php echo $session['end'] != null ? $session['end']->format('Y/m/d'):'';?>">
                    </div>
                    <button class="green pull-right" id="submitButton">OK</button>
             </form>
        <div>
        