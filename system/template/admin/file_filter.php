
	<section class="widget">
		<header>
			<span class="icon">&#128273;</span>
			<hgroup>
				<h1>File Filter</h1>
			</hgroup>
		</header>
		<div class="content">
			<form method="post" action="" >
			<table id="">
				<tr>
					<td><input type="checkbox" name="faculty" value="1"> Faculty</input></td>
					<td><input type="checkbox" name="student" value="1"> Student</input></td>
					<td><input type="text" name="extension" id="extension" placeholder="Type File Extension" style="width:400px;"></td>
					<td><button name="add" class="green" value="add" style="width:130px"><span class="icon">&#8627;</span>Add</button> </td>
				</tr>
			</table>
			<table id="myTable" class="" width="100" border="0">
				<thead>
				<tr>
					<th>Extension</th>
					<th>Faculty</th>
					<th>Student</th>
					<th>Remove</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($filters as $key => $data) { ?>
						<tr id="<?php echo($key); ?>">	
						<td id="extension">
							<?php echo($data['extension']); ?> 
						</td>
						<td> 
							<?php if($data['forFaculty'] != 0){$check = 'checked';}else{$check ='';}?>
							<input class="forFaculty" type="checkbox" <?php echo $check;?> name="Ffilter" value="1"></input>
						</td>
						 <td> 
							<?php if($data['forStudent'] != 0){$che = 'checked';}else{$che ='';}?>
							<input class="forStudent" type="checkbox" <?php echo $che;?> name="Sfilter" value="1"></input>
						 </td>
						<td><button name="delete" class="red" value="<?php echo($data['extension']);?>" style="padding:3px;"><span class="icon">&#10006;</span></button> </td>
					</tr>
				<?php
				}
				?>
			</tbody>
			</table>
			
			</form>
		</div>
	</section>