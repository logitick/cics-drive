<?php
/**
 * A wrapper for the built-in PDO Class.
 *
 * This class uses the Singleton design pattern ({@link http://en.wikipedia.org/wiki/Singleton_pattern}).
 * An instance is automatically created when invoking 
 * The object also automatically disconnects from the Database after execution 
 * of the script. {@link Database::getInstance()}
 * 
 * @package library
 * @filesource /system/library/Database.php
 */
class Database
{
    /**
     *
     * @const SUCCESS_CODE The code that MySQL returns if a query was executed successfuly.
     */
    const SUCCESS_CODE = 0;

    private static $instance = null;
    private $_dbh;
    private $_stmt;
    private $_queryCounter = 0;
    private $conn;

    private function __construct($user, $pass, $dbname, $host)
    {
        $dsn = 'mysql:'.$host.'=localhost;dbname=' . $dbname;

        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                        PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
                        );
                       
        $this->_dbh = new PDO($dsn, $user, $pass, $options);
    }
      
    
    public static function toSqlTimestamp(\DateTime $date = null) {
      if ($date == null) {
        return null;
      }
      return $date->format(\DatabaseEntity::SQL_DATETIME_FORMAT);
    }

    /**
     * getInstance() - Returns the instance of the Database.
     * @return Database
     */
    public static function getInstance() 
    {
      if (self::$instance == null) 
        try {
            self::$instance = new Database(DB_USERNAME, DB_PASSWORD, DB_NAME, DB_HOST);
        } catch (PDOException $e) {
            throw new DriveRuntimeException('Could not connect to the database', $e->getCode(), $e);
        }
      return self::$instance;
    }

    /**
     * query() - Prepares a query for execution.
     * Calling this method will create a prepared statement that is ready for 
     * execution after binding the data.
     * @return void
     * @see # Database::bind()
     */
    public function query($query)
    {
        $this->_stmt = $this->_dbh->prepare($query);
    }

    /**
     * bind() - Binds or attaches the data to the prepared statement.
     * The PDO object will automatically filter for injections.
     * @param string $index The index to bind the data with.
     * @param int|boolean|null|string $value The value to store with the record.
     * @param int $type (Optional) The datatype of the value given.
     * @return void
     */
    public function bind($index, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->_stmt->bindValue($index, $value, $type);
    }

    /**
     * execute() - Executes the prepared statement created from 
     * Database::query().
     * @return boolean True on success or false on failure.
     */
    public function execute()
    {
        $this->_queryCounter++;
        return $this->_stmt->execute();
    }


    /**
     * resultset() - Get the result set from the previously executed statement.
     * This will only work for SELECT statements.
     * @return array An associative array of the rows that match the SELECT statement.
     */
    public function resultset()
    {
        $this->execute();
        return $this->_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * single() - Get the single result from the previously executed statement.
     * This will only work for SELECT statements.
     * @return array An associative array of the row that match the SELECT statement.
     */
    public function single()
    {
        $this->execute();
        return $this->_stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * lastInsertId() - Gets the primary key of the last inserted row.
     * This method must be invoked before closing a transaction.
     * @return int The last inserted ID.
     */
    public function lastInsertId()
    {
        return $this->_dbh->lastInsertId();
    }

    /**
     * beginTransaction() - Notifies the DBMS to start a transaction.
     * @return boolean True on success or false on failure.
     */
    public function beginTransaction()
    {
        return $this->_dbh->beginTransaction();// must be innoDatabase table
    }

    /**
     * endTransaction() - Notifies the DBMS to end a transaction.
     * This will also commit the changes.
     * @return boolean True on success or false on failure.
     */
    public function endTransaction()
    {
        return $this->_dbh->commit();
    }

    /**
     * cancelTransaction() - Ends the transaction and ignore the changes to the
     * table.
     * @return True on success or false on failure.
     */
    public function cancelTransaction()
    {
        return $this->_dbh->rollBack();
    }

    /**
     * rowCount() - Gets the number of rows that were affected by the last 
     * statement executed.
     * @return int Number of rows updated, deleted, or inserted.
     */
    public function rowCount()
    {
        return $this->_stmt->rowCount();
    }

    /**
     * queryCounter() - Gets the number of queries executed in one HTTP Request.
     * @return int Number of executions.
     */
    public function queryCounter()
    {
        return $this->_queryCounter;
    }

    /**
     * debugDumpParams() - Displays debug information about the last query.
     * @return void 
     * Dumps the informations contained by a prepared statement directly on the 
     * output. It will provide the SQL query in use, the number of parameters 
     * used (Params), the list of parameters, with their name, type (paramtype) 
     * as an integer, their key name or position, the value, and the position 
     * in the query (if this is supported by the PDO driver, otherwise, it will 
     * be -1). 
     * This is a debug function, which dump directly the data on the normal output. 
     */
    public function debugDumpParams()
    {
        $this->_stmt->debugDumpParams();
    }


    /**
    * getErrorCode() - Gets the code return by the  DBMS after executions of a 
    * statement.
    * @return int The error code.
    * @see Database::SUCCESS_CODE Use this constant during validation.
    */
    public function getErrorCode() {
        return $this->_stmt->errorCode();
    }

    /**
     * __desctruct() - Responsible for closing the connection from the database.
     */
    public function __destruct() {
      $this->_dbh = null;
    }
}