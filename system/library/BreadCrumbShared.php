<?php
require_once LIBRARY.'BreadCrumb.php';
class BreadCrumbShared extends BreadCrumb {

	
	public function getTrailHtml() {
		$temp = 'Shared Folders';
 		$str = '<a href="'.$this->getBaseUrl().'">'.$temp.'</a>';
 		$path = '';
 		foreach ($this->getTrail() as $i => $trail) {
 			$path .= '/'.$trail;
 			$str .= '<span class="icon breadcrumb">&#59234;</span> <a href="'.$this->getBaseUrl().$path.'">'.$trail.'</a>';
 		}
 		return $str;
	}


}
