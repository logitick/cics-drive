<?php
require_once LIBRARY.'drive/DriveItem.php';
require_once MODEL.'User.php';
require_once MODEL.'Util.php';
require_once MODEL.'FileFilters.php';
require_once MODEL.'FileActivityLogs.php';
require_once MODEL.'FileActivityProperties.php';

class File extends DriveItem {

	//User 
	private $User;

	public static $knownFileTypes = array( // file types known by the editor
			'c' => 'text/x-csrc',
			'cpp' => 'text/x-c++src',
			'cs' => 'text/x-csharp',
			'css' => 'text/css',
			'h' => 'text/x-csrc',
			'htm' => 'text/html',
			'html' => 'text/html',
			'java' => 'text/x-java',
			'js' => 'text/javascript',
			'json' => 'application/json',
			'php' => 'text/x-php',
			'sql' => 'text/x-sql',
			'vb' => 'text/x-vb',
			'xml' => 'application/xml',
		);

	private static $imageFileTypes = array( // images
		'jpe' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'gif' => 'image/gif',
		'png' => 'image/png',
		'png' => 'image/png',
		'tif' => 'image/tif	'
	);

	/**
	*	Constructor
	*	@Parameter  absolute path
	*/
	public function __construct($file, $user = null) {
		if ($file instanceof File)
			$this->setPath($file->getPath());
		else 
			$this->setPath($file);

		$this->User = $user;
	}


    public function copy($destination) {
      if (is_dir($destination)) {
        $destination .= '/'.$this->getName();
      }
      if (copy($this->getPath(), $destination)) {
        return new File($destination);
      }
      return null;
    }
    
    public static function createFile($absPath, $contents) {
      return file_put_contents($absPath, $contents);
    }

	/**
	 * getExtension() - Looks for the extension from a file name.
	 * @param string $name The filename to get the extension from.
	 * @return string The extension extracted from the filename.
	 */
	public static function getExtension($name) {
		$name = strtolower((preg_match('/\)$/',$name) > 0)?substr($name,0,strripos($name,'(')):$name);
		$extStartIndex = strrpos($name, '.'); // the index of the start of the file extension

		if (!$extStartIndex)
			return null;

		return substr($name, $extStartIndex + 1, strlen($name));
	}

        /**
         * @method getMimeType - acquire the mime type of a specific file
         * @param type $byExtension
         * @return type 
         */
	public function getMimeType($byExtension = false) {
		if ($byExtension) {
			$extension = self::getExtension($this->getName());
			if (!array_key_exists($extension, self::$knownFileTypes))
				$mime = '';
			else
				$mime = self::$knownFileTypes[$extension];
		} else {
			$res = finfo_open(FILEINFO_MIME_TYPE);
			$mime = finfo_file($res, $this->getPath());
			finfo_close($res);
		}
		return $mime;
	}
	
        /**
         * @method getContents - get the contents of a file
         * @return type 
         */
	public function getContents(){
		return file_get_contents($this->getPath());
	}

        /**
         * @method isEditable - check if it is a text file
         * @return type 
         */
	public function isEditable() {
		return substr($this->getMimeType(), 0, 4) == 'text' || isset(self::$knownFileTypes[strtolower(self::getExtension($this->getName()))]);
	}
	
        /**
         * @method isAnImage - check if it is a image file
         * @return type 
         */
	public function isAnImage() {
		return substr($this->getMimeType(),0,5) == 'image';
	}
	
        /**
         * @method getImage 
         * @return type 
         */
	public function getImage() {
		header('Content-Type:'.$this->getMimeType());
		return readfile($this->getPath());
	}
        
        public function overwrite($contents) {
          $oldSize = $this->getSize();
          file_put_contents($this->getPath(), $contents);
          clearstatcache();
          $newSize = filesize($this->getPath());
          $newSize = ($this->User->getUsedStorage() - $oldSize) + $newSize;
          $this->User->setUsedStorage($newSize);
          $this->User->updateRecord();
        }
        
        
	
        
        /**
         * @method getFolderName - acquiring the name of the directory where this file belongs
         * @return type 
         */
	public function getFolderName() { // the file's containing folder
		$user = Session::getUser();

		$folder = str_replace('/', '', substr($this->getFolderPath(), strrpos($this->getFolderPath(), '/', -2)));
		return $folder == $user['userId'] ? '/':$folder; // if folder is root return /, else return the folder name
	}

        /**
         * @method getFolderPath - acquiring the path of the directory where this file belongs
         * @return type 
         */
	public function getFolderPath() {
                $ctr = strrpos($this->getPath(),$this->getName());
                $temp = substr($this->getPath(),0,$ctr);
                return rtrim($temp,'/');
	}

	/**
	*	@method download 
	*/
	public function download() {
		$us = Session::getUser();
                $us = $us['userId'];
                $temp_user = User::getByID($us);
		$check = (preg_match('/\)$/',$this->getName()) > 0);
		$fileToDownload = $check?Folder::tempFolder($temp_user->getUserID(),$this):$this;
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$this->getName().'"');
		header('Content-Transfer-Encoding: binary');
		header('Connection: Keep-Alive');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($this->getPath()));
		readfile($this->getPath());
		
		$actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_DOWNLOAD,$temp_user->getUserID() );
		FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRelPath() );
		exit;
	}
	
	/**
	*	@method uploadFile - upload the file
	*	@parameter string, string ,string, int, int - 
	*				string - temp_path
	*				path - file path
	*				name - file name
	*				size - file size
	*				status - upload status status
	*	@return boolean
	*/
	private function uploadFile($temp_path, $path, $name, $size, $status ){
		if( move_uploaded_file( $temp_path, $path.'/'. $name) ){
			Uploads::insertUploaded( $this->getRelPath().'/'.$name, $this->User->getUserID(), $status);
			$this->User->setUsedStorage($size);
			$this->User->updateRecord();
			if($status == UPLOAD_ERR_OK){
                            $actId = FileActivityLogs::logFileAcivity(FileActivityLogs::CATEGORY_UPLOAD,$this->User->getUserID());
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRelPath().'/'.$name );
			}
			return true;
		}
	}
	
	/**
	*	@method antivirusScan - scan the file for any potential virus
	*	@param string - file absolute path
	*	@return false if there is no virus, otherwise true
	*/
	private function antivirusScan($file){
		$out = '';
		$int = -1;
		$clamscan = '"C:/Program Files/ClamWin/bin/clamscan.exe"';	
		$clamDB = '--database="C:/ProgramData/.clamwin/db"';
		$scan = "$clamscan $clamDB $file";
		
		exec($scan,$out,$int);
		
		if($int == 0)
			return false;
		
		return true;
	}
	
	/**
	*	@method initUpload - initialize file to be uploaded, check file validity
	*	@parameter - array, int, boolean	
	*	@return int - Upload status
	*/
	public function initUpload($file, $storageCapacity, $overwrite){
		$path = $this->getPath(); 
		$finfo = new finfo;
		
		foreach ($file["file"]["error"] as $key => $error) { 
			
			switch($error){
				case 0:	$name = $file["file"]["name"][$key];
						            
						//check if file is in being filtered
						if( ($this->User->getUserType() != 1) && FileFilters::isExtensionNotValid($this->getExtension($name), $this->User->getUserType()) ){
							Uploads::insertUploaded( $this->getRelPath().'/'.$name, $this->User->getUserID(), Uploads::FILE_FILTER_REJECT);
							return array(Uploads::FILE_FILTER_REJECT,$this->getRelPath());
						}
						
						//scan file by anti-virus clamav
						if(AV_STATUS && !$overwrite && $this->antivirusScan($file["file"]["tmp_name"][$key])){
							Uploads::insertUploaded( $this->getRelPath().'/'.$name, $this->User->getUserID(), Uploads::ANTIVIRUS_REJECT);
							return array(Uploads::ANTIVIRUS_REJECT,$this->getRelPath());
						}
						
						//reject if the uploaded item is a folder
						/*if( strpos($finfo->file($file["file"]["tmp_name"][$key], FILEINFO_MIME),'inode') === 0 ){
							Uploads::insertUploaded( $this->getRelPath().'/'.$name, $this->User->getUserID(), Uploads::UNKNOWN_FILE);
							return Uploads::UNKNOWN_FILE; 
						}*/
						
						//check file size if it reaches the storage limit						
						if( ($this->User->getUserType() != 1) && $file["file"]["size"][$key] > ($storageCapacity - $this->User->getUsedStorage()) ){
							Uploads::insertUploaded( $this->getRelPath().'/'.$name, $this->User->getUserID(), Uploads::INSUFFICIENT_STORAGE_SPACE);
							return array(Uploads::INSUFFICIENT_STORAGE_SPACE,$this->getRelPath());
						}	
 						
						//overwrite uploaded file
						if((boolean)$overwrite){
							//subtract the file size to be overwritten and add the file size of that will be overwriten
							$temp_size = ($this->User->getUsedStorage() - filesize($path.'/'. $name)) + $file["file"]["size"][$key]; 
							
							//overwrite file
							if( $this->uploadFile($file["file"]["tmp_name"][$key],$path, $name, $temp_size, Uploads::OVERWRITE ) ){
								return array(UPLOAD_ERR_OK,$this->getRelPath()); 
							}
						}	
						
						//check if file is already exist
						if(file_exists($path.'/'. $name)){
							Session::set('owfile',$name);
							
							Uploads::insertUploaded( $this->getRelPath().'/'.$name, $this->User->getUserID(), Uploads::FILE_EXIST);
							return array(Uploads::FILE_EXIST,$this->getRelPath());
						}
						
						//add current storage to the file size
						//upload the file
						$fSize = $file["file"]["size"][$key] + $this->User->getUsedStorage();
						if( $this->uploadFile($file["file"]["tmp_name"][$key],$path, $name, $fSize, UPLOAD_ERR_OK ) ){
							return array(UPLOAD_ERR_OK,$this->getRelPath()); 
						}
						
				case 1:	return UPLOAD_ERR_INI_SIZE;//file size exceeds the limit size in php.ini	
				case 2:	return UPLOAD_ERR_FORM_SIZE;//file size exceeds the limit set by the admin
				case 3:	return UPLOAD_ERR_PARTIAL;//interrupted file upload
				case 4:	return UPLOAD_ERR_NO_FILE;//unknown file
				case 6:	return UPLOAD_ERR_NO_TMP_DIR;//internal error
				case 7:	return UPLOAD_ERR_CANT_WRITE;//internal error
				case 8: return UPLOAD_ERR_EXTENSION;//invalid extension
			}
		}   
	}
	
	/**
	*	@method rename - changing the current name 
	*	@parameter - string, string
	*	@return true on success, othewise false
	*/
	public function rename( $oldName, $newName ){
		if ( in_array($newName,scandir($this->getFolderPath())) ){
			return false;		
		}
		$result = rename( $this->getPath(), $this->getFolderPath().'/'.$newName );
		if($result){
			$actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_RENAME,$this->User->getUserID() );
			FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_OLD_PATH, $this->getRelPath() );
			FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_NEW_PATH, str_replace( $this->getName(), $newName, $this->getRelPath() ) );
		}
		return $result;
	}
	
	/**
	*	@method delete - permanently delete the folder from the drive
	*	@parameter boolean(optional)
	*	@return true on sucess, otherwise false;
	*/
	public function delete( $uploadCancelled = null, $archive = null, $overwrite = null, $name=null,$backup=null,$publication=null ){
                
                if((boolean)$publication){
					return unlink($this->getPath());
                }
                if($this->User->getUsedStorage() == 0 && Session::get('view_user_id') == null ){
					return unlink($this->getPath());
				}
				
                if( $uploadCancelled && !(boolean)$overwrite ){
                    Uploads::cancelUplaod( $this->getRelPath(), $this->User->getUserID(), UPLOAD_ERR_OK);
                    if($this->getName() == $name){
                        return true;
                    }
                }else if( $uploadCancelled && (boolean)$overwrite ){
                    Uploads::cancelUplaod( $this->getRelPath(), $this->User->getUserID(), UPLOAD_ERR_OK);
				}else{
                    $us = Session::getUser();
                    $us = $us['userId'];
					
                    if( Session::get('view_user_id')!=null  ){
                        $viewuser = User::getByID(Session::get('view_user_id')); 
                        $temp_size = $viewuser->getUsedStorage() - $this->getSize();	
                        $viewuser->setUsedStorage( $temp_size );
                        $viewuser->updateRecord();
                        
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ADMIN_DELETE,$viewuser->getUserID() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRecycleRelPath());
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_ADMIN, $us);
                    }else if((boolean)$archive){
                        $user = User::getByID($us);
                        
                        $user->setUsedStorage( $user->getUsedStorage() - $this->getSize() );
                        $user->updateRecord();
                        
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_DELETE_ARCHIVE,$user->getUserID() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getArchiveRelPath() );
                    }else if((boolean)$backup){
                        /*$actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_DELETE_BACKUP,$us );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getBackupRelPath() );*/
                    }else{
                        $this->User->setUsedStorage($this->User->getUsedStorage() - $this->getSize());
                        $this->User->updateRecord();

                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_GEN_DELETE,$this->User->getUserID() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRecycleRelPath() );
                    }
		}
		return unlink($this->getPath());
	}
	
	/**
	*	@method remove - remove the folder and move it into the Item Recovery 
	*	@parameter string
	*	@return true on success, otherwise false
	*/
	public function remove($root){
		$name = $this->getName();
		$recycleItem='';
		$temp_path='';
		$result=false;
		$viewuser='';
		
		if ( Session::get('view_user_id') != null /*&& Session::get('admin_id')==null*/){
			$us = Session::get('user');
			$us = $us['userId'];
			
                        $temp_path = new Folder(ADMINRECYCLE.Session::get('view_user_id').'/'); 
			$recycleItem = array_diff(scandir($temp_path->getPath()),array('.','..'));
			$viewuser = User::getByID(Session::get('view_user_id'));
                        
			$name = Util::checkDuplicate($recycleItem,$name);
			$result = rename($this->getPath(),$temp_path->getPath().$name);
		}else{
			$recycleItem = array_diff(scandir(Folder::recycleRoot($this->User->getUserID())->getPath()),array('.','..'));
                        
			$name = Util::checkDuplicate($recycleItem,$name);
			$result = rename($this->getPath(),$root.DriveItem::RECYCLE.$name); 
		}

		if($result){
			if (Session::get('view_user_id') != null /*&& Session::get('admin_id')==null*/){				
                            $temp_size = $viewuser->getUsedStorage() - $this->getSize();
                            $viewuser->setUsedStorage( $temp_size );
                            $viewuser->updateRecord();
                            $us = Session::getUser();
                            
                            $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ADMIN_REMOVE, $viewuser->getUserID() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_OLD_PATH, $this->getRelPath() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_NEW_PATH, $name );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_ADMIN, $us['userId']);
			}else{
                            $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_GEN_REMOVE,$this->User->getUserID() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_OLD_PATH, $this->getRelPath() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_NEW_PATH, $name );
                        }
		}
		
		return $result;
	}
	
	/**
	*	@method restore - restore the folder and its content to the drive from the recycle bin
	*	@return true on success, otherwise false
	*/
	public function restore(){
		if( Session::get('view_user_id') != null ){
			$viewuser = User::getByID(Session::get('view_user_id'));
			
			$actID = FileActivityProperties::queryActivityIDAdmin( $this->getName(), FileActivityProperties::INDICATE_NEW_PATH, $viewuser->getUserID(), FileActivityLogs::CATEGORY_ADMIN_REMOVE );
			$entity = FileActivityProperties::getByID($actID);
		}else{
			$actID = FileActivityProperties::queryActivityID( $this->getName(), FileActivityProperties::INDICATE_NEW_PATH, $this->User->getUserID() );
			$entity = FileActivityProperties::getByID($actID);
		}
		
		$restoreDone=false;
		$folderPath;
		$temp;
		$toRestore = substr( $entity->getValue(),strripos($entity->getValue(),'/')+1,strlen($entity->getValue()) );
                
		if( substr_count($entity->getValue(), '/') == 1 ){
			if (Session::get('view_user_id') != null){				
				$viewuser = User::getByID(Session::get('view_user_id'));
				$temp_path = array_diff( scandir(DRIVE.$viewuser->getUserID().'/'),array('.','..',';recycle;') );
				$toRestore = Util::checkDuplicate($temp_path, $toRestore);
				
				$restoreDone = rename($this->getPath(),DRIVE.$viewuser->getUserID().'/'.$toRestore);
				$folderPath = '/';	
			}else{
				$temp_path = array_diff( scandir(DRIVE.$this->User->getUserID().'/'),array('.','..',';recycle;') );
				$toRestore = Util::checkDuplicate($temp_path,$toRestore);				
				
				$restoreDone = rename($this->getPath(),DRIVE.$this->User->getUserID().'/'.$toRestore);
				$folderPath = '/';	
			}
		}else{
			$folderPath = substr( $entity->getValue(),0, strripos($entity->getValue(),'/') ); 
			$paths = explode('/',$folderPath);	
			
			if (Session::get('view_user_id') != null){
				$viewuser = User::getByID(Session::get('view_user_id'));
				
				$folder = new Folder(DRIVE.$viewuser->getUserID(),$viewuser);
			}else{			
				$folder = new Folder(DRIVE.$this->User->getUserID(),$this->User);
			}
			
			foreach($paths as $key => $path){ 
				if(!empty($path)){
					if($key>1){
						$folder->createFolder($path,true);
						if (Session::get('view_user_id') != null){
							$viewuser = User::getByID(Session::get('view_user_id'));
							$folder = new Folder($folder->getPath().'/'.$path,$viewuser);
							
							$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
							$toRestore = Util::checkDuplicate($temp_path,$toRestore);
						}else{
							$folder = new Folder($folder->getPath().'/'.$path,$this->User);
							
							$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
							$toRestore = Util::checkDuplicate($temp_path,$toRestore);
						}
					}else{
						$folder->createFolder($path,true);
						if (Session::get('view_user_id') != null){
							$viewuser = User::getByID(Session::get('view_user_id'));
							$folder = new Folder($folder->getPath().'/'.$path,$viewuser);
							
							$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
							$toRestore = Util::checkDuplicate($temp_path,$toRestore);
						}else{
							$folder = new Folder($folder->getPath().'/'.$path,$this->User);
							
							$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
							$toRestore = Util::checkDuplicate($temp_path,$toRestore);
						}
					}
				}
			}
			
			$restoreDone = rename($this->getPath(),$folder->getPath().'/'.$toRestore); 
		}
		
		if($restoreDone){
                    if (Session::get('view_user_id') != null){//$this->User->getUserID()
                        $us = Session::getUser();
                        
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ADMIN_RESTORE,Session::get('view_user_id') );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, '/'.$this->getName() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_ADMIN, $us['userId']);
                    }
                    else{
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_GEN_RESTORE,$this->User->getUserID() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $entity->getValue() );
                    }
                        
                }
		return array($restoreDone,$folderPath);
	}
	
	/**
	*	@method getSize() - get the file size 
	*	@return int
	*/
	public function getSize() {
		if(file_exists($this->getPath())){
			return filesize($this->getPath());
		}
	}
	
	/**
	*	@method getFormatedSize() - get the formated file size 
	*/
	public function getFormatedSize() {
		return $this->formatBytes(filesize($this->getPath()),2);
	}
	
	/**
	* formatBytes() - convert bytes to KB,MB,GB, or TB
	* @param - size int
	* @param - precision
	*/
	function formatBytes($size, $precision = 2)
	{
		$base = log($size) / log(1024);
		$suffixes = array('B', 'KB', 'MB', 'GB', 'TB');   
		if($size == 0) return 0;
		return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
	}
      
    public function getUser() {
      return $this->User;
    }

    public function setUser($User) {
      $this->User = $User;
    }


}
