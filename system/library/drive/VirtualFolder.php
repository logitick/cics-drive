<?php

require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/PublicationsFolder.php';

class VirtualFolder extends Folder {
  protected $items;
  public function __construct($items = array()) {
    $this->setContents($items);
    $this->setPath(null);
  }
  
  public function getContents($recoveryMode = false) { // way gamit ang parameter ani. para ra ni sa gi extend nga Folder
    return $this->items;
  }
  
  public function addContent(DriveItem $item) {
    $this->items[$item->getName()] = $item;
  }
  
  public function getFolders() {
    $items = array();
    foreach ($this->items as $driveItem) {
      if ($driveItem instanceof Folder) {
        $items[] = $driveItem;
      }
    }
    return $items;
  }
  
  public function getContent($key) {
    if (key_exists($key, $this->items)) {
      return $this->items[$key];
    }
    return null;
  }
  
  public function getFiles() {
    $items = array();
    foreach ($this->items as $driveItem) {
      if ($driveItem instanceof Item) {
        $items[] = $driveItem;
      }
    }
    return $items;
  }
  
  public static function virtualFolderFactory($contents) {
    $folder = new VirtualFolder();
    foreach ($contents as $content) {
      if ($content instanceof DriveItem) {
        $folder->addContent($content);
      }
    }
    return $folder;
  }
  
  public function getFolder($name) {
    $folder = null;
    if ($this->items != null && is_array($this->items)) {
      foreach ($this->items as $driveItem) {
        if ($driveItem instanceof Folder && $driveItem->getName() == $name) {
          $folder = $driveItem;
        }
      }
    }
    return $folder;
  }
  
  

  public function setContents($items) {
    if (is_array($items)) {
      foreach ($items as $driveItem) {
        if ($driveItem instanceof DriveItem) {
          $this->items[] = $driveItem;
        }
      }
    }
  }
}