<?php
/**
 * @package library
 * @subpackage drive
 */

require_once LIBRARY.'Drive.php';
require_once MODEL.'FileActivityLogs.php';
	
/**
 * This class contains all the basic methods for 
 */
abstract class DriveItem {

	/**
	*	@var string the folder name for the recycle bin
	*/
	const RECYCLE = '/;recycle;/';
	
	/**
	 * @var string The item's name on the file system including its path.
	 */
	private $path;

	/**
	 * getName() - Gets the name of the drive item.
	 * @return string the name of the item.
	 */
	public function getPath() {
		return $this->path;
	}

	public function setPath($path) {
		$this->path = $path;
	}

	// get the file name of the item (eg folder name of file name)
	public function getName() {
		return basename($this->getPath());
	}


	// get relative path - gets the path relative to the user's root.
	public function getRelPath() {
		$user = Session::getUser();
		if(Session::get('view_user_id')!=null){
                    return str_replace(DRIVE.Session::get('view_user_id'), '', $this->getPath());
		}else{
                    return str_replace(DRIVE.$user['userId'], '', $this->getPath());
		}
	}
        
        public function getArchiveRelPath(){
            $user = Session::getUser();
            return str_replace(ARCHIVE.$user['userId'],'',$this->getPath());
        }
        
        public function getBackupRelPath(){
            $user = Session::getUser();
            return str_replace(BACKUP,'',$this->getPath());
        }
        
	/*
        public function getBackupRelPath(){
            return str_replace(BACKUP)
        }*/
	/**
	* get relative path from admin recycle bin
	*/
	public function getRecycleRelPath(){
		$user = Session::getUser();
		if(Session::get('view_user_id')!=null){
                    if( strripos($this->getPath(), ';ADMINRECYCLE;') > -1){
                        return str_replace(ADMINRECYCLE.Session::get('view_user_id'), '', $this->getPath());
                    }else if( strripos($this->getPath(), ';recycle;') > -1 ){
                        return '/'.str_replace(DRIVE.Session::get('view_user_id').self::RECYCLE, '', $this->getPath());
                    }else{
                        return '/'.$this->getName();
                    }
		}else{
                    return '/'.str_replace(DRIVE.$user['userId'].self::RECYCLE, '', $this->getPath());
		}
	}
            
	public function __toString() {
		return $this->getName();
	}

	public abstract function download();
	public abstract function rename($oldName, $newName );
	public abstract function delete();
	public abstract function restore();
	public abstract function remove($root);
	public abstract function getSize();
    public abstract function copy($destination);

	public static function createDriveItem($absPath,$user=null) {
		$driveItem;
		if (!file_exists($absPath))
			$driveItem = null; 
		if (is_dir($absPath))
			$driveItem = new Folder($absPath, $user);
		if (is_file($absPath))
			$driveItem = new File($absPath, $user);

		return $driveItem;
	}
        
        public function move($newPath) {
          if (!is_dir($newPath)) {
            return false;
          }
          
          rename($this->getPath(), $newPath.'/'.$this->getName());
          $moved = file_exists($newPath.'/'.$this->getName());
          if ($moved) {
            $this->setPath($newPath.'/'.$this->getName());
          }
          return $moved;
        }

	/*
	 
	public function move();

	public function copy();

	public function delete();

	public function isReadable();
	public function isWriteable();


	/**
	 *	getPath() - Gets the Item's absolute path.

	public function getPath() {
		if ($this->getName() != null)
			return Drive::getRoot().$this->getName();
		return null;
	}
	 */
}
