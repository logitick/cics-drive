<?php
class File {
	public static function getExtension($name) {
		$extStartIndex = strrpos($name, '.'); // the index of the start of the file extension
		
		if (!$extStartIndex)
			return null;

		return substr($name, $extStartIndex + 1, strlen($name));
	}
}