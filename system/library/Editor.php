<?php
require_once LIBRARY.'drive/File.php';
require_once LIBRARY.'Page.php';
require_once LIBRARY.'GenericBreadCrumb.php';
class Editor {
	private $page;
	private $file;
	public function __construct(Page $page, File $file = null) {
		$this->page = $page;
		$pageData = array();
		$this->page->addAsset('editor/lib/codemirror.js');
		$this->page->addAsset('editor/mode/php/php.js');
		$this->page->addAsset('editor/mode/clike/clike.js');
		$this->page->addAsset('editor/mode/css/css.js');
		$this->page->addAsset('editor/mode/xml/xml.js');
		$this->page->addAsset('editor/mode/vb/vb.js');
		$this->page->addAsset('editor/mode/sql/sql.js');
		$this->page->addAsset('editor/mode/javascript/javascript.js');
		$this->page->addAsset('editor/mode/htmlmixed/htmlmixed.js');
		$this->page->addAsset('editor/theme/eclipse.css');
		$this->page->addAsset('codemirror.css');
		$this->page->addAsset('editor.js');

		$this->file = $file;

	}


	public function getEditorContents($uri) {
		if(Input::post('btnDownload') == "dldl"){
                        
			$this->file->download();
		}
		$uri = str_replace($this->file->getName(), '', $uri);
		$pageData['document'] = $this->file->getName();
		$pageData['mimeType'] = $this->file->getMimeType(true);
		$pageData['fileContents'] = $this->file->getContents();
        
		
		$pageData['directory'] = $editorUri = preg_replace('/editor/', 'drive', $uri, 1);
        $breadCrumbBase = '/'.uri(1).'/drive';
        $breadCrumb = new GenericBreadCrumb($breadCrumbBase, $this->file->getRelPath());
        $pageData['breadCrumb'] = $breadCrumb->getTrailHtml('My Drive');
		$this->page->setContent('editor.php', $pageData);
	}

	public function overwriteFile($contents, $user) {
                $file = new File($this->file->getPath(), $user);
                $file->overwrite($contents);
                
	}
}