<?php

class Paginator {
    
    private $baseUrl;
    private $totalRows;
    private $rowsPerPage;
    private $currentPage;
    private $numberOfLinks = 10;

    public function __construct($baseUrl, $totalRows, $rowsPerPage, $currentPage = 1) {
        $this->setBaseUrl($baseUrl);
        $this->setTotalRows($totalRows);
        $this->setRowsPerPage($rowsPerPage);
        $this->setCurrentPage($currentPage);
    }
    
    public function getNumberOfLinks() {
        return $this->numberOfLinks;
    }

    public function setNumberOfLinks($numberOfLinks) {
        $this->numberOfLinks = $numberOfLinks;
    }

        
    public function getBaseUrl() {
        return $this->baseUrl;
    }

    public function setBaseUrl($baseUrl) {
        $this->baseUrl = rtrim($baseUrl, "/").'/';
    }

    public function getTotalRows() {
        return $this->totalRows;
    }

    public function setTotalRows($totalRows) {
        $this->totalRows = $totalRows;
    }

    public function getRowsPerPage() {
        return $this->rowsPerPage;
    }

    public function setRowsPerPage($rowsPerPage) {
        $this->rowsPerPage = $rowsPerPage;
    }
    
    public function getPaginationHtml() {
        $pagination = '<div class="paginationDiv"><ul class="pagination"><li><a href="'.$this->getBaseUrl().'page/'.'1">First</a></li>';
        $distance = ceil($this->getNumberOfLinks() / 2);
        $startPage = $this->getCurrentPage() - $distance;
        $endPage = $this->getCurrentPage() + $distance;
        $numberOfPages = ceil($this->getTotalRows() / $this->getRowsPerPage());
        if ($startPage < 1) {
            $startPage = 1;
            $endPage = $startPage + $distance;
        }
        for ($i = $startPage; $i <= $endPage; $i++) {
            
            if ($i > $numberOfPages)
                break;
            
            if ($i == $this->getCurrentPage()) {
                $pagination .= '<li class="current"><a href="'.$this->getBaseUrl().'page/'.$i.'">'.$i.'</a></li>';
            } else {
                $pagination .= '<li><a href="'.$this->getBaseUrl().'page/'.$i.'">'.$i.'</a></li>';
            }
        }       
        $pagination .= '<li><a href="'.$this->getBaseUrl().'page/'.$numberOfPages.'">Last</a></li></ul></div>';
        
        return $pagination;
    }

    public function getCurrentPage() {
        return $this->currentPage;
    }

    public function setCurrentPage($currentPage) {
        $this->currentPage = $currentPage;
    }
    
    public function __toString() {
        return $this->getPaginationHtml();
    }
}