<?php
require_once LIBRARY.'Drive.php';
require_once LIBRARY.'drive/VirtualFolder.php';
class SharedFolderViewer extends Drive {
  public function getRoot() {
    return parent::getRoot();
  }

  public function run() {
    parent::run();
  }

  
  public function __construct(Folder $currentFolder) {
    $this->setCurrentFolder($currentFolder);
  }
  
  //override the following methods to ensure that drive is read-only
  public function createFolder($name) {
    return;
  }

  public function delete($driveItem, $driveSize) {
    return;
  }

  public function rename($new_name, $old_name) {
    return;
  }

  public function upload($file, $storageCapacity, $overwrite) {
    return;
  }
  // end overrides
}