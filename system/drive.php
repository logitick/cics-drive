<?php
/**
 * @package system
 */
define('DB_HOST', 'tunnel.pagodabox.com');
define('DB_USERNAME', 'lakendra');
define('DB_PASSWORD', 'gCqLBs3R');
define('DB_NAME', 'cics_drive');

error_reporting (E_ALL);


function errhandler ( $errno, $errstr = '',  $errfile = '', $errline = -1, $errcontext = array() ) {
 
	$page = new Page();
	$pageData = array(
					'message' => $errstr,
					'file' => $errfile,
					'line' => $errline,
					'context' => $errcontext
				);
	$page->bindData($pageData);

    switch ($errno) {
    	case E_NOTICE:
    		$page->setTitle("Notice");
    		break;
    	case E_PARSE:
    		$page->setTitle("Parse Error");
    		break;
    	case E_WARNING:
    		$page->setTitle("Warning");
    		break;
    	case E_ERROR:
        default:
    		$page->setTitle("An error has occured");
    		break;
    }
    $page->setContent('error/error.php');
    debug_print_r($errcontext);
    echo $page;
    die();

}


function exception_handler($e) {
  
        
	$page = new Page();
	$pageData = array(
					'message' => $e->getMessage(),
					'code' => $e->getCode(),
					'file' => $e->getFile(),
					'line' => $e->getLine(),
					'trace' => $e->getTrace(),
					'type' => get_class($e)
				);
	$page->bindData($pageData);
    $page->setTitle("An error has occured");
    $page->setContent('error/exception.php');
    die($page);
}


set_error_handler('errhandler'); 
set_exception_handler('exception_handler');


function debug_echo($a) {
    echo '<div style="background:#FFF; padding:1%; font-family:Consolas;"><pre><code>';
    echo ($a);
    echo '</code></pre></div>';
}

function debug_var_dump($a) {

    echo '<div style="background:#FFF; padding:1%; font-family:Consolas;"><pre><code>';
    var_dump($a);
    echo '</code></pre></div>';
}
function debug_print_r($a) {

    echo '<div style="background:#FFF; padding:1%; font-family:Consolas;"><pre><code>';
    print_r($a);
//    print_r( debug_backtrace());
    echo '</code></pre></div>';
}


function uri($index = -1) {
    if ($index >= 0) {
        $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
        return $segments[$index];
    }
    return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
}