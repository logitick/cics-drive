<?php
namespace model;

require_once MODEL.'iDatabaseEntity.php';
require_once MODEL.'SharedFileFilter.php';
require_once MODEL.'Submission.php';

class SharedFolder implements \DatabaseEntity {
  
  function __construct($resultSet = array()) {
    if (count($resultSet) > 0) {
      $this->setSharedFolderID($resultSet['sharedFolderID']);
      $this->setFacultyUserID($resultSet['facultyUserID']);
      $this->setFolderPath($resultSet['folderPath']);
      $this->setSharedDate(\DateTime::createFromFormat(\DatabaseEntity::SQL_DATETIME_FORMAT, $resultSet['sharedDate']));
      if ($resultSet['deadlineDate']!= null)
        $this->setDeadlineDate(\DateTime::createFromFormat(\DatabaseEntity::SQL_DATETIME_FORMAT, $resultSet['deadlineDate']));
      $this->setAccessMode($resultSet['accessMode']);
      $this->setFilterType($resultSet['filterType']);
      $this->setSubmissionPrefix($resultSet['submissionPrefix']);
      $this->setSubmissionSuffix($resultSet['submissionSuffix']);
      $this->setPublicationID($resultSet['publicationID']);
      $this->setPassword($resultSet['password']);
      $this->setStatus($resultSet['status']);
    }

  }

  const TABLE = 'sharedfolders';
  
  const ACCESS_MODE_WRITE = 1;
  const ACCESS_MODE_READ = 2;
  
  const FILTER_TYPE_NONE = 0;
  const FILTER_TYPE_REJECT = 1;
  const FILTER_TYPE_ACCEPT = 2;

  const STATUS_ACTIVE = 'A';
  const STATUS_INACTIVE = 'I';
  
  private $sharedFolderID;
  private $facultyUserID;
  private $folderPath;
  private $sharedDate;
  private $deadlineDate;
  private $accessMode;
  private $filterType;
  private $submissionPrefix;
  private $submissionSuffix;
  private $publicationID;
  private $password;
  private $status;
  private $fileFilters = array();
  
  public function setFileFilters($fileFilters) {
    $this->fileFilters = $fileFilters;
  }

  public function getAccessMode() {
    return $this->accessMode;
  }

  public function getFileFilters() {
    return $this->fileFilters;
  }

    public function setSharedFolderID($sharedFolderID) {
    $this->sharedFolderID = $sharedFolderID;
  }

  public function setFacultyUserID($facultyUserID) {
    $this->facultyUserID = $facultyUserID;
  }

  public function setFolderPath($folderPath) {
    $this->folderPath = $folderPath;
  }

  public function setSharedDate(\DateTime $sharedDate = null) {
    $this->sharedDate = $sharedDate;
  }

  public function setDeadlineDate(\DateTime $deadlineDate = null) {
    $this->deadlineDate = $deadlineDate;
  }

  public function setAccessMode($accessMode) {
    $this->accessMode = $accessMode;
  }

  public function setFilterType($filterType) {
    $this->filterType = $filterType;
  }

  public function setSubmissionPrefix($submissionPrefix) {
    $this->submissionPrefix = $submissionPrefix;
  }

  public function setSubmissionSuffix($submissionSuffix) {
    $this->submissionSuffix = $submissionSuffix;
  }

  public function setPublicationID($publicationID) {
    $this->publicationID = $publicationID;
  }

  public function setPassword($password) {
    $this->password = $password;
  }

  public function setStatus($status) {
    $this->status = $status;
  }

  public function getSharedFolderID() {
    return $this->sharedFolderID;
  }

  public function getFacultyUserID() {
    return $this->facultyUserID;
  }

  public function getFolderPath() {
    return $this->folderPath;
  }

  public function getSharedDate() {
    return $this->sharedDate;
  }

  public function getDeadlineDate() {
    return $this->deadlineDate;
  }

  public function getFilterType() {
    return $this->filterType;
  }

  public function getSubmissionPrefix() {
    return $this->submissionPrefix;
  }

  public function getSubmissionSuffix() {
    return $this->submissionSuffix;
  }

  public function getPublicationID() {
    return $this->publicationID;
  }

  public function getPassword() {
    return $this->password;
  }

  public function getStatus() {
    return $this->status;
  }
  
  public static function getById($id) {
    $db = \Database::getInstance();
    
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE sharedFolderID = :id';
    $db->query($sql);
    $db->bind(':id', $id);
    $result = $db->single();
    if ($db->rowCount() == 0) {
        return null;
    }
    return new SharedFolder($result);
  }
  
  private static function toSqlTimestamp(\DateTime $date = null) {
    if ($date == null) {
      return null;
    }
    return $date->format(\DatabaseEntity::SQL_DATETIME_FORMAT);
  }
  
  public function updateRecord() {
    $db = \Database::getInstance();
    $db->beginTransaction();
    $sql = 'UPDATE '.self::TABLE.' SET facultyUserID = :userID,
                                        folderPath = :path, 
                                        deadlineDate = :deadline,
                                        accessMode = :accessMode, 
                                        filterType = :filterType, 
                                        submissionPrefix = :prefix, 
                                        submissionSuffix = :suffix, 
                                        password = :password,
                                        status = :status
          WHERE sharedFolderID = :folderID';
    $db->query($sql);
    $db->bind(':userID', $this->getFacultyUserID());
    $db->bind(':path', $this->getFolderPath());
    $db->bind(':deadline', self::toSqlTimestamp($this->getDeadlineDate()));
    $db->bind(':accessMode', $this->getAccessMode());
    $db->bind(':filterType', $this->getFilterType());
    $db->bind(':prefix', $this->getSubmissionPrefix());
    $db->bind(':suffix', $this->getSubmissionSuffix());
    $db->bind(':password', $this->getPassword());
    $db->bind(':folderID', $this->getSharedFolderID());
    $db->bind(':status', $this->getStatus());
    $db->execute();
    
    if (!(int)$db->getErrorCode() == \Database::SUCCESS_CODE) {
      $db->cancelTransaction();
      return 1;
    }
    
    //delete current file filters
    $sql = 'DELETE FROM '.\SharedFileFilter::TABLE.' WHERE sharedFolderID = :id';
    $db->query($sql);
    $db->bind(':id', $this->getSharedFolderID());
    $db->execute();
    
    
    $filters = $this->getFileFilters();
    foreach($filters as $filter) {
      $filter->setSharedFolderID($this->getSharedFolderID());
      if (!$filter->saveRecord()) {
        $db->cancelTransaction();
        $id = 0;
        return 2;
      }
    }
    $db->endTransaction();
    return 0;
  }
  
  public function saveRecord() {
    $id = 0;
    
    $db = \Database::getInstance();
    $db->beginTransaction();
    $sql = 'INSERT INTO '.self::TABLE.' (facultyUserID, folderPath, deadlineDate,
      accessMode, filterType, submissionPrefix, submissionSuffix, password) 
      VALUES (:userID, :path, :deadline, :accessMode, :filterType, 
      :prefix, :suffix, :password)';
    $db->query($sql);
    $db->bind(':userID', $this->getFacultyUserID());
    $db->bind(':path', $this->getFolderPath());
    $db->bind(':deadline', self::toSqlTimestamp($this->getDeadlineDate()));
    $db->bind(':accessMode', $this->getAccessMode());
    $db->bind(':filterType', $this->getFilterType());
    $db->bind(':prefix', $this->getSubmissionPrefix());
    $db->bind(':suffix', $this->getSubmissionSuffix());
    $db->bind(':password', $this->getPassword());
    $db->execute();
    
    if (!(int)$db->getErrorCode() === \Database::SUCCESS_CODE) {
      $db->cancelTransaction();
    }
    
    $id = $db->lastInsertId();
    $filters = $this->getFileFilters();
    foreach($filters as $filter) {
      $filter->setSharedFolderID($id);
      if (!$filter->saveRecord()) {
        $db->cancelTransaction();
        $id = 0;
      }
    }
    
    $db->endTransaction();
    
    return $id;
  }
  
  public static function getUserSharedFolders($userID) {
    $sharedFolders = array();
    $sql = 'SELECT * FROM sharedfolders WHERE facultyUserID = :user AND status = :status';
    $db = \Database::getInstance();
    $db->query($sql);
    $db->bind(':user', $userID);
    $db->bind(':status', self::STATUS_ACTIVE);
    $results = $db->resultset();
    foreach ($results as $result) {
      $sharedFolders[] = new SharedFolder($result);
    }
    return $sharedFolders;
  }

  public static function readOnlyFolderFactory($userID, $path, $password = null) {
    $folder = new SharedFolder();
    $folder->setFacultyUserID($userID);
    $folder->setFolderPath($path);
    $folder->setPassword($password);
    $folder->setAccessMode(self::ACCESS_MODE_READ);
    $folder->setFilterType(self::FILTER_TYPE_NONE);
    return $folder;
  }
  
  public function getSubmissions() {
    $sql = 'SELECT * FROM '.  \Submission::TABLE. ' WHERE sharedFolderID = :id order by submissionDate ASC';
    $db = \Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $this->getSharedFolderID());
    $results = $db->resultset();
    $submissions = array();
    foreach ($results as $result) {
      $submissions[] = new \Submission($result);
    }
    return $submissions;
  }
  
  public static function writableFolderFactory($userID, $path, \DateTime $deadline, $password, $prefix, $suffix, $filterType, $filters) {
    $folder = new SharedFolder();
    $folder->setFacultyUserID($userID);
    $folder->setFolderPath($path);
    $folder->setAccessMode(self::ACCESS_MODE_WRITE);
    $folder->setDeadlineDate($deadline);
    $folder->setPassword($password);
    $folder->setSubmissionPrefix($prefix);
    $folder->setSubmissionSuffix($suffix);
    $folder->setFilterType($filterType);
    $filterObj = array();
    foreach ($filters as $filter) {
      $filterObj[] = new \SharedFileFilter($filter);
    }
    $folder->setFileFilters($filterObj);
    return $folder;
  }
}