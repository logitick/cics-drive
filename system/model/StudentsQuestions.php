<?php
require_once LIBRARY.'Database.php';
require_once LIBRARY.'Session.php';
require_once MODEL.'iDatabaseEntity.php';
require_once MODEL.'StudentInfo.php';

class StudentsQuestions implements DatabaseEntity {

	const TABLE = 'studentquestions';

	private $studentQuestionID;
	private $question;
	private $status;
	private static $promptQuestions = array();

	private function __construct($id, $question, $status ) {
		$this->setStudentQuestionID($id);
		$this->setQuestion($question);
		$this->setStatus($status);
	}
	
	public function setStudentQuestionID($studentQuestionID){
		$this->studentQuestionID = $studentQuestionID;
	}
	
	public function setQuestion($question){
		$this->question = $question;
	}
	
	public function setStatus($status){
		$this->status = $status;
	}
	
	public function getStudentQuestionID(){
		return $this->studentQuestionID;
	}

	public function getQuestion(){
		return $this->question;
	}
	
	public function getStatus(){
		return $this->status;
	}
	
	/**
	*
	*	readyQuestions
	*/
	public static function readyQuestions(){

		$db = Database::getInstance();
		$db->query("SELECT * FROM ".self::TABLE." WHERE status = 'A' ORDER BY RAND() LIMIT 0,5");
		$result = $db->resultset();
                
		if (count($result) > 0 ) {			
			shuffle($result);
                        $qs = Session::get('registration');
                        
                        if( Session::get('registration')!=null){
                            if( isset($qs['question'])===false ) {
                                foreach($result as $key => $question){
                                    $_SESSION['registration']['question'][] = $question;
                                }
                            }else{
                                return;
                            }
                        }
			return $result;
		}
		return null;
	}

	// $exludeArr is an array of studentQuestionIDs to exclude 
	public static function getRandom($excludeArr = array()) { 
		$db = Database::getInstance();
		$exclude = 0; // use zero since there is no zero value primary key. so ang gawas sa query NOT IN (0)
		if (count($excludeArr) > 0) // pero ug naay values ang array kay iyahang ilisan ang 0
			$exclude = implode(',', array_fill(0, count($excludeArr), '?'));

		$db->query("SELECT * FROM ".self::TABLE." WHERE status = 'A' AND studentQuestionID NOT IN (".$exclude.") ORDER BY RAND() LIMIT 0,1");
		foreach ($excludeArr as $k => $id)
		    $db->bind(($k+1), $id);
		$question = $db->single();
		if ($db->rowCount() == 0)
			return null;
		die();
		return new StudentsQuestions($question['studentQuestionID'], $question['question'], $question['status']);
	}
	
	/**
	* changeQuestion()
	*
	*/
	public static function changeQuestion($index){
                $indexs = array();
                $v = Session::get('registration');
                foreach($v['question'] as $key => $val){
                    $indexs[] = $val['studentQuestionID'];
                }
                
		$db = Database::getInstance();
                $sql = "SELECT * FROM ".self::TABLE." WHERE status = 'A' AND studentQuestionID NOT IN (:q1,:q2,:q3,:q4,:q5) ORDER BY RAND() LIMIT 0,1";
		$db->query($sql);
		$db->bind(':q1', $indexs[0]);
		$db->bind(':q2', $indexs[1]);
		$db->bind(':q3', $indexs[2]);
		$db->bind(':q4', $indexs[3]);
		$db->bind(':q5', $indexs[4]);
                $result = $db->single();
		if (count($result) > 0 && !empty($result)) {			
                    $_SESSION['registration']['question'][(int)$index] = $result;
                    return $result;
                }
	}
	
	public static function getById($id, $includeInactive = false) {
                $db = Database::getInstance();
                if ($includeInactive) {
                  $db->query("SELECT * FROM ".self::TABLE." WHERE studentQuestionID = :id");
                } else {
                  $db->query("SELECT * FROM ".self::TABLE." WHERE studentQuestionID = :id status = 'A'");
                }
		
		$db->bind(':id', $id);
		$result = $db->single();
		if (count($result) > 0)
			return new StudentsQuestions($result['studentQuestionID'], $result['question'], $result['status']);
		return null;
	}
	public function updateRecord(){
		return null;
	}
	/**
	* securityQuestion
	* parameter : string
	* view all security question
	*/
	public static function securityQuestion(){
	$db = Database::getInstance();
	$db->query("SELECT studentQuestionID,question,status FROM studentquestions ORDER BY 1 DESC");
	$result = $db->resultSet();
	return $result;
	}
	/**
	* countuser
	* parameter : int
	* count no of user of the security question
	*/
	public static function countUser($studentQuestionID){
	$db = Database::getInstance();
	$db->query("SELECT COUNT(*) FROM studentinfo WHERE studentQuestionID = :studentQuestionID");
	$db->bind(':studentQuestionID', $studentQuestionID);
	$result = $db->single();
	return $result;
	}
	
	/**
	* addSecurityQuestion
	* parameter : string
	* add security question
	*/
	public static function addSecurityQuestion($question){
			$db = Database::getInstance();
			$sql= "INSERT INTO studentquestions (studentQuestionID ,question ,status) VALUES (NULL , :question, 'A');";
			$db->query($sql);
			$db->bind(':question', $question);
			$db->execute();
                        if ((int)$db->getErrorCode() == Database::SUCCESS_CODE) {
                          return $db->lastInsertId();
                        }
			return false;
			
	}
	/**
	* existingSecurityQuestion
	* parameter : string
	* check if security question already exist return true if exist false if not
	*/
	public static function existingSecurityQuestion($question){
		$db = Database::getInstance();
		$db->query("SELECT * FROM studentquestions WHERE question = :question" );
		$db->bind(':question', $question);
		$result = $db->single();
		if (count($result) > 0) {
			return $result;
		}
		return false;
	}

	/**
	* validateSecurityQuestion
	* parameter : string
	* check if security question already Active or Inactive return value true if Active false if Inactive
	*/
	public static function validateSecurityQuestion($question){
		$db = Database::getInstance();
		$db->query("SELECT status,question FROM studentquestions WHERE question = :question" );
		$db->bind(':question', $question);
		$result = $db->single();
		if (count($result) > 0) {
			return $result;
		}
		return false;
	}
	/**
	* updatequestionstatus
	* parameter : string, string
	* update status of the question when button status is A it will be I and if I it will be A.
	*/
	public static function updateQuestionStatus($status,$question){
		$db = Database::getInstance();
		$sql= "UPDATE studentquestions SET status =:status WHERE question =:question";
		$db->query($sql);
		$db->bind(':status', $status);
		$db->bind(':question', $question);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
        
        public static function getNewQuestion($id){
            $db=Database::getInstance();
            
            $sql  = "SELECT * FROM ".self::TABLE." WHERE studentQuestionID NOT IN(SELECT studentQuestionID FROM ".StudentInfo::TABLE." i
                    JOIN ".User::TABLE." u
                    ON i.userID = u.userID
                    WHERE i.status = 'A' AND u.userID = :id) and status = 'A'";
			
            $db->query($sql);
            $db->bind(':id', $id);
            $result = $db->single();
           
            if(count($result)>0){
                return $result;
            }
            return null;
        }
}
