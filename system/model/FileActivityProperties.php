<?php
require_once MODEL.'iDatabaseEntity.php';

class FileActivityProperties implements DatabaseEntity {
	
	/*
	*	Constant variable
	*	Used in - Upload/Edit/ Delete/ Create Folder/ Share Folder/ Publishing/ Archive
	*/	 
	const INDICATE_PATH = 01;
	
	/*
	*	Constant variable
	*	Used in - Rename/ Remove/ Restore
	*/
	const INDICATE_OLD_PATH = 02;
	const INDICATE_NEW_PATH = 03;
	const INDICATE_ADMIN = 06;
	/*
	*	Constant variable
	*	Used in Submit
	*/
	const INDICATE_ORIG_PATH = 04;
	const INDICATE_SHARED_FOLDER_ID = 05;
	
	/**
	*	Table name
	*/
	const TABLE = 'fileactivityproperties';
	
	/**
	*	@viriable
	*/
	private $activityID;
	private $id;
	private $value;
	
	/**
	*	Constructor
	*	@Parameter array
	*/
	public function __construct( $fileactivityproperteis=null ){
		if($fileactivityproperteis!=null){
			$this->setActivityID($fileactivityproperteis['activityID']);
			$this->setID($fileactivityproperteis['id']);
			$this->setValue($fileactivityproperteis['value']);
		}
	}
	
	/**
	*	@Method setActivityID
	*	@Parameter int
	*/
	public function setActivityID( $activityID ){
		$this->activityID = $activityID;
	}
	
	/**
	*	@Method setID
	*	@Parameter int
	*/
	public function setID( $id ){
		$this->id = $id;
	}
	
	/**
	*	@Method setValue
	*	@Parameter string
	*/
	public function setValue( $value ){
		$this->value = $value;
	}
        
        public static function getProperties($activityID) {
          $sql = 'SELECT * FROM '.self::TABLE. ' WHERE activityID = :actID';
          $db = Database::getInstance();
          $db->query($sql);
          $db->bind(':actID', $activityID);
          return $db->resultset();
          
        }
	
	/**
	*	@Method getActivityID
	*	@return int
	*/
	public function getActivityID(){
		return $this->activityID;
	}
	
	/**
	*	@Method getID
	*	@Parameter int
	*/
	public function getID(){
		return $this->id;
	}
	
	/**
	*	@Method getValue
	*	@Parameter string
	*/
	public function getValue(){
		return $this->value;
	}
	
	/**
	*	@Method logFileProperties -logging properties of this logged file activities
	*	@Parameter int,int,string
	*	@Return void
	*/
	public static function logFileProperties($actID,$indicator,$value){
		$db = Database::getInstance();
		$statement = 'INSERT INTO '.self::TABLE.' (activityID, id, value) VALUES (:actID, :indicator, :value)'; 
		$db->query($statement);
		$db->bind(':actID',$actID);
		$db->bind(':indicator',$indicator);
		$db->bind(':value',$value);
		$db->execute();
                return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
         *
         * @param type $name
         * @param type $id
         * @param type $userID
         * @return type 
         */
	public static function queryActivityIDAdmin( $name, $id ,$userID, $category){
		$db = Database::getInstance();
		$statement='SELECT '.FileActivityLogs::TABLE.'.activityID FROM '.self::TABLE.'
					INNER JOIN '.FileActivityLogs::TABLE.' ON '.FileActivityLogs::TABLE.'.activityID = '.self::TABLE.'.activityID
					WHERE '.self::TABLE.'.id= :id AND '.self::TABLE.'.value= :value AND '.FileActivityLogs::TABLE.'.userID = :userID
                                        AND '.FileActivityLogs::TABLE.'.category = :category     
					ORDER BY '.self::TABLE.'.activityID DESC ';
                
		$db->query($statement);
		$db->bind(':value',$name);
		$db->bind(':id',$id); 
		$db->bind(':userID',$userID); 
		$db->bind(':category',$category); 
		$result = $db->single();
		if(count($result)>0){
			return $result['activityID'];
		}
		return null;
	}
	
        /**
         *
         * @param type $name
         * @param type $id
         * @param type $userID
         * @return type 
         */
        public static function queryActivityID( $name, $id ,$userID){
		$db = Database::getInstance();
		$statement='SELECT '.FileActivityLogs::TABLE.'.activityID FROM '.self::TABLE.'
					INNER JOIN '.FileActivityLogs::TABLE.' ON '.FileActivityLogs::TABLE.'.activityID = '.self::TABLE.'.activityID
					WHERE '.self::TABLE.'.id= :id AND '.self::TABLE.'.value= :value AND '.FileActivityLogs::TABLE.'.userID = :userID
					ORDER BY '.self::TABLE.'.activityID DESC ';
		$db->query($statement);
		$db->bind(':value',$name);
		$db->bind(':id',$id); 
		$db->bind(':userID',$userID); 
		$result = $db->single();
		if(count($result)>0){
			return $result['activityID'];
		}
		return null;
	}
	
	/**
	*	getValue
	*/
	public static function getSpecificValue($actid,$id){
		require_once LIBRARY.'DBQuery.php';
                $db = Database::getInstance();
                
                $query = DBQuery::select('value')->from(self::TABLE)->where('activityID',$actid)->where('id',$id);
		
		$db->query($query->__toString());
		$result = $db->single();
                
                if(count($result)>0){
                    return  $result['value'];
                }
		return null;
	}
        
	//@Override
	public static function getByID( $activityID ) {
		$db = Database::getInstance();
		$db->query('SELECT * FROM '.self::TABLE.' WHERE activityID = :id');
		$db->bind(':id', $activityID);
		$result = $db->single();
		if ($db->rowCount() == 0)
			throw new UserException('No such Activity Log Properties Record', 1);
		$activitylogproperties = new FileActivityProperties($result);
		return $activitylogproperties;
	}
	
	//@Override
	public function updateRecord() {
		$db = Database::getInstance();
		$statement = "UPDATE ".self::TABLE.' SET id = :id,
												value = :value
												WHERE activityID = :activityID';
		$db->query($statement);
		$db->bind(':id',$this->getID());
		$db->bind(':value',$this->getValue());
		$db->bind(':activityID',$this->getActivityID());
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
}