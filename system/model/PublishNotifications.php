<?php

class PublishNotifications{
		private $notificationID;
		private $studentUserID;
		private $publicationID;
		private $status;
		
		public function setNotificationID($notificationID){
			$this->notificationID=$notificationID;
		}
		
		public function setStudentUserID($studentUserID){
			$this->studentUserID=$studentUserID;
		}
		
		public function setPublicationID($publicationID){
			$this->publicationID=$publicationID;
		}
		
		public function setStatus($status){
			$this->status=$status;
		}
		
		public function getNotificationID(){
			return $this->notificationID;
		}
		
		public function getStudentUserID(){
			return $this->studentUserID;
		}
		
		public function getPublicationID(){
			return $this->publicationID;
		}
		
		public function getStatus(){
			return $this->status;
		}

}