<?php

require_once LIBRARY.'Session.php';
require_once LIBRARY.'Database.php';
require_once LIBRARY.'DBQuery.php';

class Logins implements DatabaseEntity{
	
	const TABLE = 'logins';
	
	private $loginID;
	private $userID;
	private $loginDate;
	private $IpAddress;
	private $sessionID;
	private $userAgent;
	private $status;
        
	public function __construct($arrgs) {
            if($arrgs!=null){
                $this->setLoginID($arrgs['loginID']);
                $this->setUserID($arrgs['userID']);
                $this->setLoginDate($arrgs['loginDate']);
                $this->setIpAddress($arrgs['IpAddress']);
                $this->setSessionID($arrgs['sessionID']);
                $this->setUserAgent($arrgs['userAgent']);
                $this->setStatus($arrgs['status']);
            }
        }
        
        public static function getByDate($date=null){
            $db = Database::getInstance();
                    
            $query = DBQuery::select()->from(self::TABLE)->where('loginDate BETWEEN ? AND ?',$date);
            
            $db->query($query->__toString());
            
            return $db->resultset();
        }
        
	public function setLoginID($loginID) {
		$this->loginID = $loginID;
	}
	
	public function setUserID($userID){
	$this->userID = $userID;
	}
	
	public function setLoginDate($loginDate){
	$this->loginDate = $loginDate;	
	}
	
	public function setIpAddress($IpAddress){
	$this->IpAddress = $IpAddress;
	}
	
	public function setSessionID($sessionID){
	$this->sessionID = $sessionID;
	}
	
	public function setUserAgent($userAgent){
            $this->userAgent = $userAgent;
	}
	
	public function setStatus($status){
	$this->status = $status;
	}
	
	public function getLoginID() {
		return $this->loginID;
	}
	
	public function getUserID(){
	return $this->userID;
	}
	
	public function getLoginDate(){
	return $this->loginDate;
	}
	
	public function getIpAddress(){
	return $this->IpAddress;
	}
	
	public function getSessionID(){
	return $this->sessionID;
	}

        public function getUserAgent(){
	return $this->status;
	}

	public function getStatus(){
	return $this->status;																											
	}
	
	
	public static function setLoginInfo( $user, $ipAdd, $sessionId, $userAgent ){
		$db = Database::getInstance();																																									
		$statement = "INSERT INTO " .self::TABLE. " (loginID, userID, loginDate, IpAddress, sessionID, userAgent, status) VALUES ( DEFAULT, :user, DEFAULT, :ipAdd, :sesID, :userAgent, 'A')";
                debug_print_r($ipAdd);
                debug_print_r($sessionId);
                debug_print_r($userAgent);
                $db->query($statement);																							
		$db->bind(':user', $user );
		$db->bind( ':ipAdd', $ipAdd );
		$db->bind( ':sesID', $sessionId );
		$db->bind( ':userAgent', $userAgent );
		$db->execute();
	}
	
        public static function getById($id) {
            $db = Database::getInstance();
            
            $db->query("SELECT * FROM ".self::TABLE." WHERE loginID = :id");
            $db->bind(':id',$id);
            $result = $db->single();
            debug_print_r($result);
            if ($db->rowCount() == 0)
                  throw new UserException('No such login details', 1);
            $login = new Logins($result);
            return $login;
        }
        
        public function updateRecord() {
            $db = Database::getInstance();
                    
            $statement = "UPDATE FROM ".self::TABLE." SET userID = :userId,
                                                          loginDate = :loginDate,
                                                          IpAddress = :ipAddress,
                                                          sessionID = :sessionId,
                                                          userAgent =  :userAgent,
                                                          status = :status  
                                                          WHERE loginID = :loginId";
        
            $db->bind(":userId", $this->getUserID());
            $db->bind(":loginDate", $this->getLoginDate());
            $db->bind(":ipAddress", $this->getIpAddress());
            $db->bind(":sessionId", $this->getSessionID());
            $db->bind(":userAgent", $this->getUserAgent());
            $db->bind(":status", $this->getStatus());
            $db->execute();
            
            return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
        }
        
        public static function getNumberofLogins($id){
            $db = Database::getInstance();
            
            $statement = "SELECT COUNT(*) as logs FROM ".self::TABLE." WHERE userID = :id";
            
            $db->query($statement);
            $db->bind(':id', $id);
            $result = $db->single();
            if(count($result)>0){
                return $result['logs'];
            }
            return null;
        }
        
}

