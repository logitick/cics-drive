<?php

require_once LIBRARY.'Session.php';
require_once LIBRARY.'Database.php';
require_once MODEL.'StudentsQuestions.php';
require_once MODEL.'iDatabaseEntity.php';

class StudentInfo implements DatabaseEntity {
	
	const TABLE = 'studentinfo';
	
	private $studentInfoID;
	private $userID;
	private $answer;
	private $studentQuestionID;
	private $status;
	
	public function __construct($arrg = null) {
            if( $arrg!=null ){
                $this->setStudentInfoID($arrg['studentInfoID']);
                $this->setUserID($arrg['userID']);
                $this->setAnswer($arrg['answer']);
                $this->setStudentQuestionID($arrg['studentQuestionID']);
                $this->setStatus($arrg['status']);
            }
	}

	public function setStudentInfoID($studentInfoID){
	$this->studentInfoID = $studentInfoID;
	}
	
	public function setUserID($userID){
	$this->userID = $userID;
	}
	
	public function setAnswer($answer){
	$this->answer = $answer;
	}
	
	public function setStudentQuestionID($studentQuestionID){
	$this->studentQuestionID = $studentQuestionID;
	}
	
	public function setStatus($status){
	$this->status = $status;
	}
	
	public function getStudentInfoID(){
	return $this->studentInfoID;
	}

	public function getUserID(){
	return $this->userID;
	}
	
	public function getAnswer(){
	return $this->answer;
	}
	
	public function getStudentQuestionID(){
	return $this->studentQuestionID;
	}
	
	public function getStatus(){
	return $this->status;
	}
	
	public static function populateStudentInfo( $userID , $question ){

		$db = Database::getInstance();
		$statement = "INSERT INTO " .self::TABLE. " (`studentInfoID`, `userID`, `answer`, `studentQuestionID`, `status`)  VALUES
				( DEFAULT, ?, ?, ?, 'A'  ),
				( DEFAULT, ?, ?, ?, 'A'  ),
				( DEFAULT, ?, ?, ?, 'A'  ),
				( DEFAULT, ?, ?, ?, 'A'  ),
				( DEFAULT, ?, ?, ?, 'A'  );
		";
		$db->query($statement);
		$dataCount = 1;
		foreach( $question as $key => $value ){
			$idNumber = $userID;
			$questionID = $value['studentQuestionID'];
			$answers = $value['answer'];
			
			$db->bind($dataCount++, $idNumber);
			$db->bind($dataCount++, $answers);
			$db->bind($dataCount++, $questionID);
		}

		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/*
	* to get the student security question and its answer 
	* 
	**/
	
	public static function resetAnswer($userID){
		$db = Database::getInstance();
		$db->query("SELECT * FROM ".self::TABLE." s inner join ".StudentsQuestions::TABLE." q on s.studentQuestionID = q.studentQuestionID WHERE userID = :userID");
		$db->bind(':userID', $userID);
		$result = $db->resultset();
		if (count($result) > 0) {			
			return $result;	
		}
		return false;
	}
	/*
	*
	*
	**/
	
	public static function updateAnswer($userID,$answer,$sid){
		$db = Database::getInstance();
		$sql = "UPDATE studentinfo SET answer= :answers WHERE userID = :ID AND studentQuestionID = :sid";
		//UPDATE studentinfo SET answer = 'cob' WHERE userID = '21629' AND studentQuestionID = '21 '
		$db->query($sql);
		$db->bind(':answers', $answer);
		$db->bind(':ID', $userID);
		$db->bind(':sid',$sid);
		$result = $db->single();
		/* return ((int)$db->getErrorCode() == Database::SUCCESS_CODE); */
		if(count($result)>0)
		{
			return 1;
		}
		return 0;
	}

	public static function getQuestionAnswerSet($studentID) {
		$sql = "
				SELECT q.studentQuestionID, q.question, i.studentInfoID, i.answer FROM studentQuestions q
				JOIN studentInfo i ON
				 q.studentQuestionID = i.studentQuestionID
				WHERE i.userID = :uid AND 
				q.status = 'A' AND i.status = 'A' ORDER BY RAND();
		";
		$db = Database::getInstance();
		$sqlSuffix = ''; // sql suffix to fetch active records only
                
		$db->query($sql);
		$db->bind(':uid', $studentID);
		$set = $db->resultSet();
		if ($db->rowCount() == 0)
			return null;
		return $set;
	}

	public static function getSpecifiedAnswer($questionId, $userId) {
		$db = Database::getInstance();
		$db->query("SELECT * FROM ".self::TABLE." WHERE studentQuestionID = :qid AND userID = :uid AND status = 'A'");
		$db->bind(':qid', $questionId);
		$db->bind(':uid', $userId);
		$result = $db->single();
		if(count($result)>0)
                    return new StudentInfo($result);

		return null;
	}

	public static function getById($id) {
		$db = Database::getInstance();
		$db->query("SELECT * FROM ".self::TABLE." WHERE studentInfoID = :sid");
		$db->bind(':sid', $id);
		$result = $db->single();
		if(count($result)>0)
                    return new StudentInfo($result);
		
		return null;	
	}

	public function updateRecord() {
		$db = Database::getInstance();
		$sql = "UPDATE ".self::TABLe." SET answer= :answers, status = :status WHERE studentInfoID = :id";
		$db->query($sql);
		$db->bind(':answers', $this->getAnswer());
		$db->bind(':id', $this->getUserID());
		$db->bind(':status',$this->getStatus());
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
        
        public static function addNewSecurity($id,$sid,$answer){
            $db = Database::getInstance();
            
            $sql ="INSERT INTO ".self::TABLE." VALUES(DEFAULT,:id,:answer,:sid,'A')";
            $db->query($sql);
            $db->bind(':id', $id);
            $db->bind(':sid', $sid);
            $db->bind(':answer', $answer);
            $db->execute();
            return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
        }
        
        public static function countQuestionOfUser($id){
            $db=Database::getInstance();
            $sql = "SELECT COUNT(*) as countQuestions FROM ".self::TABLE." WHERE userID = :id AND status = 'A'";
          
            $db->query($sql);
            $db->bind(':id', $id);
            $result = $db->single();
            if(count($result)>0){
                return $result['countQuestions'];
            }
            return null;
        }
       
}
