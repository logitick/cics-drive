<?php
/**
 * @package model
 */
require_once LIBRARY.'Session.php';
require_once MODEL.'iDatabaseEntity.php';
class User implements DatabaseEntity {
	
	const TABLE = 'users';
        
    const STATUS_ACTIVE = 'A';
    const STATUS_INACTIVE = 'I';
    const STATUS_SUSPENDED = 'S';
    const STATUS_SUSPICIOUS = 'K';
	
	private $userID; 
	private $email;
	private $password;
	private $idNumber;
	private $firstName;
	private $middleInitial;
	private $lastName;
	private $usedStorage;
	private $userType;
	private $avatarFileName;
	private $driveView;
	private $status;
    private $userTypeInstance;
	
	/**
     * Optional Parameter: an associative array from the database
	*/
	public function __construct($arr = null) {
		if ($arr != null) {
			$this->setUserID($arr['userID']);
			$this->setEmail($arr['email']);
			$this->password = $arr['password'];
			$this->setIdNumber($arr['idNumber']);
			$this->setFirstName($arr['firstName']);
			$this->setMiddleInitial($arr['middleInitial']);
			$this->setLastName($arr['lastName']);
			$this->setUsedStorage($arr['usedStorage']);
			$this->setUserType($arr['userType']);
			$this->setAvatarFileName($arr['avatarFileName']);
			$this->setDriveView($arr['driveView']);
			$this->setStatus($arr['status']);
		}
	}
	public function setUserID($userID){
		$this->userID = $userID;
	}
	
	public function setEmail($email){
		$this->email = $email;
	}
	
	public function setPassword($password){
		$this->password = md5($password);
	}
	
	public function setIdNumber($idNumber){
		$this->idNumber = $idNumber;
	}
	
	public function setFirstName($firstName){
		$this->firstName = $firstName;
	}
	
	public function setMiddleInitial($middleInitial){
		$this->middleInitial = $middleInitial;
	}
	
	public function setLastName($lastName){
		$this->lastName = $lastName;
	}
	
	public function setUsedStorage($usedStorage){
		$this->usedStorage = $usedStorage;
	}
	
	public function setUserType($userType){
		$this->userType = $userType;
	}
	
	public function setAvatarFileName($avatarFileName){
		$this->avatarFileName = $avatarFileName;
	}
	
	public function setDriveView($driveView){
		$this->driveView = (int)$driveView;
	}
	
	public function setStatus($status){
          switch ($status ){
              case User::STATUS_ACTIVE:
              case User::STATUS_INACTIVE:
              case User::STATUS_SUSPENDED:
              case User::STATUS_SUSPICIOUS:
              $this->status = $status;
          }
		
	}
        
    public function getUserTypeInstance() {
      if ($this->getUserType() == null)
        return null;
      if ($this->userTypeInstance == null)
			$this->userTypeInstance = UserType::getById ($this->getUserType ());
      return $this->userTypeInstance;
    }
    
    public static function getByEmail($email) {
      $sql = 'SELECT * FROM '.self::TABLE.' WHERE email=:email';
      $db= Database::getInstance();
      $db->query($sql);
      $db->bind(':email', $email);
      $result = $db->single();
      if (count($result) > 0) {
        return new User($result);
      }
      return null;
    }
	
	public function getUserID(){
		return $this->userID;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function getPassword(){
		return $this->password;
	}
	
	public function getIdNumber(){
		return $this->idNumber;
	}
	
	public function getFirstName(){
		return $this->firstName;
	}
	
	public function getMiddleInitial(){
		return $this->middleInitial;
	}
	
	public function getLastName(){
		return $this->lastName;
	}
	
	public function getUsedStorage(){
		return $this->usedStorage;
	}
        
    public function getUsedStoragePercent() {
      $userType = $this->getUserTypeInstance();
      $remaining = round($this->getUsedStorage() / $userType->getStorageCapacity() * 100,2);
      return $remaining;
    }
	
	public function getUserType(){
		return $this->userType;
	}
	
	public function getAvatarFileName() {
		return $this->avatarFileName;
	}
	public function getAvatarUrl(){
		return $this->avatarFileName == null ? IMAGES.'avatar.png':IMAGES.'avatars/'.$this->avatarFileName;
	}
	
	public function getDriveView(){
		return $this->driveView;
	}
	
	public function getStatus(){
		return $this->status;
	}
        
        public function getStatusString() {
          $status = '';
            switch ($this->getStatus()) {
                case User::STATUS_ACTIVE:
                    $status = "Active";
                    break;
                case User::STATUS_INACTIVE:
                    $status = "Inactive";
                    break;
                case User::STATUS_SUSPENDED:
                    $status = 'Suspended';
                    break;
                case User::STATUS_SUSPICIOUS:
                    $status = 'Suspicious';
                    break;
            }
            return $status;
        }
        
    public function getStatusButton() {
        $class = 'btnStatus button small';
        $status = '';

        switch ($this->getStatus()) {
            case User::STATUS_ACTIVE:
                $status = "Active";
                $class .= ' green';
                break;
            case User::STATUS_INACTIVE:
                $status = "Inactive";
                break;
            case User::STATUS_SUSPENDED:
                $status = 'Suspended';
                $class .= ' red';
                break;
            case User::STATUS_SUSPICIOUS:
                $class .= ' orange';
                $status = 'Suspicious';
                break;
        }
        $buttonStr = '<a href="#setStatusModal" rel="modal:open" title="'.$status.'" class="'.$class.'" data-user-status="'.$this->getStatus().'" data-user-id="'.$this->getUserID().'">';
        $icon = '<span class="icon">&#9873;</span>';
        $buttonStr .= $icon . '</a>';
        return $buttonStr;
    }
        
	//@Override
	public static function getByID($userId) {
                
		$db = Database::getInstance();
		$db->query('SELECT * FROM '.self::TABLE.' WHERE userID = :id');
		$db->bind(':id', $userId);
		$result = $db->single();
		if ($db->rowCount() == 0)
			throw new UserException('No such user', 1);
		$user = new User($result);
		return $user;
	}
	
	//@Override
	public function updateRecord() {
		$db = Database::getInstance();
		$sql = "UPDATE users SET email = :email,
								 password = :password,
								 idNumber = :idNumber, 
								 firstName = :firstName, 
								 middleInitial = :middleInitial, 
								 lastname = :lastName,
								 usedStorage = :usedStorage, 
								 avatarFileName = :avatarFileName, 
								 driveVIew = :driveView,
								 status = :status
				WHERE userId = :id";
		$db->query($sql);
		$db->bind(':email', $this->getEmail());
		$db->bind(':password', $this->getPassword());
		$db->bind(':firstName', $this->getFirstName());
		$db->bind(':middleInitial', $this->getMiddleInitial());
		$db->bind(':lastName', $this->getLastName());
		$db->bind(':usedStorage', $this->getUsedStorage());
		$db->bind(':avatarFileName', $this->getAvatarFileName());
		$db->bind(':driveView', $this->getDriveView());
		$db->bind(':status', $this->getStatus());
		$db->bind(':idNumber', $this->getIdNumber());
		$db->bind(':id', $this->getUserID());
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	* create account for admin / faculty using admin account
	*
	*/
	public function createRecord() {
		$db = Database::getInstance();
		$sql = "INSERT INTO users (userID ,email ,password ,idNumber ,firstName ,middleInitial ,lastName ,usedStorage ,userType ,avatarFileName ,driveView ,status) VALUES (NULL , :email, :password , NULL , ' ', NULL , ' ', '0', :usertype, NULL , '0', 'A')";
		//$sql = "INSERT INTO users(userID, email, password, idNumber, firstName, middleInitial, lastName, usedStorage, userType, avatarFileName, status) VALUES (DEFAULT,:email,:password,'','','','',0,:userType,null,'A')";
		$db->query($sql);
		$db->bind(':email', $this->getEmail());
		$db->bind(':password', $this->getPassword());
		$db->bind(':usertype', $this->getUserType());
		$db->execute();
                if ((int)$db->getErrorCode() == Database::SUCCESS_CODE) {
                  return $db->lastInsertId();
                }
		return false; 
	}
	
	/**
	* create student account using admin account
	*
	*/
	public function createStudentRecord() {
		$db = Database::getInstance();
		$sql = "INSERT INTO users (userID ,email ,password ,idNumber ,firstName ,middleInitial ,lastName ,usedStorage ,userType ,avatarFileName ,driveView ,status) VALUES (NULL ,NULL ,NULL ,:idNumber ,:firstName, NULL ,:lastName , '0', :userType, NULL , '0', 'I')";
		//$sql = "INSERT INTO users(userID, email, password, idNumber, firstName, middleInitial, lastName, usedStorage, userType, avatarFileName, status) VALUES (DEFAULT,:email,:password,'','','','',0,:userType,null,'A')";
		$db->query($sql);
		$db->bind(':idNumber', $this->getIdNumber());
		$db->bind(':firstName', $this->getFirstName());
		$db->bind(':lastName', $this->getLastName());
		$db->bind(':userType', $this->getUserType());
		$db->execute();
                if ((int) $db->getErrorCode() == Database::SUCCESS_CODE) {
                  return $db->lastInsertId();
                }
		return false; 
	}
	
	/**
	*	show all the list of user
	*
	*/
	public static function userList() {
		$db = Database::getInstance();
		$sql="SELECT email, idNumber, avatarFileName,firstName, lastName FROM ".self::TABLE." WHERE status = 'A' ORDER BY userType";
		$db->query($sql);
		$result = $db->resultSet();
		if($result != null){
			return $result;
		}	
		return false;
	}
	
	/**
	*	get all users 
	*/
	public function getAllUsers(){
		$db = Database::getInstance();
		$sql="SELECT idNumber, email, firstName, middleInitial, lastName, usedStorage, avatarFileName, userType, status FROM ".self::TABLE." ORDER BY 8";
		$db->query($sql);
		$result = $db->resultSet();
		if($result != null){
			return $result;
		}	
		return false;
	}
	
	
	public static function login($email, $password) {
		$db = Database::getInstance();
		$db->query("SELECT userId, firstName, userType, status FROM " .self::TABLE. " WHERE email = :email AND password = :password and status NOT IN ('".User::STATUS_INACTIVE."', '".User::STATUS_SUSPENDED."')");
		$db->bind(':email', $email);
		$db->bind(':password', md5($password));
		$result = $db->single();
		if ($result['userType'] == 3) { // add a flag for student logins for validation
			$result['validate'] = true;
		}
		if ( count($result) > 0 && !empty($result) ) {
			Session::set('user', $result);
			return true;
		}
		false;
	}

	public static function removeValidationFlag() { // removes the user validation flag
		$user = Session::getUser();
		unset($user['validate']);
		Session::set('user', $user);
	}
	
	/**
	*	validateUser
	*	Parameter : string
	*	This is used to validate a student wanted to be recorded into the database.
	*	If query is successful, the values will be set into the current Session.
	*/
	public static function validateUser($idno) {
		$db = Database::getInstance();
		$db->query("SELECT * FROM users WHERE idNumber = :idno");
		$db->bind(':idno', $idno);
		$result = $db->single();
		
		if (count($result) > 0 && !empty($result)) {
			return true;
		}
		return false;
	}

	/**
	*	getValidation
	*	Parameter : string,string
	*	This is used to validate a student wanted to register.
	*	If query is successful, the values will be set into the current Session.
	*/
	public static function getValidation($idno, $lastname) {
		$db = Database::getInstance();
		$db->query("SELECT 	userID, idNumber, firstName, lastName, status FROM " .self::TABLE. " WHERE idNumber = :idno AND LOWER(lastName) = :lastname");
		$db->bind(':idno', $idno);
		$db->bind(':lastname', $lastname);
                
		$result = $db->single();
		if (count($result) > 0 && !empty($result)) {
			Session::set('registration', $result);
		}
		return false;
	}
	
	/**
	*	getUserValidation
	*	Parameter : string,string
	*	This is used to validate a csv file.
	*	If query is successful, return true(user exist) false(user did not exist).
	*/
	public static function getUserValidation($idno) {
		$db = Database::getInstance();
		$db->query("SELECT * FROM " .self::TABLE. " WHERE idNumber = :idno");
		$db->bind(':idno', $idno);
		$result = $db->single();
		if (count($result) > 0) {
			return $result;
		}
		return false;
	}
	
	/**
	* importCSV
	* import user account using admin account
	*
	*/
	public static function importCSV($idNumber,$lastName,$firstName) {
		$db = Database::getInstance();
		$sql = "INSERT INTO users (userID ,email ,password ,idNumber ,firstName ,middleInitial ,lastName ,usedStorage ,userType ,avatarFileName ,driveView ,status) VALUES (NULL ,NULL ,NULL ,:idNumber ,:firstName, NULL ,:lastName , '0', '3', NULL , '0', 'I')";
		$db->query($sql);
		$db->bind(':idNumber', $idNumber);
		$db->bind(':firstName', $firstName);
		$db->bind(':lastName', $lastName);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE); 
	}
	
	/**
	*	existingEmail
	*	Parameter : string
	*	Checks the email if it is existing in the database.
	*	Returns the result set if it is not existing,otherwise false.
	*/
	public static function existingEmail($email){
		$db = Database::getInstance();
		$db->query("SELECT COUNT(*) as count FROM " .self::TABLE. " WHERE email = :email" );
		$db->bind(':email', $email);
		$result = $db->single();
		
		if (count($result) > 0) {
			return $result['count'];
		}
		return false;
	}
	
	/**
	*	register
	*	Parameter : array 
	*	This is used to process the registration of a student, sets the status from I-inactive into A-active.
	*	Returns true if registered successfully, otherwise false.
	*/
	public static function register($values){
		$db = Database::getInstance();
		$db->query("UPDATE " .self::TABLE. " SET email=:email, password=:password, status='A' WHERE idNumber = :idno AND lastName = :lastname");
		$db->bind(':email', $values['email']);
		$db->bind(':password', md5($values['password']));
		$db->bind(':idno', $values['idNumber']);
		$db->bind(':lastname', $values['lastName']);
		$result = $db->single();
		
		if (count($result) > 0) {
			return true;
		}
		false;
	}
	
	
	/**
	*	getRecoveryValidation
	*	Parameter : int,string
	*	This is used to validate a student wanted to recover forgot password.
	*	If query is successful, the values will be set into the current Session.
	*/
	public static function getRecoveryValidation($idno, $email) {
		$db = Database::getInstance();
		$db->query("SELECT * FROM " .self::TABLE. " s INNER JOIN studentinfo i ON s.userID = i.userID INNER JOIN studentquestions q ON i.studentQuestionID = q.studentQuestionID WHERE idNumber = :idno AND email = :email ORDER BY RAND()");
		$db->bind(':idno', $idno);
		$db->bind(':email', $email);
		$result = $db->single();
		//debug_print_r($result);
		if (count($result) > 0 && !empty($result)) {
			Session::set('recovery', $result);
		}
	}
	
	/**
	*	getRecoveryPassword
	*	Parameter : int
	*	This is used to validate a student wanted to recover forgot password.
	*	If query is successful the password has been recover, the values will be set into the current Session.
	*/
	public function resetPassword($password,$userID) {
		$db = Database::getInstance();
		$sql = "UPDATE users SET password = :password
				WHERE userId = :id";
		$db->query($sql);
		$db->bind(':password', md5($password));
		$db->bind(':id', $userID);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	*	validatePassword
	*	Parameter : string
	*	This is used to validate a user password if match.
	*	If query is successful the it return true other wise its false.
	*/
	public function validatePassword($password,$userID){
		$db = Database::getInstance();
		$sql = "SELECT * FROM users WHERE userID =: userID AND password =:password";
		$db->query($sql);
		$db->bind(':password', md5($password));
		$db->bind(':id', $userID);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	*	getUserPassword
	*	Parameter : string
	*	This is used to validate a user wanted to reset forgot password.
	*	If query is successful the password has been recover, the values will be set into the current Session.
	*/
	public function resetUserPassword($email,$password) {
		
		$db = Database::getInstance();
		$sql = "UPDATE users SET password = :password
				WHERE email = :email";
		$db->query($sql);
		$db->bind(':password', md5($password));
		$db->bind(':email', $email);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	//update password
	public function updateAnswer($userID,$answer,$sid){
		$db = Database::getInstance();
		$sql = "UPDATE studentinfo SET answer= :answers WHERE userID = :ID AND studentQuestionID = :sid";
		//UPDATE studentinfo SET answer = 'cob' WHERE userID = '21629' AND studentQuestionID = '21 '
		//UPDATE studentinfo SET answer = 'cob' WHERE userID = '21629' AND studentQuestionID = '21 '
		$db->query($sql);
		$db->bind(':answers', $answer);
		$db->bind(':ID', $userID);
		$db->bind(':sid',$sid);
		$result = $db->single();
		/* return ((int)$db->getErrorCode() == Database::SUCCESS_CODE); */
		if(count($result)>0)
		{
			return 1;
		}
		return 0;
	}
        
        /**
         * 
         * @param type $flags
         * @param type $count
         * @param type $offset
         * @return User[] 
         */
        public static function getUserList($flags, $count = 0, $offset = 0, $orderBy = 1) {
           
            require_once LIBRARY.'DBQuery.php';
            $query = DBQuery::select()->from(self::TABLE)->limit($count, $offset);
            
            switch ($orderBy) {

                case 2:
                    $orderBy = 'firstName';
                    break;
                case 3:
                    $orderby = 'email';
                    break;
                case 4:
                    $orderBy = 'usedStorage';
                    break;
                case 5:
                    $orderBy = 'userType';
                    break;
                case 6:
                    $orderBy = 'status';
                    break;
                case 1:
                default:
                    $orderBy = 'lastName';
                    break;
            }
            
            // check for user type flags
            $userTypes = array();
            if (UserFlags::STUDENT & $flags)
                 $userTypes[] = UserType::STUDENT;
            if (UserFlags::FACULTY & $flags)
                  $userTypes[] = UserType::FACULTY;
            if (UserFlags::ADMIN & $flags)
                 $userTypes[] = UserType::ADMIN;
            $query->where('usertype', $userTypes);
            
            //check for ascending flag
            if (UserFlags::ASC & $flags) 
                $query->orderBy($orderBy, DBQuery::ASC);
            else 
                $query->orderBy($orderBy, DBQuery::DESC);
            
            //check for the user status
             if (UserFlags::ACTIVE & $flags)
                 $query->where('status', User::STATUS_ACTIVE);
             if (UserFlags::INACTIVE & $flags)
                 $query->where('status', User::STATUS_INACTIVE);
             if (UserFlags::SUSPICIOUS & $flags)
                 $query->where('status', User::STATUS_SUSPICIOUS);
             if (UserFlags::SUSPENDED & $flags)
                 $query->where('status', User::STATUS_SUSPENDED);
             
            $db = Database::getInstance();
            $db->query($query->__toString());
            $result = $db->resultset();
            
            if (count($result) > 0) {
                $studentsArray = array();
                foreach ($result as $student)
                    $studentsArray[] = new User($student);
                return $studentsArray;
            }
            
            return array();
        }
        
        public static function getUserListCount($flags) {
            require_once LIBRARY.'DBQuery.php';
            $query = DBQuery::select('COUNT(*) as userCount')->from(self::TABLE);
            
            // check for user type flags
            $userTypes = array();
            if (UserFlags::STUDENT & $flags)
                 $userTypes[] = UserType::STUDENT;
            if (UserFlags::FACULTY & $flags)
                  $userTypes[] = UserType::FACULTY;
            if (UserFlags::ADMIN & $flags)
                 $userTypes[] = UserType::ADMIN;
            $query->where('usertype', $userTypes);
            
           
            //check for the user status
             if (UserFlags::ACTIVE & $flags)
                 $query->where('status', User::STATUS_ACTIVE);
             if (UserFlags::INACTIVE & $flags)
                 $query->where('status', User::STATUS_INACTIVE);
             if (UserFlags::SUSPICIOUS & $flags)
                 $query->where('status', User::STATUS_SUSPICIOUS);
             if (UserFlags::SUSPENDED & $flags)
                 $query->where('status', User::STATUS_SUSPENDED);
             
            $db = Database::getInstance();
            $db->query($query->__toString());
            $result = $db->single();
            if (count($result) > 0) {
                return $result['userCount'];
            }
                
            return null;
        }
        
        public static function search($searchString) {
          if (is_numeric($searchString)) {
            $sql = "SELECT * FROM ". self::TABLE ." WHERE idNumber LIKE :searchString";
          } else {
            $sql = "SELECT * FROM ". self::TABLE ." WHERE LCASE(lastName) LIKE  :searchString OR LCASE(firstName) LIKE  :searchString";
          }
          $db = Database::getInstance();
          $db->query($sql);
          $db->bind(':searchString', "%".  strtolower($searchString)."%");
          $results = $db->resultset();
          $users = array();
          if (count($results) > 0) {
            foreach ($results as $result) {
              $users[] = new User($result);
            }
          }
          return $users;
        }
}	

class UserException extends RuntimeException {
	public function __construct($message, $code) {
		parent::__construct($message, $code);
	}
}

final class UserFlags  {
    
    const ADMIN = 1;
    const FACULTY = 2;
    const STUDENT = 4;
    const ASC = 8;
    
    
    const SUSPENDED = 128;
    
    const SUSPICIOUS = 64;
    const INACTIVE = 32;
    const ACTIVE = 16;
    
    public static function flagExists($value) {
        switch($value) {
            case self::ADMIN:
            case self::FACULTY:
            case self::STUDENT:
            case self::ASC:
            case self::SUSPENDED:
            case self::SUSPICIOUS:
            case self::INACTIVE:
            case self::ACTIVE:
                return true;
        }
        return false;
    }
    
}
