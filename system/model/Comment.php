<?php

require_once MODEL.'iDatabaseEntity.php';
class Comment implements DatabaseEntity {
  
  
  const STATUS_ACTIVE = 'A';
  const STATUS_INACTIVE = 'I';
  private $commentID;
  private $userID;
  private $publicationID;
  private $comment;
  private $commentDate;
  private $status;
  
  const TABLE = 'comments';
  
  
  public function __construct($resultSet = null) {
    if (is_array($resultSet)) {
      $this->setComment($resultSet['comment']);
      $this->setCommentID($resultSet['commentID']);
      $this->setUserID($resultSet['userID']);
      $this->setPublicationID($resultSet['publicationID']);
      $this->setStatus($resultSet['status']);
      $commentDate = DateTime::createFromFormat(DatabaseEntity::SQL_DATETIME_FORMAT, $resultSet['commentDate']);
      if ($commentDate instanceof DateTime) {
        $this->setCommentDate($commentDate);
      }
      
    }
  }
  
  public function getCommentID() {
    return $this->commentID;
  }

  public function setCommentID($commentID) {
    $this->commentID = $commentID;
  }

  public function getUserID() {
    return $this->userID;
  }

  public function setUserID($userID) {
    $this->userID = $userID;
  }

  public function getPublicationID() {
    return $this->publicationID;
  }

  public function setPublicationID($publicationID) {
    $this->publicationID = $publicationID;
  }

  public function getComment() {
    return $this->comment;
  }

  public function setComment($comment) {
    $this->comment = $comment;
  }

  public function getCommentDate() {
    return $this->commentDate;
  }

  public function setCommentDate(DateTime $commentDate) {
    $this->commentDate = $commentDate;
  }

  public function getStatus() {
    return $this->status;
  }

  public function setStatus($status) {
    $this->status = $status;
  }
  
  public static function getById($id) {
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE commentID = :id';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $id);
    $resultSet = $db->single();
    if (count($resultSet) > 0) {
      return new Comment($resultSet);
    }
    return null;
  }

  public function updateRecord() {
    $sql = 'UPDATE '.self::TABLE.' SET comment = :comment, status = :status WHERE commentID = :id';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':comment', $this->getComment());
    $db->bind(':status', $this->getStatus());
    $db->bind(':id', $this->getCommentID());
    return $db->execute();
  }
  
  public function storeRecord() {
    $db = Database::getInstance();
    $sql = 'INSERT INTO '.self::TABLE.' (userID, publicationID, comment, status) VALUES (:userID, :publicationID, :comment, :status)';
    $db->query($sql);
    $db->bind(':userID', $this->getUserID());
    $db->bind(':publicationID', $this->getPublicationID());
    $db->bind(':comment', $this->getComment());
    $db->bind(':status', $this->getStatus());
    return (bool)$db->execute();
  }
  
  public static function getByPublicationID($id, $includeInactive = false) {
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE publicationID = :id AND userID NOT IN (SELECT userID from publicationmutes WHERE publicationID = :id)';
    if (!$includeInactive) {
      $sql .= " AND status = 'A'";
    }
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $id);
    $resultSet = $db->resultset();
    if (count($resultSet) > 0) {
      $arr = array();
      foreach ($resultSet as $record) {
        $arr[] = new Comment($record);
      }
      return $arr;
    }
    return null;
  }
  
  public static function factory($userID, $publicationID, $commentStr) {
    $comment = new Comment();
    $comment->setUserID($userID);
    $comment->setPublicationID($publicationID);
    $comment->setComment($commentStr);
    $comment->setStatus('A');
    return $comment;
  }

}

