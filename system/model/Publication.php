<?php
require_once MODEL.'iDatabaseEntity.php';
class Publication implements DatabaseEntity {
  
  private $publicationID;
  private $userID;
  private $publicationDate;
  private $filePath;
  private $origPath;
  private $description;
  private $status;
  
  const TABLE = 'publications';
  
  public function __construct($resultSet = null) {
    if (is_array($resultSet)) {
      $this->setPublicationID($resultSet['publicationID']);
      $this->setUserID($resultSet['userID']);
      $this->setFilePath($resultSet['filePath']);
      $this->setOrigPath($resultSet['origPath']);
      $this->setDescription($resultSet['description']);
      $this->setStatus($resultSet['status']);
      $date = DateTime::createFromFormat(DatabaseEntity::SQL_DATETIME_FORMAT, $resultSet['publicationDate']);
      if ($date instanceof DateTime) {
        $this->setPublicationDate($date);
      }
    }
      
  }


  public function getPublicationID() {
    return $this->publicationID;
  }

  public function setPublicationID($publicationID) {
    $this->publicationID = $publicationID;
  }

  public function getUserID() {
    return $this->userID;
  }

  public function setUserID($userID) {
    $this->userID = $userID;
    return $this;
  }

    
  public function getPublicationDate() {
    return $this->publicationDate;
  }

  public function setPublicationDate(DateTime $publicationDate) {
    $this->publicationDate = $publicationDate;
  }

  public function getFilePath() {
    return $this->filePath;
  }

  public function setFilePath($filePath) {
    $this->filePath = $filePath;
  }

  public function getOrigPath() {
    return $this->origPath;
  }

  public function setOrigPath($origPath) {
    $this->origPath = $origPath;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function getStatus() {
    return $this->status;
  }

  public function setStatus($status) {
    $this->status = $status;
  }
  
  public static function getById($id) {
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE publicationID = :id';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $id); 
    $result = $db->single();
    if ($result != false) {
      return new Publication($result);
    }
    return null;
  }
  
  public static function getByUserID($userID, $activeOnly = true) {
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE userID = :id';
	if ($activeOnly) {
		$sql .= ' AND status = "A"';
	}
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $userID); 
    $results = $db->resultSet();
    if (count($results) > 0) {
      $arr = array();
     
      foreach ($results as $result) {
        $arr[] = new Publication($result);
      }
      return $arr;
    }
    return null;
  }

  public function updateRecord() {
    $sql = 'UPDATE '.self::TABLE.' SET description = :desc, filePath = :path, origPath = :orig, status = :status WHERE publicationID = :id';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':desc', $this->getDescription());
    $db->bind(':path', $this->getFilePath());
    $db->bind(':orig', $this->getOrigPath());
    $db->bind(':status', $this->getStatus());
    $db->bind(':id', $this->getPublicationID());
    $db->execute();
    return $db->getErrorCode() == Database::SUCCESS_CODE;
  }
  
  public function saveRecord() {
    $sql = 'INSERT INTO '.self::TABLE.' (publicationDate, userID, filePath, origPath, description) 
                                 VALUES (:date, :user, :filePath, :origPath, :desc)';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':date', Database::toSqlTimestamp($this->getPublicationDate()));
    $db->bind(':user', $this->getUserID());
    $db->bind(':filePath', $this->getFilePath());
    $db->bind(':origPath', $this->getOrigPath());
    $db->bind(':desc', $this->getDescription());

    
    $db->execute();
    
    return $db->lastInsertId();
  }
  
  public static function factory(DateTime $date, $path, $desc, $user, $origPath) {
    $pub = new Publication();
    $pub->setFilePath($path);
    $pub->setPublicationDate($date);
    $pub->setDescription($desc);
    $pub->setUserID($user);
    $pub->setOrigPath($origPath);
    return $pub;
  }
  public static function publicationExists(User $user, $path) {
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE userID = :userID AND origPath = :path';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':userID', $user->getUserID());
    $db->bind(':path', $path);
    $result = $db->single();
    if ($result != false) {
      return true ;
    }
    return false;
  }
  public function getCommenters() {
    $sql = 'SELECT DISTINCT users.*
            FROM users
            JOIN comments
            ON
            comments.userID = users.userID
            WHERE (comments.publicationID = :id)';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $this->getPublicationID());
    $result = $db->resultset();
    
    if (count($result) > 0) {
      $arr = array();
      foreach ($result as $user) {
        $arr[] = new User($user);
      }
      return $arr;
    }
    return null;
  }
  
  public function muteUser($userID) {
    $sql = 'INSERT INTO publicationmutes (publicationID, userID) VALUES (:pubID, :userID)';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':pubID', $this->getPublicationID());
    $db->bind(':userID', $userID);
    return $db->execute();
  }
  
  public function unmuteUser($userID) {
    $sql = 'DELETE FROM publicationmutes WHERE publicationID = :pubID AND  userID = :userID';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':pubID', $this->getPublicationID());
    $db->bind(':userID', $userID);
    return $db->execute();
  }
  
  public function getMutes() {
    $sql = 'SELECT DISTINCT users.* FROM users JOIN publicationmutes ON publicationmutes.userID = users.userID WHERE publicationID = :pubID';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':pubID', $this->getPublicationID());
    $result = $db->resultset();
    
    if (count($result) > 0) {
      $arr = array();
      foreach ($result as $user) {
        $arr[] = new User($user);
      }
      return $arr;
    }
    return null;
  }
}