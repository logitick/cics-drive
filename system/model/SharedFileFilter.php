<?php

class SharedFileFilter {

  private $sharedFileFilterID;
  private $extension;
  private $sharedFolderID;
  
  const TABLE = 'sharedfilefilters';
  
  function __construct($extension, $sharedFolderID = 0, $sharedFileFilterID = 0) {
    $this->extension = $extension;
    $this->sharedFolderID = $sharedFolderID;
    $this->sharedFileFilterID = $sharedFileFilterID;
  }
  
  public function getSharedFileFilterID() {
    return $this->sharedFileFilterID;
  }

  public function setSharedFileFilterID($sharedFileFilterID) {
    $this->sharedFileFilterID = $sharedFileFilterID;
    return $this;
  }

  public function getExtension() {
    return $this->extension;
  }

  public function setExtension($extension) {
    $this->extension = $extension;
    return $this;
  }

  public function getSharedFolderID() {
    return $this->sharedFolderID;
  }

  public function setSharedFolderID($sharedFolderID) {
    $this->sharedFolderID = $sharedFolderID;
    return $this;
  }
  
  public static function getByFolderID($id) {
    $sql = 'SELECT * FROM '.self::TABLE.' WHERE sharedFolderID = :id';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':id', $id);
    $results = $db->resultset();
    if (count($results) > 0) {
      $filters = array();
      foreach ($results as $resultset) {
        $filter = new SharedFileFilter($resultset['extension'], $resultset['sharedFolderID'], $resultset['sharedFileFilterID']);
        $filters[] = $filter;
      }
      return $filters;
    }
    return null;
  }
  
  public function saveRecord() {
    $db = Database::getInstance();
    $sql = $sql = "INSERT INTO `cics_drive`.`sharedfilefilters` (`sharedFileFilterID`, `extension`, `sharedFolderID`) VALUES (NULL, \'java\', \'17\');";
    $db->query('INSERT INTO '.self::TABLE.' (extension, sharedFolderID) VALUES (:ext, :id)');
    $db->bind(':ext', $this->getExtension());
    $db->bind(':id', $this->getSharedFolderID());
    $db->execute();
    return $db->getErrorCode() == Database::SUCCESS_CODE;
  }
  
  public function __toString() {
    return $this->getExtension();
  }
}