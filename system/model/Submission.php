<?php
require_once MODEL.'iDatabaseEntity.php';
require_once LIBRARY.'DBQuery.php';
class Submission implements DatabaseEntity {

  private $submissionID;
  private $sharedFolderID;
  private $studentUserID;
  private $submissionDate;
  private $originalFolderName;
  private $status;
  const TABLE = 'submissions';
  
  const STATUS_ACTIVE = 'A';
  const STATUS_INACTIVE = 'I';
  
  public function __construct($resultSet = null) {
    if (is_array($resultSet)) {
      $this->setSubmissionID($resultSet['submissionID']);
      $this->setSharedFolderID($resultSet['sharedFolderID']);
      $this->setStudentUserID($resultSet['studentUserID']);
      $this->setSubmissionDate(DateTime::createFromFormat(DatabaseEntity::SQL_DATETIME_FORMAT, $resultSet['submissionDate']));
      $this->setOriginalFolderName($resultSet['originalFolderName']);
      $this->setStatus($resultSet['status']);
    }
  }
  
  public function setSubmissionID($submissionID) {
    $this->submissionID = $submissionID;
  }

  public function setSharedFolderID($sharedFolderID) {
    $this->sharedFolderID = $sharedFolderID;
  }

  public function setStudentUserID($studentUserID) {
    $this->studentUserID = $studentUserID;
  }

  public function setSubmissionDate(DateTime $submissionDate) {
    $this->submissionDate = $submissionDate;
  }

  public function setOriginalFolderName($originalFolderName) {
    $this->originalFolderName = $originalFolderName;
  }

  public function setStatus($status) {
    $this->status = $status;
  }

  public function getSubmissionID() {
    return $this->submissionID;
  }

  public function getSharedFolderID() {
    return $this->sharedFolderID;
  }

  public function getStudentUserID() {
    return $this->studentUserID;
  }

  public function getSubmissionDate() {
    return $this->submissionDate;
  }

  public function getOriginalFolderName() {
    return $this->originalFolderName;
  }

  public function getStatus() {
    return $this->status;
  }

    
  public static function getByID($id) {
    $sql = DBQuery::select()->from(self::TABLE)->where('submissionID', $id);
    $db = Database::getInstance();
    $db->query($sql->__toString());
    $result = $db->single();
    if ($db->rowCount() > 0) {
      return new Submission($result);
    }
    return null;
  }

  public function updateRecord() {
    $sql = 'UPDATE '.self::TABLE.' SET status = :status WHERE submissionID = :id';
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':status', $this->getStatus());
    $db->bind(':id', $this->getSubmissionID());
    $db->execute();
    return $db->getErrorCode() == Database::SUCCESS_CODE;
  }
  
  public function saveRecord() {
    $sql = DBQuery::INSERT()->into(self::TABLE)->
              columns(array('sharedFolderID', 'studentUserID', 'originalFolderName'))->
              values(array($this->getSharedFolderID(), $this->getStudentUserID(), $this->getOriginalFolderName()));

    $db = Database::getInstance();
    $db->query($sql->__toString());
    if ($db->execute()) {
      $this->setSubmissionID($db->lastInsertId());
      return true;
    }
    return false;
  }
  
  public static function factory($sharedFolderID, $submitterID, $origFolderName) {
    $submission = new Submission();
    $submission->setSharedFolderID($sharedFolderID);
    $submission->setStudentUserID($submitterID);
    $submission->setOriginalFolderName($origFolderName);
    return $submission;
  }

}